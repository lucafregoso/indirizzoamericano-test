<?php

return array(
    # Account credentials from developer portal
    'Account' => array(
        'ClientId' => env('PAYPAL-ID', 'ASVrbR1If6RbzOAYyVzWUSAylRiv32Q1updWPPrw0eT4nr4bfvEMkytFIVaBixat0Itq8QKgbaY15AOe'),
        'ClientSecret' => env('PAYPAL-SECRET', 'EGW5pbCqz56dyyu8Cl83XjZRDylW0SFlI80eBrxEL6WGC2gfhSQ6WNxyNkd1dyK6Q9UfDikLRU5Sahk5'),
    ),

    # Connection Information
    'Http' => array(
        // 'ConnectionTimeOut' => 30,
        'Retry' => 1,
        //'Proxy' => 'http://[username:password]@hostname[:port][/path]',
    ),

    # Service Configuration
    'Service' => array(
        # For integrating with the live endpoint,
        # change the URL to https://api.paypal.com!
        'EndPoint' => env('PAYPAL-ENDPOINT','https://api.sandbox.paypal.com'),
    ),


    # Logging Information
    'Log' => array(
        'LogEnabled' => true,

        # When using a relative path, the log file is created
        # relative to the .php file that is the entry point
        # for this request. You can also provide an absolute
        # path here
        'FileName' => '../PayPal.log',

        # Logging level can be one of FINE, INFO, WARN or ERROR
        # Logging is most verbose in the 'FINE' level and
        # decreases as you proceed towards ERROR
        'LogLevel' => 'FINE',
    ),
);
