<?php

use Illuminate\Database\Seeder;
use App\File;
use App\Suggestion;

class SuggestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('files')->truncate();
        DB::table('suggestions')->truncate();

        //Image
        $suggestions_image_data =  [ 
            ['originalname' => 'hollister.png', 'extension' => 'png', 'mimetype' => 'image/png', 'size' => '8732', 'filename' => 'd1baf07265e154cde8116e2ae2d184a3' ],
            ['originalname' => 'best-buy.png', 'extension' => 'png', 'mimetype' => 'image/png', 'size' => '10124', 'filename' => 'fdac20c5cbf322e33937b6f7d8ee67da' ],
            ['originalname' => 'new-egg.png', 'extension' => 'png', 'mimetype' => 'image/png', 'size' => '12818', 'filename' => '33842593039a4cc0ca071d3199db706e' ],
            ['originalname' => 'harley.png', 'extension' => 'png', 'mimetype' => 'image/png', 'size' => '14768', 'filename' => 'f08d8a2b99a719f1c264c6c474f72a3e' ],
        ];

        foreach ($suggestions_image_data as $sid) {
            $image = File::create($sid);
        }

        $suggestions = [
            ['name' => 'hollister', 'description' => 'hollister', 'image_id' => 1, 'url' => 'https://hollisterco.com', 'blank' => null ],
            ['name' => 'best-buy', 'description' => 'best-buy', 'image_id' => 2, 'url' => 'www.bestbuy.com', 'blank' => null ],
            ['name' => 'new-egg', 'description' => 'new-egg', 'image_id' => 3, 'url' => 'https://www.newegg.com/', 'blank' => null ],
            ['name' => 'harley', 'description' => 'harley', 'image_id' => 4, 'url' => 'www.harley-davidson.com', 'blank' => null ],
        ];

        foreach ($suggestions as $s) {
            $suggestion = Suggestion::create($s);
        }

    }
}