<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Package;
use App\User;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->truncate();
        $faker = Faker::create();
        $user = User::find(6);


       for ($i=0; $i < 10; $i++) { 
            $package = Package::create([
                'barcode' => $faker->creditCardNumber,
                'description' => $faker->text,
                'reseller' => $faker->company,
                'user_id' => $user->id
            ]);

            $package->update(['uid' => $user->uuic.'-'.$package->id]);
        }

    }
}
