<?php

use Faker\Factory as Faker;
use App\Helpers\Helper;

class AdminSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('users')->truncate(); // Using truncate function so all info will be cleared when re-seeding.
        DB::table('roles')->truncate();
        DB::table('role_users')->truncate();
        DB::table('activations')->truncate();

        $faker = Faker::create();

        $superadminRole = Sentinel::getRoleRepository()->createModel()->create([
            'name'  => 'Superadmin',
            'slug'  => 'superadmin',
            'permissions' => ['admin' => 1],
        ]);

        $adminRole = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => ['admin' => 1],
        ]);

        $personalshopperRole = Sentinel::getRoleRepository()->createModel()->create([
            'name'  => 'Personal Shopper',
            'slug'  => 'personal_shopper',
        ]);

        $warehouseworkerRole = Sentinel::getRoleRepository()->createModel()->create([
            'name'  => 'Warehouse Worker',
            'slug'  => 'warehouse_worker',
        ]);

        $premiumuserRole = Sentinel::getRoleRepository()->createModel()->create([
            'name'  => 'Premium User',
            'slug'  => 'premium_user',
        ]);

        $userRole = Sentinel::getRoleRepository()->createModel()->create([
            'name'  => 'User',
            'slug'  => 'user',
        ]);

        # Users
        $super_admin = Sentinel::registerAndActivate(array(
            'email'       => 'superadmin@superadmin.com',
            'password'    => "superadmin",
            'first_name'  => 'Super',
            'last_name'   => 'Admin',
            //'uuic'        => Helper::generate_uuic(),
        ));

        $super_admin->roles()->attach($superadminRole);

        $admin = Sentinel::registerAndActivate(array(
            'email'       => 'admin@admin.com',
            'password'    => "admin",
            'first_name'  => 'Admin',
            'last_name'   => 'Admin',
            //'uuic'        => Helper::generate_uuic(),
        ));

        $admin->roles()->attach($adminRole);

        $personal_shopper = Sentinel::registerAndActivate(array(
            'email'       => 'personalshopper@personalshopper.com',
            'password'    => "personalshopper",
            'first_name'  => 'Personal',
            'last_name'   => 'Shopper',
            //'uuic'        => Helper::generate_uuic(),
        ));

        $personal_shopper->roles()->attach($personalshopperRole);

        $warehouse_worker = Sentinel::registerAndActivate(array(
            'email'       => 'warehouseworker@warehouseworker.com',
            'password'    => "warehouseworker",
            'first_name'  => 'Warehouse',
            'last_name'   => 'Worker',
            //'uuic'        => Helper::generate_uuic(),
        ));

        $warehouse_worker->roles()->attach($warehouseworkerRole);

        $premium_user = Sentinel::registerAndActivate(array(
            'email'       => 'premiumuser@premiumuser.com',
            'password'    => "premiumuser",
            'first_name'  => 'Premium',
            'last_name'   => 'User',
            //'uuic'        => Helper::generate_uuic(),
        ));

        $premium_user->roles()->attach($premiumuserRole);

        for ($i=0; $i <= 20 ; $i++) { 
            $user = Sentinel::registerAndActivate(array(
                'email'       => 'user'.$i.'@user.com',
                'password'    => "user",
                'first_name'  => $faker->firstName,
                'last_name'   => $faker->lastName,
                'uuic'        => Helper::generate_uuic(),
                'tax'         => $faker->creditCardNumber,
                'phone'       => $faker->e164PhoneNumber,
                'address'     => $faker->streetAddress,
                'city'        => $faker->city,
                'zip_code'    => $faker->postcode,
                'state'       => $faker->state,
            ));

            $user->roles()->attach($userRole);
        }

        $this->command->info('Super Admin User created with username superadmin@superadmin.com and password superadmin');
        $this->command->info('Admin User created with username admin@admin.com and password admin');
        $this->command->info('Personal Shopper User created with username personalshopper@personalshopper.com and password personalshopper');
        $this->command->info('Warehouse Worker User created with username warehouseworker@warehouseworker.com and password warehouseworker');
        $this->command->info('Users created with username user0@user.com TO user20@user.com and password user');
    }

}