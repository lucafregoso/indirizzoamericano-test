<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Service;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->truncate();
        $faker = Faker::create();

        $services = [
            [   'name' => 'Foto del contenuto di ogni pacco', 
                'slug' => 'photo', 
                'description' => 'Fotografiamo l’interno del tuo pacco per vedere che tutto sia integro e corretto', 
                'price' => $faker->numberBetween(1 , 100), 
                'status' => 1, 
                'reference' => 'packages', 
                'locale' => 'it' 
            ],
            [   'name' => 'DOA (Dead On Arrival)', 
                'slug' => 'doa', 
                'description' => 'Verifichiamo che il vostro apparecchio elettronico per verificare che all’arrivo si accenda', 
                'price' => $faker->numberBetween(1 , 100), 
                'status' => 1, 
                'reference' => 'packages', 
                'locale' => 'it' 
            ],
            [   'name' => 'Ottimizzazione Pacco', 
                'slug' => 'package_optimizer', 
                'description' => '(Selezionate questa opzione gratuita e elimineremo anche la scatola, un ulteriore risparmio sul volume e quindi sul costo finale.)', 
                'price' => $faker->numberBetween(1 , 100), 
                'status' => 1, 
                'reference' => 'packages', 
                'locale' => 'it' 
            ],
            [   'name' => 'Assicurazione Pacco', 
                'slug' => 'package_insurance', 
                'description' => '(Assicurazione sul valore della spedizione oltre i 100$ + 3$ di costo)', 
                'price' => $faker->numberBetween(1 , 100), 
                'status' => 1, 
                'reference' => 'orders', 
                'locale' => 'it' 
            ],
            [   'name' => 'Rispedizione automatica del pacco', 
                'slug' => 'package_auto_send', 
                'description' => 'Aspettate un pacco solo e non volete attendere la notifica? Dovete spedire temporaneamente 
                    ad un altro indirizzo? Puoi farlo gratuitamente! Selezionate questa opzione gratuita e quando 
                    lo riceveremo sapremo subito che è da spedire e lo prepareremo immediatamente, riceverete 
                    solo la richiesta di pagamento della spedizione!', 
                'price' => $faker->numberBetween(1 , 100), 
                'status' => 1, 
                'reference' => 'users', 
                'locale' => 'it' 
            ],
            [   'name' => 'Spedisci a un altro indirizzo', 
                'slug' => 'extra_address', 
                'description' => null, 
                'price' => $faker->numberBetween(1 , 100), 
                'status' => 1, 
                'reference' => 'orders', 
                'locale' => 'it' 
            ],
        ];

        //Add Service data
        foreach($services as $service) {
            Service::create($service);
        }

    }
}
