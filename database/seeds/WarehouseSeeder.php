<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Warehouse;

class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('warehouses')->truncate();
        $faker = Faker::create();

        Warehouse::create([
            'name' => $faker->company,
            'description' => null,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'state' => $faker->state,
            'zip_code' => $faker->postcode,
            'phone' => $faker->tollFreePhoneNumber,
            'fax' => $faker->tollFreePhoneNumber,
            'facebook' => 'http://www.facebook.com',
            'email' => 'info@indirizzo-americano.app',
        ]);

    }
}
