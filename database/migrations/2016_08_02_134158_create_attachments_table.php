<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('attachments', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id');

      $table->string('type');
      $table->string('status')->default('undetermined');

      $table->string('filename')->nullable();
      $table->string('originalname')->nullable();
      $table->string('mimetype')->nullable();
      $table->string('size')->nullable();
      $table->string('extension')->nullable();

      $table->string('path')->nullable();
      $table->longtext('description')->nullable();

      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('attachment_polies', function (Blueprint $table) {
      $table->integer('attachment_id');
      $table->integer('attachment_poly_id');
      $table->string('attachment_poly_type');

      $table->nullableTimestamps();

      $table->engine = 'InnoDB';
      $table->primary(['attachment_id', 'attachment_poly_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    $table = 'attachments';
    Storage::disk('local')->put($table.'_'.date('Y-m-d_H-i-s').'.bak', json_encode(DB::table($table)->get()));
    Schema::drop($table);

    Schema::drop('attachment_polies');
  }
}
