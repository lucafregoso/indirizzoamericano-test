<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('conversations', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('creator_id');
      $table->string('title');
      $table->string('type');
      $table->string('status')->default('new');
      $table->boolean('email_notifications')->default(0);
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('conversation_user', function (Blueprint $table) {
      $table->integer('conversation_id')->unsigned();
      $table->integer('user_id')->unsigned();

      $table->nullableTimestamps();

      $table->engine = 'InnoDB';
      $table->primary(['conversation_id', 'user_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    $table = 'conversations';
    Storage::disk('local')->put($table.'_'.date('Y-m-d_H-i-s').'.bak', json_encode(DB::table($table)->get()));
    Schema::drop($table);

    Schema::drop('conversation_user');
  }
}
