<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalShopperProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_shopper_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('personal_shopper_id');
            $table->string('quantity')->nullable();
            $table->string('article_code')->nullable();
            $table->string('size')->nullable();
            $table->string('color')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_shopper_products');
    }
}
