<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationEventsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('conversation_events', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id');
      $table->integer('conversation_id');
      $table->string('type')->default('message');
      $table->longtext('content')->nullable();
      $table->longtext('detail')->nullable();
      $table->longtext('data')->nullable();

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    $table = 'conversation_events';
    Storage::disk('local')->put($table.'_'.date('Y-m-d_H-i-s').'.bak', json_encode(DB::table($table)->get()));
    Schema::drop($table);
  }
}
