<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('dob')->nullable();
            $table->string('tax')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('state')->nullable();
            $table->string('pic')->nullable();
            $table->string('uuic')->nullable();
            $table->boolean('privacy')->default(0);
            $table->boolean('newsletter')->default(0);

            $table->unique('uuic');
            $table->index('uuic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            // delete above columns
            $table->dropColumn(array('first_name', 'last_name', 'dob', 'tax', 'phone', 'address', 'state', 'pic', 'uuic', 'privacy', 'newsletter'));
        });
    }

}
