<?php

use App\User;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase {
  use DatabaseMigrations;

  public function testUserCanBeLookedUpByUsername() {
    $createdUser = factory(User::class)->create(['username' => 'browncoat']);

    $foundUser = User::findByUsername('browncoat')[0];

    $this->assertEquals($createdUser->id, $foundUser->id);
    $this->assertEquals($createdUser->first_name, $foundUser->first_name);
    $this->assertEquals($createdUser->username, $foundUser->username);
  }

  public function testMultipleUsersCanBeLookedUpByUsername() {
    $browncoat = factory(User::class)->create(['username' => 'browncoat']);
    $bruiseast = factory(User::class)->create(['username' => 'bruiseast']);
    $brawlcant = factory(User::class)->create(['username' => 'brawlcant']);

    $foundUsers = User::findByUsername('browncoat', 'bruiseast', 'brawlcant');

    $this->assertEquals($browncoat->id, $foundUsers[0]->id);
    $this->assertEquals($bruiseast->id, $foundUsers[1]->id);
    $this->assertEquals($brawlcant->id, $foundUsers[2]->id);

    $this->assertEquals($browncoat->first_name, $foundUsers[0]->first_name);
    $this->assertEquals($bruiseast->first_name, $foundUsers[1]->first_name);
    $this->assertEquals($brawlcant->first_name, $foundUsers[2]->first_name);

    $this->assertEquals($browncoat->username, $foundUsers[0]->username);
    $this->assertEquals($bruiseast->username, $foundUsers[1]->username);
    $this->assertEquals($brawlcant->username, $foundUsers[2]->username);
  }
}
