<?php

use App\User;
use S2K\Mercury\Attachment;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Illuminate\Http\UploadedFile;

class AttachmentTest extends TestCase {
  use DatabaseMigrations;

  public function testAttachmentCanBeCreatedPersistedAndRecovered() {
    $user = factory(User::class)->create(['username' => 'browncoat']);

    Sentinel::login($user);

    $attachment = factory(Attachment::class)->create([
      'user_id' => $user->id,
    ]);
    $file = new UploadedFile( // Fake Upload
      __DIR__.'/../assets/test.file.txt',
      'test.file.txt',
      'text/plain',
      null,
      null
    );
    $attachment->persist($file);

    $databaseAttachment = Attachment::find($attachment->id);

    $this->assertEquals($databaseAttachment->filename_original, 'test.file.txt');
    $this->assertEquals($databaseAttachment->url(), '/storage/'.$databaseAttachment->filename);

    $this->assertEquals($databaseAttachment->user->id, $user->id);
  }
}
