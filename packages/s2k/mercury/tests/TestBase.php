<?php

namespace S2K\Mercury\Tests;

use App\User;
use S2K\Mercury\Conversation;
use S2K\Mercury\ConversationEvent;

use Illuminate\Support\Collection;

class TestBase extends \TestCase {
  public static function createUsers($usersOrAmount = 1) {
    if (is_array($usersOrAmount)) {
      $usersArray = array();
      foreach ($usersOrAmount as $user) {
        if (!is_array($user)) {
          $user = ['username' => $user];
        }

        $usersArray[] = factory(User::class)->create($user);
      }

      $users = collect($usersArray);
    } elseif (is_numeric($usersOrAmount)) {
      $users = factory(User::class, $usersOrAmount)->create();
    } elseif ($usersOrAmount instanceof Collection) {
      $users = $usersOrAmount;
    }

    return $users;
  }

  public static function createConversationWithUsers($attributes, $immutableUsers) {
    $users = clone $immutableUsers;

    $creator = $users->shift();

    $fullAttributes = array_merge($attributes, [
      'creator_id' => $creator->id,
    ]);

    $conversation = factory(Conversation::class)->create($fullAttributes);
    $conversation->addUser(...$users);

    return $conversation;
  }

  public static function createConversationEvent($attributes, $conversation, $user) {
    $event = factory(ConversationEvent::class)->create(
      array_merge(
        $attributes,
        array(
          'user_id' => $user->id,
          'conversation_id' => $conversation->id,
        )
      )
    );
  }
}
