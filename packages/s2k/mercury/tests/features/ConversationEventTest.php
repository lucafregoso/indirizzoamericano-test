<?php

use S2K\Mercury\Tests\TestBase;

use Illuminate\Support\Collection;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ConversationEventTest extends TestBase {
  use DatabaseMigrations;

  public function createRoledUsers() {
    $users = $this->createUsers(['browncoat', 'admin', 'warehouse']);

    $warehouse = $users->where('username', 'warehouse')->first();
    $admin = $users->where('username', 'admin')->first();
    $user = $users->where('username', 'browncoat')->first();

    // Roles
    $adminRole = Sentinel::getRoleRepository()->createModel()->create([
      'name' => 'Admin',
      'slug' => 'admin',
      'permissions' => [
        'application.admin' => true,
        'mercury.message.create.*' => true,
      ],
    ]);
    $admin->roles()->attach($adminRole);

		$warehouseRole = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Warehouse Man',
			'slug' => 'warehouse',
			'permissions' => [
				'application.warehouse' => true,
				'mercury.message.create.owned.*' => true,
				'mercury.message.create.owned.resolved' => false,
				'mercury.message.create.other.active' => true,
			],
		]);
		$warehouse->roles()->attach($warehouseRole);

		$userRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Registered User',
			'slug'  => 'user',
			'permissions' => [
				'application.user' => true,
				'mercury.message.create.owned.*' => true,
				'mercury.message.create.owned.resolved' => false,
				'mercury.message.create.other.active' => true,
			]
		]);
		$user->roles()->attach($userRole);

    return [$users, [$admin, $warehouse, $user]];
  }

  // // Open
  // public function testAdminParticipantsCanAddMessageWhenOpen() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $admin = $users[1][0];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation',
  //     'status' => 'active'
  //   ], $users[0]);
  //
  //   Sentinel::login($admin);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //        ->see('Can Add Message Conversation')
  //        ->see('New Message')
  //        ->type('Test Event Admin', 'detail')
  //        ->type('Test Event Admin Message Content', 'content')
  //        ->press('Reply')
  //        ->seeInDatabase('conversation_events', ['content' => 'Test Event Admin Message Content'])
  //        ->see('Test Event Admin')
  //        ->see('Test Event Admin Message Content');
  // }
  //
  // public function testOwnerParticipantsCanAddMessageWhenOpen() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $user = $users[1][2];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation',
  //     'status' => 'active'
  //   ], $users[0]);
  //
  //   Sentinel::login($user);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //       ->see('Can Add Message Conversation')
  //       ->see('New Message')
  //       ->type('Test Event User', 'detail')
  //       ->type('Test Event User Message Content', 'content')
  //       ->press('Reply')
  //       ->seeInDatabase('conversation_events', ['content' => 'Test Event User Message Content'])
  //       ->see('Test Event User')
  //       ->see('Test Event User Message Content');
  // }
  //
  // public function testNormalParticipantsCanAddMessageWhenOpen() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $warehouse = $users[1][1];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation',
  //     'status' => 'active'
  //   ], $users[0]);
  //
  //   Sentinel::login($warehouse);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //       ->see('Can Add Message Conversation')
  //       ->see('New Message')
  //       ->type('Test Event Warehouse', 'detail')
  //       ->type('Test Event Warehouse Message Content', 'content')
  //       ->press('Reply')
  //       ->seeInDatabase('conversation_events', ['content' => 'Test Event Warehouse Message Content'])
  //       ->see('Test Event Warehouse')
  //       ->see('Test Event Warehouse Message Content');
  // }
  //
  // // Closed
  // public function testAdminParticipantsCanAddMessageWhenClosed() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $admin = $users[1][0];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation (Closed)',
  //     'status' => 'closed'
  //   ], $users[0]);
  //
  //   Sentinel::login($admin);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //        ->see('Can Add Message Conversation (Closed)')
  //        ->see('New Message')
  //        ->type('Test Event Admin', 'detail')
  //        ->type('Test Event Admin Message Content', 'content')
  //        ->press('Reply')
  //        ->seeInDatabase('conversation_events', ['content' => 'Test Event Admin Message Content'])
  //        ->see('Test Event Admin')
  //        ->see('Test Event Admin Message Content');
  // }
  //
  // public function testOwnerParticipantsCanAddMessageWhenClosed() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $user = $users[1][2];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation (Closed)',
  //     'status' => 'closed'
  //   ], $users[0]);
  //
  //   Sentinel::login($user);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //       ->see('Can Add Message Conversation (Closed)')
  //       ->see('New Message')
  //       ->type('Test Event User', 'detail')
  //       ->type('Test Event User Message Content', 'content')
  //       ->press('Reply')
  //       ->seeInDatabase('conversation_events', ['content' => 'Test Event User Message Content'])
  //       ->see('Test Event User')
  //       ->see('Test Event User Message Content');
  // }
  //
  // public function testNormalParticipantsCantAddMessageWhenClosed() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $warehouse = $users[1][1];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation (Closed)',
  //     'status' => 'closed'
  //   ], $users[0]);
  //
  //   Sentinel::login($warehouse);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //       ->see('Can Add Message Conversation (Closed)')
  //       ->dontSee('New Message')
  //       ->see('Conversation Closed');
  // }
  //
  // // Resolved
  // public function testAdminParticipantsCanAddMessageWhenResolved() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $admin = $users[1][0];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation (Resolved)',
  //     'status' => 'resolved'
  //   ], $users[0]);
  //
  //   Sentinel::login($admin);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //        ->see('Can Add Message Conversation (Resolved)')
  //        ->see('New Message')
  //        ->type('Test Event Admin', 'detail')
  //        ->type('Test Event Admin Message Content', 'content')
  //        ->press('Reply')
  //        ->seeInDatabase('conversation_events', ['content' => 'Test Event Admin Message Content'])
  //        ->see('Test Event Admin')
  //        ->see('Test Event Admin Message Content');
  // }
  //
  // public function testOwnerParticipantsCantAddMessageWhenResolved() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $user = $users[1][2];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation (Resolved)',
  //     'status' => 'resolved'
  //   ], $users[0]);
  //
  //   Sentinel::login($user);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //       ->see('Can Add Message Conversation (Resolved)')
  //       ->dontSee('New Message')
  //       ->see('Conversation Resolved');
  // }
  //
  // public function testNormalParticipantsCantAddMessageWhenResolved() {
  //   Sentinel::logout();
  //
  //   $users = $this->createRoledUsers();
  //   $warehouse = $users[1][1];
  //
  //   $conversation = $this->createConversationWithUsers([
  //     'title' => 'Can Add Message Conversation (Resolved)',
  //     'status' => 'resolved'
  //   ], $users[0]);
  //
  //   Sentinel::login($warehouse);
  //   var_dump(Sentinel::getUser()->username);
  //   $this->visit('/conversations/'.$conversation->id)
  //       ->see('Can Add Message Conversation (Resolved)')
  //       ->dontSee('New Message')
  //       ->see('Conversation Resolved');
  // }

  public function testOwnerAdminParticipantsCanAddUser() {

  }
}
