<?php

use S2K\Mercury\Tests\TestBase;

use Illuminate\Support\Collection;

use S2K\Mercury\Conversation;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AttachmentInsertionTest extends TestBase {
  use DatabaseMigrations;

  public function testAttachmentCanBeAddedToConversationList() {
    $users = $this->createUsers(['admin', 'warehouse', 'browncoat']);
    $user = $users->where('username', 'browncoat')->first();

    Sentinel::login($user);

    $this->visit('/conversations/new')
         ->type('Test Attachment Conversation', 'title')
         ->select('administration', 'type')
         ->select(['admin', 'warehouse'], 'users')
         ->attach(__DIR__.'/../assets/test.file.txt', 'attachment')
         ->press('Create')
         ->seeInDatabase('conversations', ['title' => 'Test Attachment Conversation'])
         ->see('Test Attachment Conversation')
         ->see('admin')
         ->see('warehouse')
         ->see('browncoat')
         ->see('Conversation Attachments')
         ->see('test.file.txt');
  }
}
