<?php

use S2K\Mercury\Tests\TestBase;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserConversationEventRelationshipTest extends TestBase {
  use DatabaseMigrations;

  public function testConversationEventsList() {
    $users = $this->createUsers(['browncoat']);
    $conversation = $this->createConversationWithUsers(['title' => 'Test Conversation'], $users);

    $firstEvent = $this->createConversationEvent(['content' => 'First Message'], $conversation, $conversation->creator);
    $secondEvent = $this->createConversationEvent(['content' => 'Second Message'], $conversation, $conversation->creator);

    Sentinel::login($conversation->creator);

    $this->visit('/conversations/'.$conversation->id)
         ->see('Test Conversation')
         ->see('First Message')
         ->see('Second Message');

    $this->get('/conversations/'.$conversation->id, ['Accept' => 'application/json'])
         ->seeJson([
           'title' => 'Test Conversation',
         ])
         ->seeJson([
           'content' => 'First Message'
         ])
         ->seeJson([
           'content' => 'Second Message'
         ]);
  }
}
