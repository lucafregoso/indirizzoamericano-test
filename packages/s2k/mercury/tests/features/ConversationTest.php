<?php

use S2K\Mercury\Tests\TestBase;

use Illuminate\Support\Collection;

use S2K\Mercury\Conversation;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ConversationTest extends TestBase {
  use DatabaseMigrations;

  public function testConversationCanBeCreated() {
    $users = $this->createUsers(['admin', 'warehouse', 'browncoat']);
    $user = $users->where('username', 'browncoat')->first();

    Sentinel::login($user);

    $this->visit('/conversations/new')
         ->type('Test Conversation', 'title')
         ->select('administration', 'type')
         ->select(['admin', 'warehouse'], 'users')
         ->press('Create')
         ->seeInDatabase('conversations', ['title' => 'Test Conversation'])
         ->see('Test Conversation')
         ->see('admin')
         ->see('warehouse')
         ->see('browncoat');
  }

  public function testConversationCanBeViewedByParticipants() {
    $users = $this->createUsers(5);
    $user = $users->random();

    $conversation = $this->createConversationWithUsers(['title' => 'Can View Conversation'], $users);

    Sentinel::login($user);

    $this->visit('conversations/'.$conversation->id)
         ->see('Can View Conversation');
  }

  public function testConversationCantBeViewedByOthers() {
    $users = $this->createUsers(5);
    $otherUser = $users->pop();

    $conversation = $this->createConversationWithUsers(['title' => 'Can\'t View Conversation'], $users);

    Sentinel::login($otherUser);

    $this->visit('conversations/'.$conversation->id)
         ->dontSee('Can\'t View Conversation')
         ->see('404');
  }

  public function testUserCanSeeOwnConversations() {
    $users = $this->createUsers(5);
    $user = $users->random();

    $firstConversation = $this->createConversationWithUsers(['title' => 'First Conversation', 'status' => 'new'], $users);
    $secondConversation = $this->createConversationWithUsers(['title' => 'Second Conversation', 'status' => 'updated'], $users);
    $closedConversation = $this->createConversationWithUsers(['title' => 'Closed Conversation', 'status' => 'closed'], $users);

    Sentinel::login($user);

    $this->visit('account/conversations')
         ->see('First Conversation')
         ->see('Second Conversation')
         ->see('Closed Conversation');
  }

  public function testConversationCanBeMarkedResolvedByParticipantWhenNotClosed() {
    $users = $this->createUsers(3);
    $conversation = $this->createConversationWithUsers(['title' => 'Status Change Conversation', 'status' => 'new'], $users);
    $conversation2 = Conversation::find($conversation->id);

    Sentinel::login($conversation->users->random());

    $this->visit('conversations/'.$conversation->id)
         ->see('new')
         ->click('Mark as Resolved')
         ->see('resolved');
  }

  public function testConversationCantBeMarkedResolvedByParticipantWhenClosed() {
    $users = $this->createUsers(3);

    $conversation = $this->createConversationWithUsers(['title' => 'Status Change Conversation', 'status' => 'closed'], $users);

    Sentinel::login($conversation->users->random());

    $this->visit('conversations/'.$conversation->id)
         ->see('closed')
         ->click('Mark as Resolved')
         ->withSession(['flash_notification' => array('message' => 'You are not allowed to make that operation.')])
         ->see('closed');
  }

  public function testConversationCanBeMarkedResolvedByCreatorAlways() {
    $users = $this->createUsers(3);

    $newConversation = $this->createConversationWithUsers(['title' => 'Status Change Conversation', 'status' => 'new'], $users);
    $closedConversation = $this->createConversationWithUsers(['title' => 'Status Change Conversation', 'status' => 'closed'], $users);

    Sentinel::login($newConversation->creator);

    $this->visit('conversations/'.$newConversation->id)
         ->see('new')
         ->click('Mark as Resolved')
         ->see('resolved');

    $this->visit('conversations/'.$closedConversation->id)
         ->see('closed')
         ->click('Mark as Resolved')
         ->see('resolved');
  }
}
