<?php

use S2K\Mercury\Tests\TestBase;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserConversationRelationshipTest extends TestBase {
  use DatabaseMigrations;

  public function testUserConversationsList() {
    $users = $this->createUsers(['browncoat']);
    $conversation = $this->createConversationWithUsers(['title' => 'Test Conversation'], $users);

    Sentinel::login($conversation->creator);

    $this->visit('/user/'.$conversation->creator->username.'/conversations')->see('Test Conversation');
    $this->get('/user/'.$conversation->creator->username.'/conversations', ['Accept' => 'application/json'])->seeJson([
      'title' => 'Test Conversation',
    ]);
  }

  public function testConversationUsersList() {
    $users = $this->createUsers(3);
    $conversation = $this->createConversationWithUsers(['title' => 'Test Conversation'], $users);

    Sentinel::login($conversation->creator);

    $this->visit('/conversations/'.$conversation->id)
         ->see($conversation->creator->username)
         ->see($users[1]->username)
         ->see($users[2]->username);

    $this->get('/conversations/'.$conversation->id, ['Accept' => 'application/json'])
         ->seeJson(['username' => $conversation->creator->username])
         ->seeJson(['username' => $users[1]->username])
         ->seeJson(['username' => $users[2]->username]);
  }
}
