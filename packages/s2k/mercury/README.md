# Mercury Messenger

_User_ (Sentinel)
UserConversationRelationship - conversation_id user_id

Conversation - id title type status
* list all conversations
* list filtered conversations
* create one conversation
* add users to one conversation
* view one conversation
* change the status of one conversation

ConversationEvent - id conversation_id type message detail data
* list all conversation events
* list filtered conversation events
* create one conversation event

Attachment - id type object status
* create one attachment
* retrieve one attachment

AttachmentPolymorphicRelationship - polymorphic_id polymorphic_model attachment_id
