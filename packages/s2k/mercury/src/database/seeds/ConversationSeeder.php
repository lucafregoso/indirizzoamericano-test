<?php

use App\User;
use S2K\Mercury\Conversation;
use S2K\Mercury\ConversationEvent;

class ConversationSeeder extends DatabaseSeeder {
	public function run() {
		DB::table('conversations')->truncate();
		DB::table('conversation_user')->truncate();
		DB::table('conversation_events')->truncate();

		$usernames = ['admin', 'warehouse', 'shopper', 'user'];
		$mainUsers = User::findByUsername(...$usernames);
		$allUsers = User::all();

		foreach ($mainUsers as $mainUser) {
			factory(Conversation::class, 15)->create([
				'creator_id' => $mainUser->id,
			])->each(function($conversation) use ($mainUsers, $mainUser) {
				// Add Users
				$randomUsers = $mainUsers->random(rand(2, 3));
				$conversation->addUser(...$randomUsers);

				// Create Events
				factory(ConversationEvent::class, rand(2, 6))->create([
					'conversation_id' => $conversation->id,
				])->each(function($conversationEvent) use ($conversation) {
					$eventUser = (bool)random_int(0, 1) ? $conversation->users->random() : $conversation->creator;

					$conversationEvent->user()->associate($eventUser);
					$conversationEvent->save();
				});
			});
		}

		$this->command->info('Created 50 random Conversations assigned to the Main Users.');

		factory(Conversation::class, 250)->create([
			'creator_id' => $allUsers->random()->id,
		])->each(function($conversation) use ($allUsers) {
			$randomUsers = $allUsers->random(rand(2, 6));
			$conversation->addUser(...$randomUsers);

			// Create Events
			factory(ConversationEvent::class, rand(2, 6))->create([
				'conversation_id' => $conversation->id,
			])->each(function($conversationEvent) use ($conversation) {
				$eventUser = (bool)random_int(0, 1) ? $conversation->users->random() : $conversation->creator;

				$conversationEvent->user()->associate($eventUser);
				$conversationEvent->save();
			});
		});

		$this->command->info('Created 250 random Conversations assigned to random Users.');
	}
}
