<?php

use S2K\Mercury\ConversationEvent;

$factory->define(ConversationEvent::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->paragraphs(4, true),
        'detail' => $faker->sentence,
        'type' => 'message',
        'data' => serialize($faker->creditCardDetails)
    ];
});
