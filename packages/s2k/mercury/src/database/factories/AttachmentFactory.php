<?php

use S2K\Mercury\Attachment;

$factory->define(Attachment::class, function (Faker\Generator $faker) {
  return [
    'type' => $faker->randomElement(['file', 'picture']),
    'status' => $faker->randomElement(['relevant', 'useless']),
    'description' => $faker->sentence,
  ];
});
