<?php

use S2K\Mercury\Conversation;

$factory->define(Conversation::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'type' => $faker->randomElement(['user', 'shopper', 'admin']),
        'status' => $faker->randomElement(['new', 'active', 'updated', 'resolved', 'closed']),
    ];
});
