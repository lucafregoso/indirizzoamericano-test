@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Attachment List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
  <h1>Conversations</h1>
  @include('mercury::admin.breadcrumb', [
    'breadcrumbs' => [
      [
        'title' => 'Dashboard',
        'link' => route('admin.dashboard'),
      ],
      [
        'title' => 'Attachments',
        'active' => true
      ]
    ]
  ])
</section>

<section class="content paddingleft_right15">
  <div class="row">
    <div class="panel panel-primary ">
      <div class="panel-heading clearfix">
          <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
              Attachment List
          </h4>
      </div>
      <br />
      <div class="panel-body table-responsive">
        <table class="table table-bordered" id="attachments-table">
            <thead>
             <tr class="filters">
              <th>ID</th>
              <th>Name</th>
              <th>File</th>
              <th>Parent</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>UUIC</th>
              <th>Creator</th>
              <th>Created At</th>
              <th>Actions</th>
             </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
    </div>
  </div>    <!-- row-->
</section>
@stop

{{-- Body Bottom confirm modal --}}
@section('footer_scripts')

  <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

  <script>
      $(function() {
          var table = $('#attachments-table').DataTable({
              processing: true,
              serverSide: true,
              ajax: {
                  url: '{!! route("mercury::admin.attachments.data") !!}'
              },
              columns: [
                  { data: 'id', name: 'id' },
                  { data: 'originalname', name: 'originalname'},
                  { data: 'file', name: 'file'},
                  { data: 'parent_data', name: 'parent_data'},
                  { data: 'user.first_name', name: 'user.first_name', "visible": false},
                  { data: 'user.last_name', name: 'user.last_name', "visible": false},
                  { data: 'user.uuic', name: 'user.uuic', "visible": false},
                  { data: 'user.email', name: 'user.email', "visible": false},
                  { data: 'user_data', name: 'user_data'},
                  { data: 'created_at', name:'created_at'},
                  { data: 'actions', name: 'actions', orderable: false, searchable: false }
              ]
          });
          table.on( 'draw', function () {
              $('.livicon').each(function(){
                  $(this).updateLivicon();
              });
          } );
      });

  </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
