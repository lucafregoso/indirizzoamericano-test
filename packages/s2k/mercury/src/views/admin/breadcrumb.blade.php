<ol class="breadcrumb">
  @foreach ($breadcrumbs as $breadcrumb)
    <li class="{!! (isset($breadcrumb['active']) && $breadcrumb['active']) ? 'active' : '' !!}">
      @if (isset($breadcrumb['link']) && $breadcrumb['link'])
        <a href="{{ $breadcrumb['link'] }}">
          {{ $breadcrumb['title'] }}
        </a>
      @else
        {{ $breadcrumb['title'] }}
      @endif
    </li>
  @endforeach
</ol>
