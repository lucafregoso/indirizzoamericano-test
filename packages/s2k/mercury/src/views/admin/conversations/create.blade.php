@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Create New Conversation
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Conversations</h1>
    @include('mercury::admin.breadcrumb', [
      'breadcrumbs' => [
        [
          'title' => 'Dashboard',
          'link' => route('admin.dashboard'),
        ],
        [
          'title' => 'Conversations',
          'link' => route('mercury::admin.conversations.index'),
        ],
        [
          'title' => 'Create New Conversation',
          'active' => true,
        ]
      ]
    ])
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Create a new conversation
                    </h4>
                </div>
                <div class="panel-body">
                     @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open(['url' => 'admin/conversations', 'files'=>true]) !!}
                        @include('mercury::admin.conversations.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript">
        $("#select-user").select2({
            theme:"bootstrap",
            placeholder:"Select user..."
        });

        $("#select-reference").select2({
            theme:"bootstrap",
            placeholder:"Select reference..."
        });
    </script>
@stop
