<div class="form-group col-sm-12">
    {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('type', 'Type:', ['class' => 'control-label']) !!}
    {!! Form::select('type', array('administration' => 'Administration', 'personal shopper' => 'Personal Shopper', 'warehouse assistant' => 'Warehouse Assistant'), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:', ['class' => 'control-label']) !!}
    {!! Form::select('status', array('new' => 'New', 'closed' => 'Closed'), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('user_id', 'Recipients:') !!}
    <select class="form-control select2" id="select-user" name="users[]" multiple='multiple'>
        @foreach($users as $user)
            <option value="{{$user->id}}" {{ (isset($participants) && $participants->contains('id', $user->id)) ? 'selected' : '' }}>{{$user->uuic}} - {{$user->first_name}} {{$user->last_name}} - {{$user->email}}</option>
        @endforeach
    </select>
</div>

<div class="form-group col-sm-12">
    <label for="select-reference" class="control-label">
        Reference:
    </label>
    <select id="select-reference" class="form-control select2" name="reference">
        <option value="">Select value...</option>
        <optgroup label="Pacchi">
        @foreach ($packages as $package)
            <option value="packages-{{ $package->id }}" {{ (isset($package_reference) && $package_reference->contains('id', $package->id)) ? 'selected' : '' }}>{{ $package->barcode }}</option>
        @endforeach
        </optgroup>
        <optgroup label="Ordini">
            @foreach ($orders as $order)
                <option value="orders-{{ $order->id }}" {{ (isset($order_reference) && $order_reference->contains('id', $order->id)) ? 'selected' : '' }}>{{ $order->id }}</option>
            @endforeach
        </optgroup>
    </select>
</div>

@if (!Request::is('admin/conversations/*/edit'))
    <div class="form-group col-sm-12">
        {!! Form::label('content', 'Message:', ['class' => 'control-label']) !!}
        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>
@endif

{{-- <div class="form-group  col-sm-12">
{!! Form::label('attachment', 'Attachment:', ['class' => 'control-label']) !!}
@if (isset($attachment))
    <a target="_blank" href="{{ url('/').'/uploads/files/'.App\Helpers\Helper::getFilePath($attachment->filename).$attachment->filename.'.'.$attachment->extension }}" >
     {{ $attachment->originalname }}
    </a>
@endif
{!! Form::file('attachment') !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.services.index') !!}" class="btn btn-default">Cancel</a>
</div>