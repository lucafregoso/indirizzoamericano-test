@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Conversation List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
  <h1>Conversations</h1>
  @include('mercury::admin.breadcrumb', [
    'breadcrumbs' => [
      [
        'title' => 'Dashboard',
        'link' => route('admin.dashboard'),
      ],
      [
        'title' => 'Conversations',
        'active' => true
      ]
    ]
  ])
</section>

<section class="content paddingleft_right15">
  <div class="row">
    <div class="panel panel-primary ">
      <div class="panel-heading clearfix">
          <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
              Conversation List
          </h4>
          <div class="pull-right">
              <a href="{{ route('mercury::admin.conversations.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
          </div>
      </div>
      <br />
      <div class="panel-body">
          <form method="POST" id="search-form" class="form-inline" role="form">
              <div class="form-group">
                  <label for="status">Filter Status</label>
                  <select name="status" id="status" class="form-control">
                      <option value="">All</option>
                      <option value="new">New</option>
                      <option value="closed">Closed</option>
                      <option value="resolved">Resolved</option>
                  </select>
              </div>
              <button type="submit" class="btn btn-primary">Search</button>
          </form>
      </div>

      <div class="panel-body table-responsive">

        <table class="table table-bordered" id="conversations-table">
            <thead>
             <tr class="filters">
              <th>ID</th>
              <th>Title</th>
              <th>Type</th>
              <th>Status</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>UUIC</th>
              <th>Creator</th>
              <th>Created At</th>
              <th>Actions</th>
             </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
    </div>
  </div>    <!-- row-->
</section>
@stop

{{-- Body Bottom confirm modal --}}
@section('footer_scripts')

  <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

  <script>
      $(function() {
          var route = '{!! route("mercury::admin.conversations.data") !!}';
          @if (Request::is('admin/conversations/lists/new'))
              route = '{!! route('mercury::admin.conversations.new.data') !!}';
          @endif
          var table = $('#conversations-table').DataTable({
              processing: true,
              serverSide: true,
              ajax: {
                  url: route,
                  data: function (d) {
                      d.status = $('select[name=status]').val();
                  }
              },
              columns: [
                  { data: 'id', name: 'id' },
                  { data: 'title', name: 'title'},
                  { data: 'type', name: 'type'},
                  { data: 'status', name: 'status'},
                  { data: 'creator.first_name', name: 'creator.first_name', "visible": false},
                  { data: 'creator.last_name', name: 'creator.last_name', "visible": false},
                  { data: 'creator.uuic', name: 'creator.uuic', "visible": false},
                  { data: 'creator.email', name: 'creator.email', "visible": false},
                  { data: 'creator_data', name: 'creator_data'},
                  { data: 'created_at', name:'created_at'},
                  { data: 'actions', name: 'actions', orderable: false, searchable: false }
              ]
          });
          table.on( 'draw', function () {
              $('.livicon').each(function(){
                  $(this).updateLivicon();
              });
          } );

          $('#search-form').on('submit', function(e) {
              table.draw();
              e.preventDefault();
          });
      });

  </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
