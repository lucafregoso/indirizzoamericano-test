@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Conversation
@parent
@stop

@section('content')
<section class="content-header">
    <h1>Conversations</h1>
    @include('mercury::admin.breadcrumb', [
      'breadcrumbs' => [
        [
          'title' => 'Dashboard',
          'link' => route('admin.dashboard'),
        ],
        [
          'title' => 'Conversations',
          'link' => route('mercury::admin.conversations.index'),
        ],
        [
          'title' => $conversation->title,
          'active' => true,
        ]
      ]
    ])
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Conversation {{ $conversation->title }} details
                </h4>
            </div>
            <br />
            <div class="panel-body">
                <h2 class="text-center"> {{ $conversation->title }}</h2><br><br>
                <div class="col-lg-12">

                    <div class="col-lg-3">
                        <p><strong>Conversation ID :</strong> {{ $conversation->id }}</p>
                        <p><strong>Creator :</strong> <a target="_blank" href="{{ route('admin.users.show', $conversation->creator->id) }}"> {!! $conversation->creator->uuic .' - '.$conversation->creator->first_name .' '. $conversation->creator->last_name !!}</a></p>
                        <p><strong>Date :</strong> {{ $conversation->created_at->diffForHumans() }}</p>
                    </div>
                    <div class="col-lg-3">
                        <p><strong>Reference : </strong></p>
                        @if ($conversation->packages()->count())
                                <a target="_blank" href="{{ route('admin.packages.show', $conversation->packages()->first()->id) }}">{{ $conversation->packages()->first()->barcode }}</a>
                        @elseif ($conversation->orders()->count())
                                <a target="_blank" href="{{ route('admin.orders.show', $conversation->orders()->first()->id) }}">{{ $conversation->orders()->first()->id }}</a>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <p><strong>Participants : </strong>
                        @foreach($conversation->users as $participant)
                            <br>
                            <a target="_blank" href="{{ route('admin.users.show', $participant->id) }}"> {!! $participant->uuic .' - '.$participant->first_name .' '. $participant->last_name !!}</a>
                        @endforeach
                        </p>
                    </div>
                    <div class="col-lg-3">
                        <p><strong>Attachments : </strong>
                            @if ($conversation->attachments->count())
                                @foreach ($conversation->attachments as $attachment)
                                <br>
                                <a target="_blank" href="{{ url('/').'/uploads/files/'.App\Helpers\Helper::getFilePath($attachment->filename).$attachment->filename.'.'.$attachment->extension }}" >
                                    {{ $attachment->originalname }}
                                </a>
                                @endforeach
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Conversation Event's
                </h4>
            </div>
            <br />
            <div class="panel-body">
                @foreach ($conversation->events as $event)
                    
                    @if ($event->type != 'message')
                        <p class="text-center">
                            <a target="_blank" href="{{ route('admin.users.show', $event->user->id) }}"> {!! $event->user->uuic .' - '.$event->user->first_name .' '. $event->user->last_name !!}</a>
                            <strong>{{ $event->detail }}</strong> <br>
                            {{ $event->created_at->diffForHumans() }}
                        </p>
                    @endif

                    @if ($event->type == 'message')
                        @if ($event->user_id != $conversation->creator->id)
                            <p class="text-left">
                        @else
                            <p class="text-right">
                        @endif
                                <a target="_blank" href="{{ route('admin.users.show', $event->user->id) }}"> {!! $event->user->uuic .' - '.$event->user->first_name .' '. $event->user->last_name !!}</a>
                                @if ($event->detail)
                                    <br><br>
                                    <strong>{{ $event->detail }}</strong> 
                                @endif
                                <br><br>
                                <strong>{!! nl2br(e($event->content)) !!}</strong> <br><br>
                                @if ($event->attachments->count())
                                    <strong>Attachments:</strong>
                                    @foreach ($event->attachments as $attachment)
                                            <a target="_blank" href="{{ url('/').'/uploads/files/'.App\Helpers\Helper::getFilePath($attachment->filename).$attachment->filename.'.'.$attachment->extension }}" >
                                            {{ $attachment->originalname }}
                                            </a>
                                        </li>
                                        @endforeach
                                @endif
                                <br>
                                {{ $event->created_at->diffForHumans() }}
                            </p>
                    @endif
                    <hr>
                @endforeach

            </div>
        </div>

        <div class="panel panel-primary ">
            <div class="panel-body">
                <div>
                  {!! Form::open([
                    'route' => ['mercury::admin.conversations.events.store', $conversation->id],
                    'files' => true,
                  ]) !!}

<!--                     <div class="form-group">
                      {!! Form::label('detail', 'Title:', ['class' => 'control-label']) !!}
                      {!! Form::text('detail', null, ['class' => 'form-control']) !!}
                    </div> -->

                    <div class="form-group">
                      {!! Form::label('content', 'Message:', ['class' => 'control-label']) !!}
                      {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
                    </div>

                    {{-- <div class="form-group">
                      {!! Form::label('attachment', 'Attachment:', ['class' => 'control-label']) !!}
                      {!! Form::file('attachment') !!}
                    </div> --}}

                    {!! Form::hidden('conversation_id', $conversation->id) !!}
                    {!! Form::submit('Reply', ['class' => 'btn btn-primary']) !!}

                  {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>
@stop
