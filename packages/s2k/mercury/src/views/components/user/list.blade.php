<div>
  <h4>Users</h4>
  <ul>
    <li><strong>{{ $conversation->creator->id }} - {{ $conversation->creator->first_name }} {{ $conversation->creator->last_name }}</strong></li>
    @foreach ($conversation->users as $user)
      <li>
        {{ $user->id }} - {{ $user->first_name }} {{ $user->last_name }}
      </li>
    @endforeach
  </ul>
</div>
