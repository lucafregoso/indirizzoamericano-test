<div>
  <h4>{{ $title or "Attachments:" }}</h4>
  <ul>
    @foreach ($attachments as $attachment)
      <li>
        <a target="_blank" href="{{ url('/').'/uploads/files/'.App\Helpers\Helper::getFilePath($attachment->filename).$attachment->filename.'.'.$attachment->extension }}" >
          {{ $attachment->originalname }}
        </a>
      </li>
    @endforeach
  </ul>
</div>
