<div>
  <h2>New Conversation</h2>
  {!! Form::open([
    'route' => 'mercury::conversation.store',
    'files' => true,
  ]) !!}

    <div class="form-group">
      {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
      {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('type', 'Type:', ['class' => 'control-label']) !!}
      {!! Form::select('type', array('administration' => 'Administration', 'personal shopper' => 'Personal Shopper', 'warehouse assistant' => 'Warehouse Assistant'), null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="select21" class="control-label">
            Reference:
        </label>
        <select id="select21" class="form-control select2" name="reference">
            <option value="">Select value...</option>
            <optgroup label="Pacchi">
            @foreach ($packages as $package)
                <option value="packages-{{ $package->id }}">{{ $package->barcode }}</option>
            @endforeach
            </optgroup>
            <optgroup label="Ordini">
                @foreach ($orders as $order)
                  <option value="orders-{{ $order->id }}">{{ $order->id }}</option>
                @endforeach
            </optgroup>
        </select>
    </div>

    <div class="form-group">
      {!! Form::label('content', 'Message:', ['class' => 'control-label']) !!}
      {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>

    {{-- <div class="form-group">
      {!! Form::label('attachment', 'Attachment:', ['class' => 'control-label']) !!}
      {!! Form::file('attachment') !!}
    </div> --}}

    {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}

  {!! Form::close() !!}
</div>
