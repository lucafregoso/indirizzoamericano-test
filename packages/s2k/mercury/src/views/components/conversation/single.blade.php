<span>
  {!! link_to_route('mercury::conversation.show', $conversation->id.' '.$conversation->title, ['conversation_id' => $conversation->id]) !!}
</span>
<span>&#151;</span>
<span>
  {{ $conversation->updated_at->diffForHumans() }}
</span>
@if ($conversation->status === 'closed')
  (<strong>CLOSED</strong>)
@endif
