  <span>
    {{ $event->id }} -> <strong>{{ $event->detail }}</strong> by <em>{{ $event->user->username }}</em> - 
    <span title="{{ $event->created_at->toDayDateTimeString() }}">
      {{ $event->created_at->diffForHumans() }}
    </span>
  </span>
  <br>

  @if ($event->content)
    <em>Message: </em>{{ $event->content }}
  @endif
  <br>

  @if ($event->attachments->count())
    @include('mercury::components.attachment.list', [
      'attachments' => $event->attachments
    ])
  @endif

