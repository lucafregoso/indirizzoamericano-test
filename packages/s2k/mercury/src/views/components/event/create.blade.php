<div>
  <h2>New Message</h2>
  {!! Form::open([
    'route' => ['mercury::conversation.events.store', $conversation->id],
    'files' => true,
  ]) !!}

    <!-- <div class="form-group">
      {!! Form::label('detail', 'Title:', ['class' => 'control-label']) !!}
      {!! Form::text('detail', null, ['class' => 'form-control']) !!}
    </div> -->

    <div class="form-group">
      {!! Form::label('content', 'Message:', ['class' => 'control-label']) !!}
      {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('attachment', 'Attachment:', ['class' => 'control-label']) !!}
      {!! Form::file('attachment') !!}
    </div>

    {!! Form::hidden('conversation_id', $conversation->id) !!}
    {!! Form::submit('Reply', ['class' => 'btn btn-primary']) !!}

  {!! Form::close() !!}
</div>
