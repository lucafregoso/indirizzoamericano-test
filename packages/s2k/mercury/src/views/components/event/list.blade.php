<div>
  <h4>Events</h4>
  <ul>
    @foreach ($conversation->events as $event)
      <li>
        @include('mercury::components.event.single', [
          'event' => $event
        ])
      </li>
    @endforeach
  </ul>
</div>
