@extends('layouts/default')

{{-- Page title --}}
@section('title')
Tickets
@parent
@stop

{{-- content --}}
@section('content')


<div class="wrap spaceTB30 cf">
    <div class="columns spaceB20">
        <div class="column m-all t-all d-all text-center">
            <h1 class="bold italic">I tuoi ticket</h1>
            <a href="{{ route('mercury::conversation.new')}}">Crea Ticket</a>
        </div>
    </div>
</div>

<div class="wrap spaceB30 cf">
    <div class="columns spaceB10">
        <div class="column m-1of12 t-1of12 d-1of12">

        </div>
        <div class="column m-11of12 t-11of12 d-11of12">
            <div class="tickets-header hidden-xs">
                <div class="columns">
                    <div class="column m-1of12 t-1of4 d-1of4">
                        <h5 class="bold italic">Tipologia di ticket</h5>
                    </div>
                    <div class="column m-1of12 t-1of4 d-1of3">
                        <h5 class="bold italic">Oggetto</h5>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        <h5 class="bold italic">Stato del ticket</h5>
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        <h5 class="bold italic">Risposte</h5>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        <h5 class="bold italic">data</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{-- Conversations open --}}
@foreach ($totalConversations as $conversation)
    <div class="columns spaceB10 column-middle">
        <div class="column m-1of6 t-1of12 d-1of12 text-center ">
            <span class="ticket-alert">!</span>
        </div>
        <div class="column m-5of6 t-11of12 d-11of12">
            <div class="ticket-collapsed">
                <div class="columns column-middle">
                    <div class="column-no-pad m-all t-1of4 d-1of4">
                        <a href="{{ route('mercury::conversation.show', $conversation['id']) }}">
                            @if ($conversation->packages()->count())
                                <div class="ticket-open ticket-pacco ticket-reference">
                                    Ticket Pacco <br/>
                                    n° {{ $conversation->packages()->first()->barcode }}
                                </div>
                            @elseif ($conversation->orders()->count())
                            <div class="ticket-open ticket-shipping ticket-reference">
                                Ticket Spedizione <br/>
                                n° {{ $conversation->orders()->first()->id }}
                            </div>
                            @else
                                <div class="ticket-open ticket-pacco ticket-reference">
                                    Ticket<br/>
                                    n° {{ $conversation->id }}
                                </div>
                            @endif
                        </a>
                    </div>
                    <div class="column m-all t-1of4 d-1of3">
                        <p class="">
                            {{ $conversation->title }}
                        </p>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        {{ $conversation->status }}
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        {{ $conversation->events()->where('type', 'message')->count() }} post
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        {{ Carbon\Carbon::parse($conversation->created_at)->format('d/m/Y') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach


@foreach ($closedConversations as $conversation)
    <div class="columns spaceB10 column-middle">
        <div class="column m-1of6 t-1of12 d-1of12 text-center">
            <span class="ticket-ok"></span>
        </div>
        <div class="column m-5of6 t-11of12 d-11of12">
            <div class="ticket-collapsed">
                <div class="columns column-middle">
                    <div class="column-no-pad m-all t-1of4 d-1of4">
                        <a href="{{ route('mercury::conversation.show', $conversation['id']) }}">
                            @if ($conversation->packages()->count())
                                <div class="ticket-closed ticket-pacco ticket-reference">
                                    Ticket Pacco <br/>
                                    n° {{ $conversation->packages()->first()->barcode }}
                                </div>
                            @elseif ($conversation->orders()->count())
                            <div class="ticket-open ticket-shipping ticket-reference">
                                Ticket Spedizione <br/>
                                n° {{ $conversation->orders()->first()->id }}
                            </div>
                            @endif
                        </a>
                    </div>
                    <div class="column m-all t-1of4 d-1of3">
                        <p class="">
                            {{ $conversation->title }}
                        </p>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        {{ $conversation->status }}
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        {{ $conversation->events()->count() }} post
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        {{ Carbon\Carbon::parse($conversation->created_at)->format('d/m/Y') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach


</div>
@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
