@extends('layouts/default')

{{-- Page title --}}
@section('title')
Tickets
@parent
@stop

{{-- content --}}
@section('content')
<div class="wrap spaceTB30 cf">
    <div class="columns spaceB20">
        <div class="column m-all t-all d-all text-center">
            <h2 class="bold italic spaceB30">Questo ticket contiene {{ $conversation->events()->where('type', 'message')->count() }} post, ultimo in data {{ Carbon\Carbon::parse($conversation->events()->where('type', 'message')->orderBy('created_at', 'desc')->first()->created_at)->format('d/m/Y') }}</h2>
            <a href="{{ route('mercury::user.conversations')}}" class="bold italic">< Torna all'elenco dei ticket</a>
        </div>
    </div>
</div>

<div class="wrap spaceB30 cf">
    <div class="ticket-head">
        <div class="columns spaceTB10 column-middle">
            <div class="column m-all t-1of4 d-2of3">
                {{ $conversation->title }}
            </div>
            <div class="column-no-pad m-1of2 t-1of4 d-1of4">
                @if ($conversation->packages()->count())
                    <div class="ticket-pacco ticket-reference">
                        Ticket Pacco n° {{ $conversation->packages()->first()->barcode }}
                    </div>
                @elseif ($conversation->orders()->count())
                    <div class="ticket-shipping ticket-reference">
                        Ticket Spedizione n° {{ $conversation->orders()->first()->id }}
                    </div>
                @endif
            </div>
            <div class="column m-1of2 t-1of12 d-1of12 text-center">
                @if ($conversation->status == 'closed')
                    <span class="ticket-ok"></span>
                @else
                    <span class="ticket-alert">!</span>
                @endif
                
            </div>
        </div>
    </div>

    @foreach ($conversation->events as $event)
    @if ($event->type == 'message')
        @if ($event->user->id === $user->id)

            <div class="ticket-first">
                <div class="columns spaceTB10">
                    <div class="column m-1of5 t-1of12 d-1of12 text-center">
                        <div class="storage-profile-img">
                            <img src="{{ asset('assets/library/images/storage/profile.png') }}" alt=""/>
                        </div>
                    </div>
                    <div class="column m-4of5 t-11of12 d-11of12">
                        <div class="ticket-user">{{ '@'.$user->first_name }}.{{ $user->last_name }}</div>
                        <div class="ticket-date">{{ Carbon\Carbon::parse($event->created_at)->format('d/m/Y H:i') }}</div>
                        <div class="ticket-text-cont">
                            @if ($event->content)
                                {{ $event->content }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="ticket-answer">
                <div class="columns spaceTB10">
                    <div class="column m-1of5 t-1of12 d-1of12 text-center">
                        <div class="ticket-profile-img">
                            <img src="{{ asset('assets/library/images/indirizzo-americano-small.png') }}" alt=""/>
                        </div>
                    </div>
                    <div class="column m-4of5 t-11of12 d-11of12">
                        <div class="ticket-user">Indirizzo Americano</div>
                        <div class="ticket-date">{{ Carbon\Carbon::parse($event->created_at)->format('d/m/Y H:i') }}</div>
                        <div class="ticket-text-cont">
                            @if ($event->content)
                                {{ $event->content }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif

    @endif
    @endforeach

    @if ($conversation->status != 'closed')
        @if ( $conversation->users->contains($user->id) || $conversation->creator_id == $user->id )
            <div class="ticket-answer-form">
                <div class="columns spaceTB10">
                    <div class="column m-all t-all d-all">
                        <div class="bold italic spaceB10">Rispondi a Indirizzo Americano</div>
                        {!! Form::open(['route' => ['mercury::conversation.events.store', $conversation->id],'files' => true]) !!}
                        {!! Form::textarea('content', null, ['class' => 'form-control pack-value-num-input', 'placeholder' => 'Scrivi qui la tua risposta.']) !!}
                    </div>
                </div>
                <div class="columns spaceB10">
                    <div class="column m-1of2 t-1of2 d-1of2">
                        <label class="control-label cf">
                            {!! Form::checkbox('email_notifications', 1, $conversation->email_notifications == 1 ? true : false , ['class' => 's2k-input-checkbox form-control']) !!}
                            <span class="s2k-input-checkbox-style to-left"></span>
                            <span class="to-left checkbox-text spaceT5">Avvisami delle risposte via mail</span>
                        </label>
                    </div>
                    <div class="column m-1of2 t-1of2 d-1of2 text-right">
                        <!-- Form actions -->
                        {!! Form::hidden('conversation_id', $conversation->id) !!}
                        {!! Form::submit('Invia', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endif
    @endif

</div>
@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
