@extends('layouts/default')

{{-- Page title --}}
@section('title')
Conversations
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/portfolio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <!-- <div class="breadcum">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="hidden-xs">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Portfolio</a>
                </li>
            </ol>
            <div class="pull-right">
                <i class="livicon icon3" data-name="briefcase" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Portfolio
            </div>
        </div>
    </div> -->
@stop

{{-- Page content --}}
@section('content')
  @include('flash::message')

  {{-- New Conversations --}}
  <a href="{{ route('mercury::conversation.new')}}">New Conversation</a>

  {{-- Created Conversations --}}
  @if ($createdConversations->count())
    <h2>Owned Conversations</h2>
    <ul>
      @foreach ($createdConversations as $conversation)
        <li>
          @include('mercury::components.conversation.single', [
            'conversation' => $conversation
          ])
        </li>
      @endforeach
    </ul>
  @else
    <p>
      <strong>No Owned Conversations</strong>
    </p>
  @endif

  <hr>

  {{-- Participating Conversations --}}
  @if ($participatingConversations->count())
    <h2>Other Conversations</h2>
    <ul>
      @foreach ($participatingConversations as $conversation)
        <li>
          @include('mercury::components.conversation.single', [
            'conversation' => $conversation
          ])
        </li>
      @endforeach
    </ul>
  @else
    <p>
      <strong>No Other Conversations</strong>
    </p>
  @endif

  <hr>

  {{-- Closed Conversations --}}
  @if ($closedConversations->count())
    <h2>Closed Conversations</h2>
    <ul>
      @foreach ($closedConversations as $conversation)
        <li>
          @include('mercury::components.conversation.single', [
            'conversation' => $conversation
          ])
        </li>
      @endforeach
    </ul>
  @else
    <p>
      <strong>No Closed Conversations</strong>
    </p>
  @endif
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/vendors/mixitup/jquery.mixitup.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-media.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/portfolio.js') }}"></script>
    <!--page level js ends-->

@stop
