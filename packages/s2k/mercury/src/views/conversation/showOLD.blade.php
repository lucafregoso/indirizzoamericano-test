@extends('layouts/default')

{{-- Page title --}}
@section('title')
Conversations
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/portfolio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <!-- <div class="breadcum">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="hidden-xs">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Portfolio</a>
                </li>
            </ol>
            <div class="pull-right">
                <i class="livicon icon3" data-name="briefcase" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Portfolio
            </div>
        </div>
    </div> -->
@stop


{{-- Page content --}}
@section('content')
  @include('flash::message')
  <h2>{{ $conversation->title }}</h2>
  <p>Status: <strong>{{ $conversation->status }}</strong> 
  @if ($conversation->status != 'closed') 
    @if ($conversation->status != 'resolved')
      ({!! link_to_route('mercury::conversation.resolve', 'Mark as Resolved', ['conversation_id' => $conversation->id]) !!})</p>
    @else
      ({!! link_to_route('mercury::conversation.restore', 'Reopen', ['conversation_id' => $conversation->id]) !!})</p>
    @endif
  @endif
  
  <p>Type: <strong>{{ $conversation->type }}</strong></p>

  {{-- Attachments --}}
  @if ($conversation->attachments->count())
    @include('mercury::components.attachment.list', [
      'title' => 'Conversation Attachments',
      'attachments' => $conversation->attachments
    ])
  @endif

  {{-- Users --}}
  @include('mercury::components.user.list', [
    'conversation' => $conversation
  ])

  {{-- Create Event --}}
  @if ($conversation->status != 'closed')
    @if ( $conversation->users->contains($user->id) || $conversation->creator_id == $user->id )
      @include('mercury::components.event.create', [
        'conversation' => $conversation
      ])
    @endif
  @endif

  {{-- Events --}}
  @if ($conversation->events->count())
    @include('mercury::components.event.list', [
      'conversation' => $conversation
    ])
  @else
    <p>
      No Events, Currently
    </p>
  @endif
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/vendors/mixitup/jquery.mixitup.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-media.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/portfolio.js') }}"></script>
    <!--page level js ends-->

@stop
