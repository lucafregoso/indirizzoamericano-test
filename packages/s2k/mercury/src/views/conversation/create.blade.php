@extends('layouts/default')

{{-- Page title --}}
@section('title')
Tickets
@parent
@stop

{{-- content --}}
@section('content')

<div class="wrap spaceTB30 cf">
    <div class="columns spaceB20">
        <div class="column m-all t-all d-all text-center">
            <h1 class="bold italic">Apri un ticket</h1>
            <a href="{{ route('mercury::user.conversations')}}">< Torna all'elenco dei ticket</a>
        </div>
    </div>
</div>


<div class="wrap spaceB30 cf">
    <div class="ticket-answer-form">
        <div class="columns spaceTB10">
            <div class="column m-all t-all d-all">
                <div class="bold italic spaceB10">Crea il tuo ticket</div>
                {!! Form::open(['route' => 'mercury::conversation.store', 'files' => true]) !!}
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Oggetto']) !!}
            </div>
        </div>
        <div class="columns spaceB10">
            <div class="column m-1of2 t-1of2 d-1of2">
                {!! Form::select('type', array('administration' => 'Amministrazione', 'personal shopper' => 'Personal Shopper', 'warehouse assistant' => 'Magazziniere'), null, ['class' => 'form-control']) !!}
            </div>
            <div class="column m-1of2 t-1of2 d-1of2">
                    <div class="form-group">
                        <select id="select21" class="form-control select2" name="reference">
                            <option value="">Seleziona pacco o ordine...</option>
                            <optgroup label="Pacchi">
                                @foreach ($packages as $package)
                                    <option value="packages-{{ $package->id }}">{{ $package->barcode }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Ordini">
                                @foreach ($orders as $order)
                                    <option value="orders-{{ $order->id }}">{{ $order->id }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
            </div>
        </div>
        <div class="columns spaceB10">
            <div class="column m-all t-all d-all">
                {!! Form::textarea('content', null, ['class' => 'form-control pack-value-num-input', 'placeholder' => 'Scrivi qui la tua risposta.']) !!}
            </div>
        </div>
        <div class="columns spaceB10">
            <div class="column m-1of2 t-1of2 d-1of2">
                <label class="control-label cf">
                    {!! Form::checkbox('email_notifications', 1, null, ['class' => 's2k-input-checkbox form-control']) !!}
                    <span class="s2k-input-checkbox-style to-left"></span>
                    <span class="to-left checkbox-text spaceT5">Avvisami delle risposte via mail</span>

                    {{-- <div class="form-group">
                        {!! Form::label('attachment', 'Attachment:', ['class' => 'control-label']) !!}
                        {!! Form::file('attachment') !!}
                    </div> --}}
                </label>
            </div>
            <div class="column m-1of2 t-1of2 d-1of2 text-right">
                <!-- Form actions -->
                {!! Form::submit('Invia', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
