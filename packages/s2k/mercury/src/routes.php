<?php

use S2K\Mercury\Middlewares\LoggedInMiddleware;
use S2K\Mercury\Middlewares\AccessibleConversationMiddleware;
use S2K\Mercury\Attachment;

#############
# Adimn
#############
Route::group(array('middleware' => ['web', 'SentinelAdmin'], 'as' => 'mercury::admin.', 'prefix' => 'admin'), function () {

    # Conversations
    Route::get('conversations',                      ['as' => 'conversations.index', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@index']);
    Route::post('conversations',                     ['as' => 'conversations.store', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@store']);
    Route::get('conversations/data',                 ['as' => 'conversations.data',  'uses' =>'S2K\Mercury\Controllers\Admin\ConversationController@data']);
    Route::get('conversations/create',               ['as' => 'conversations.create', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@create']);
    Route::put('conversations/{conversations}',      ['as' => 'conversations.update', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@update']);
    Route::patch('conversations/{conversations}',    ['as' => 'conversations.update', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@update']);
    Route::get('conversations/{id}/delete',          ['as' => 'conversations.delete', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@getDelete']);
    Route::get('conversations/{id}/confirm-delete',  ['as' => 'conversations.confirm-delete', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@getModalDelete']);
    Route::get('conversations/{conversations}',      ['as' => 'conversations.show', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@show']);
    Route::get('conversations/{conversations}/edit', ['as' => 'conversations.edit', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@edit']);

    Route::get('conversations/lists/new/data',['as' => 'conversations.new.data', 'uses' =>'S2K\Mercury\Controllers\Admin\ConversationController@newConversationsData']);
    Route::get('conversations/lists/new', ['as'=> 'conversations.lists.new', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationController@index']);

    # Attachment
    Route::get('attachments',                        ['as' => 'attachments.index', 'uses' => 'S2K\Mercury\Controllers\Admin\AttachmentController@index']);
    Route::get('attachments/data',                   ['as' => 'attachments.data',  'uses' =>'S2K\Mercury\Controllers\Admin\AttachmentController@data']);
    Route::get('attachments/{id}/delete',            ['as' => 'attachments.delete', 'uses' => 'S2K\Mercury\Controllers\Admin\AttachmentController@getDelete']);
    Route::get('attachments/{id}/confirm-delete',    ['as' => 'attachments.confirm-delete', 'uses' => 'S2K\Mercury\Controllers\Admin\AttachmentController@getModalDelete']);

    # Events
    Route::group([ 'prefix' => 'conversations'], function() {
        Route::group(['prefix' => 'events'], function() {
            Route::post('new', array('as' => 'conversations.events.store', 'uses' => 'S2K\Mercury\Controllers\Admin\ConversationEventController@store'));
        });
    });
});

#############
# Frontend
#############
Route::group(['middleware' => ['web', 'SentinelUser'], 'as' => 'mercury::'], function () {

    Route::get('conversations', array('as' => 'user.conversations', 'uses' => 'S2K\Mercury\Controllers\ConversationController@index'));

    Route::group(array('prefix' => 'conversations'), function() {
        Route::get('new', array('as' => 'conversation.new', 'uses' => 'S2K\Mercury\Controllers\ConversationController@create'));
        Route::post('new', ['as'=> 'conversation.store', 'uses' => 'S2K\Mercury\Controllers\ConversationController@store']);

        Route::group([ 'prefix' => '{conversation_id}'], function() {
            Route::get('/', array('as' => 'conversation.show', 'uses' => 'S2K\Mercury\Controllers\ConversationController@show'));
            Route::get('resolve', array('as' => 'conversation.resolve', 'uses' => 'S2K\Mercury\Controllers\ConversationController@resolve'));
            Route::get('restore', array('as' => 'conversation.restore', 'uses' => 'S2K\Mercury\Controllers\ConversationController@restore'));

            Route::group(['prefix' => 'events'], function() {
                Route::post('new', array('as' => 'conversation.events.store', 'uses' => 'S2K\Mercury\Controllers\ConversationEventController@store'));
            });
        });
    });
});

#############
# Api
#############
// Route::group(['middleware' => ['api', 'cors', 'jwt.auth'], 'as' => 'mercury::api.', 'prefix' => 'api/v1'], function () {
//     Route::group(array('prefix' => 'user'), function() {
//         Route::get('{uuic}/conversations', array('as' => 'user.conversations', 'uses' => 'S2K\Mercury\Controllers\ConversationController@index'));
//     });

//     Route::group(array('prefix' => 'account'), function() {
//         Route::get('conversations', array('as' => 'account.conversations', 'uses' => 'S2K\Mercury\Controllers\AccountController@conversations'));
//     });

//     Route::group(array('prefix' => 'conversations'), function() {
//         Route::post('new', ['as'=> 'conversation.store', 'uses' => 'S2K\Mercury\Controllers\ConversationController@store']);

//         Route::group(['prefix' => '{conversation_id}', 'middleware' => [AccessibleConversationMiddleware::class]], function() {
//             Route::get('/', array('as' => 'conversation.show', 'uses' => 'S2K\Mercury\Controllers\ConversationController@show'));
//             Route::get('resolve', array('as' => 'conversation.resolve', 'uses' => 'S2K\Mercury\Controllers\ConversationController@resolve'));

//             Route::group(['prefix' => 'events'], function() {
//                 Route::post('new', array('as' => 'conversation.events.store', 'uses' => 'S2K\Mercury\Controllers\ConversationEventController@store'));
//             });
//         });
//     });
// });
