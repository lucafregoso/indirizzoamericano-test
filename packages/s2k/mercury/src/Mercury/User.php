<?php

namespace S2K\Mercury;


class User extends \App\User {

    // Extra Functionality
    public static function findByIds(...$ids) {
        $users = [];
        foreach($ids as $id) {
            $user = self::where('id', $id)->first();
            $users[] = $user;
        }
        return collect($users);
    }

    public static function findByUuic(...$uuics) {
        $users = [];
        foreach($uuics as $uuic) {
            $user = self::where('uuic', $uuic)->first();
            $users[] = $user;
        }
        return collect($users);
    }

    // Mercury relationship -> Conversation
    public function conversations() {
        return $this->belongsToMany(Conversation::class, 'conversation_user', 'user_id', 'conversation_id')->withTimestamps();
    }

    public function createdConversations() {
        return $this->hasMany(Conversation::class, 'creator_id');
    }

    public function conversationEvents() {
        return $this->hasMany(ConversationEvent::class, 'user_id');
    }

    public function attachments() {
        return $this->hasMany(Attachment::class, 'user_id');
    }

}