<?php

namespace S2K\Mercury\Controllers;

use App\User;
use S2K\Mercury\Conversation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Responder;

use Sentinel;

class AccountController extends Controller {
  public function conversations(Request $request) {
    $user = Sentinel::getUser();

    $createdConversations = $user->createdConversations->sortByDesc('updated_at');
    $participatingConversations = $user->conversations->sortByDesc('updated_at');

    $totalConversations = $createdConversations->merge($participatingConversations);
    $groupedStatusTotal = $totalConversations->groupBy('status');

    return Responder::makeResponse(
      $request,
      view('mercury::conversation.index', [
        'user' => $user,
        'createdConversations' => $createdConversations->whereIn('status', ['new', 'active', 'updated', 'resolved']),
        'participatingConversations' => $participatingConversations->whereIn('status', ['new', 'active', 'updated', 'resolved']),
        'closedConversations' => $groupedStatusTotal->get('closed')->sortByDesc('updated_at'),
      ]),
      response()->json($totalConversations)
    );
  }
}
