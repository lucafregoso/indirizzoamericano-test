<?php

namespace S2K\Mercury\Controllers;

use App\User;
use S2K\Mercury\Attachment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Responder;

use Sentinel;
use Storage;

class AttachmentController extends Controller {
  public function store(Request $request) {
    $data = $request->all();
    $user = Sentinel::getUser();

    $conversationEventData = [
      'detail' => $data['detail'],
      'content' => $data['content'],
      'type' => 'message',

      'user_id' => $user->id,
      'conversation_id' => $data['conversation_id'],
    ];
    $conversationEvent = ConversationEvent::create($conversationEventData);

    if (!$conversationEvent) {
      return Responder::makeResponse(
        $request,
        view('500'),
        Responder::makeErrorResponse('Server error.', 500)
      );
    } else {
      return Responder::makeResponse(
        $request,
        redirect()->route('mercury::conversation.show', $data['conversation_id']),
        response()->json($conversationEvent)
      );
    }
	}
}
