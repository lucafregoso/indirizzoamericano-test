<?php

namespace S2K\Mercury\Controllers;

use S2K\Mercury\User;
use S2K\Mercury\Conversation;
use S2K\Mercury\ConversationEvent;
use S2K\Mercury\Attachment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Responder;
use Sentinel;
use App\Helpers\Helper;
use App\Package;
use App\Order;

class ConversationController extends Controller {

    public function index(Request $request) {
        $currentUser = Sentinel::getUser();
        $user = User::find($currentUser->id);

        if (!$user || $user->id !== $currentUser->id) {
            return Responder::makeResponse( $request, view('404'), Responder::makeErrorResponse('Resource not found.', 404) );
        } else {
            $createdConversations = $user->createdConversations->sortByDesc('updated_at');
            $participatingConversations = $user->conversations->sortByDesc('updated_at');

            $totalConversations = $createdConversations->merge($participatingConversations);
            $groupedStatusTotal = $totalConversations->groupBy('status');

            return Responder::makeResponse(
                $request,
                view('mercury::conversation.index', [
                'user' => $user,
                'createdConversations' => $createdConversations->whereIn('status', ['new', 'active', 'updated', 'resolved']),
                'participatingConversations' => $participatingConversations->whereIn('status', ['new', 'active', 'updated', 'resolved']),
                'totalConversations' => $totalConversations->whereIn('status', ['new', 'active', 'updated', 'resolved']),
                'closedConversations' => collect($groupedStatusTotal->get('closed')),
                ]),
                response()->json($totalConversations)
            );
        }
    }

    public function create() {
        $users = User::pluck('email', 'id');

        $currentUser = Sentinel::getUser();
        $orders = $currentUser->orders()->get();
        $packages = $currentUser->packages()->get();

        return view('mercury::conversation.create', ['users' => $users, 'orders' => $orders, 'packages' => $packages]);
    }

    public function store(Request $request) {
        $data = $request->all();
        $user = Sentinel::getUser();

        $conversationData = array(
            'title' => $data['title'],
            'type' => $data['type'],
            'status' => 'new',
            'creator_id' => $user->id,
            'email_notifications' => $request->email_notifications ? 1 : 0,
        );

        $conversation = Conversation::create($conversationData);

        if (!$conversation) {
                return Responder::makeResponse($request, view('500'), Responder::makeErrorResponse('Server error.', 500));
        }

        if ($data['reference'] != "") {
            $ref_details = explode('-', $data['reference']);

            switch ($ref_details[0]) {
                case 'packages':
                    $conversation->packages()->sync([$ref_details[1]]);
                    break;
                case 'orders':
                    $conversation->orders()->sync([$ref_details[1]]);
                    break;
            }
        }

        $conversationEventData = [
            'detail'          => isset($data['detail']) ? $data['detail'] : null,
            'content'         => $data['content'],
            'type'            => 'message',
            'user_id'         => $user->id,
            'conversation_id' => $conversation->id,
        ];
        $conversationEvent = ConversationEvent::create($conversationEventData);

        if (!$conversationEvent) {
            return Responder::makeResponse( $request, view('500'), Responder::makeErrorResponse('Server error.', 500));
        }

        if ($file = $request->file('attachment')) {
            $filename         = md5(uniqid(microtime(), true));
            $path             = Helper::getFilePath($filename);
            $originalname     = $file->getClientOriginalName();
            $extension        = $file->getClientOriginalExtension();
            $mimetype         = $file->getClientMimeType();
            $size             = $file->getClientSize();
            $destination_path = public_path().'/uploads/files/'.Helper::getFilePath($filename);
            $remote_url       = url('/').'/uploads/files/'.Helper::getFilePath($filename).$filename.'.'.$extension;

            $save = $file->move($destination_path . '/', $filename . '.' . $extension);

            if($save) {
                $attachment = Attachment::create(['type' => 'file', 'user_id' => $user->id, 'originalname' => $originalname, 'mimetype' => $mimetype, 'filename' => $filename, 'size' => $size, 'extension' => $extension]);
                $conversationEvent->attachments()->save($attachment);
            }
        }

        //Select manager
        if ($data['type'] == 'administration') {
            $recipients = User::haveRole('admin')->get();
        } else {
            $recipients = User::haveRole('warehouse_worker')->get();
        }

        $conversation->addUser(...$recipients);

        return Responder::makeResponse(
            $request,
            redirect()->route('mercury::conversation.show', [$conversation->id]),
            response()->json($conversation)
        );
    }

    public function show($conversation_id, Request $request) {
        $conversation = Conversation::with('creator', 'users', 'events')->find($conversation_id);
        $user = Sentinel::getUser();

        if (!$conversation || ($user->id !== $conversation->creator->id && !$conversation->users->contains($user))) {
            return Responder::makeResponse($request, view('404'), Responder::makeErrorResponse('Resource not found.', 404));
        } else {
            return Responder::makeResponse(
                $request,
                view('mercury::conversation.show', [
                    'user'          => $user,
                    'users'         => $conversation->users,
                    'conversation'  => $conversation,
                    'events'        => $conversation->events,
                ]),
                response()->json($conversation)
            );
        }
    }

    public function resolve($conversation_id, Request $request) {
        $conversation = Conversation::with('creator', 'users')->find($conversation_id);
        $user = Sentinel::getUser();

        if (!$conversation || ($user->id !== $conversation->creator->id && !$conversation->users->contains('id', $user->id))) {
            return Responder::makeResponse(
            $request,
            view('404'),
            Responder::makeErrorResponse('Resource not found.', 404)
            );
        } else if ($conversation->status === 'closed' && $user->id !== $conversation->creator->id) {
            return Responder::makeResponse(
            $request,
            redirect()->route('mercury::conversation.show', [$conversation->id])->with('flash_notification', array('message' => 'You are not allowed to make that operation.')),
            response()->json($conversation)
            );
        } else {

            $conversationEventData = [
                'detail'          => 'resolved',
                'content'         => null,
                'type'            => 'event',
                'user_id'         => $user->id,
                'conversation_id' => $conversation->id,
            ];
            $conversationEvent = ConversationEvent::create($conversationEventData);

            $conversation->resolve();
            return Responder::makeResponse(
            $request,
            redirect()->route('mercury::conversation.show', [$conversation->id]),
            response()->json($conversation)
            );
        }
    }

    public function restore($conversation_id, Request $request) {
        $conversation = Conversation::with('creator', 'users')->find($conversation_id);
        $user = Sentinel::getUser();

        if (!$conversation || ($user->id !== $conversation->creator->id && !$conversation->users->contains('id', $user->id))) {
            return Responder::makeResponse(
            $request,
            view('404'),
            Responder::makeErrorResponse('Resource not found.', 404)
            );
        } else if ($conversation->status === 'closed' && $user->id !== $conversation->creator->id) {
            return Responder::makeResponse(
            $request,
            redirect()->route('mercury::conversation.show', [$conversation->id])->with('flash_notification', array('message' => 'You are not allowed to make that operation.')),
            response()->json($conversation)
            );
        } else {

            $conversationEventData = [
                'detail'          => 'new',
                'content'         => null,
                'type'            => 'event',
                'user_id'         => $user->id,
                'conversation_id' => $conversation->id,
            ];
            $conversationEvent = ConversationEvent::create($conversationEventData);

            $conversation->restore();
            return Responder::makeResponse(
            $request,
            redirect()->route('mercury::conversation.show', [$conversation->id]),
            response()->json($conversation)
            );
        }
    }
}
