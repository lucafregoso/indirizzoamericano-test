<?php

namespace S2K\Mercury\Controllers\Admin;

use S2K\Mercury\User;
use S2K\Mercury\Conversation;
use S2K\Mercury\ConversationEvent;
use S2K\Mercury\Attachment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Responder;
use Sentinel;
use Lang;
use App\Helpers\Helper;
use Datatables;
use App\Order;
use App\Package;

class ConversationController extends Controller {

    public function index() {
        //$conversations = Conversation::latest()->get();

        return view('mercury::admin.conversations.index');
    }

    public function create() {
        $users = User::get();
        $orders = Order::get();
        $packages = Package::get();

        return view('mercury::admin.conversations.create', ['users' => $users, 'orders' => $orders, 'packages' => $packages]);
    }

    public function store(Request $request) {

        $data = $request->all();
        $user = Sentinel::getUser();

        $conversationData = [
            'title' => $data['title'],
            'type' => $data['type'],
            'status' => $data['status'],
            'creator_id' => $user->id,
        ];

        $conversation = Conversation::create($conversationData);

        if (!$conversation) {
            return Responder::makeResponse(
                $request,
                view('500'),
                Responder::makeErrorResponse('Server error.', 500)
            );
        }

        if ($data['reference'] != "") {
            $ref_details = explode('-', $data['reference']);

            switch ($ref_details[0]) {
                case 'packages':
                    $conversation->packages()->sync([$ref_details[1]]);
                    break;
                case 'orders':
                    $conversation->orders()->sync([$ref_details[1]]);
                    break;
            }
        }

        $conversationEventData = [
            'detail'          => isset($data['detail']) ? $data['detail'] : null,
            'content'         => $data['content'],
            'type'            => 'message',
            'user_id'         => $user->id,
            'conversation_id' => $conversation->id,
        ];
        $conversationEvent = ConversationEvent::create($conversationEventData);

        if ($file = $request->file('attachment')) {
            $filename         = md5(uniqid(microtime(), true));
            $path             = Helper::getFilePath($filename);
            $originalname     = $file->getClientOriginalName();
            $extension        = $file->getClientOriginalExtension();
            $mimetype         = $file->getClientMimeType();
            $size             = $file->getClientSize();
            $destination_path = public_path().'/uploads/files/'.Helper::getFilePath($filename);
            $remote_url       = url('/').'/uploads/files/'.Helper::getFilePath($filename).$filename.'.'.$extension;

            $save = $file->move($destination_path . '/', $filename . '.' . $extension);

            if($save) {
                $attachment = Attachment::create(['type' => 'file', 'user_id' => $user->id, 'originalname' => $originalname, 'mimetype' => $mimetype, 'filename' => $filename, 'size' => $size, 'extension' => $extension]);
                $conversation->attachments()->save($attachment);
            }
        }

        $recipients = User::findByIds(...$data['users']);
        $conversation->addUser(...$recipients);

        return Responder::makeResponse(
            $request,
            redirect()->route('mercury::admin.conversations.index')->with('success', Lang::get('message.success.create')),
            response()->json($conversation)
        );
    }

    public function show($id) {
        $conversation = Conversation::with('events')->findOrFail($id);
        return view('mercury::admin.conversations.show', ['conversation' => $conversation]);
    }

    public function edit($id) {
        $conversation = Conversation::findOrFail($id);
        $participants = $conversation->users()->get();
        $attachment = $conversation->attachments()->first();
        $package_reference = $conversation->packages()->get();
        $order_reference = $conversation->orders()->get();

        $users = User::get();
        $orders = Order::get();
        $packages = Package::get();

        return view('mercury::admin.conversations.edit', [
            'conversation' => $conversation, 
            'users' => $users, 
            'participants' => $participants, 
            'attachment' => $attachment, 
            'orders' => $orders, 
            'packages' => $packages,
            'package_reference' => $package_reference,
            'order_reference' => $order_reference
        ]);
    }

    public function update($id, Request $request) {
        $conversation = Conversation::findOrFail($id);
        $data = $request->all();
        $user = Sentinel::getUser();

        if ($conversation) {
            $conversationData = [
                'title' => $data['title'],
                'type' => $data['type'],
                'status' => $data['status']
            ];

            $conversation->update($conversationData);

            if ($data['reference'] != "") {
                $ref_details = explode('-', $data['reference']);

                switch ($ref_details[0]) {
                    case 'packages':
                        $conversation->packages()->sync([$ref_details[1]]);
                        break;
                    case 'orders':
                        $conversation->orders()->sync([$ref_details[1]]);
                        break;
                }
            }

            if ($file = $request->file('attachment')) {

                $filename         = md5(uniqid(microtime(), true));
                $path             = Helper::getFilePath($filename);
                $originalname     = $file->getClientOriginalName();
                $extension        = $file->getClientOriginalExtension();
                $mimetype         = $file->getClientMimeType();
                $size             = $file->getClientSize();
                $destination_path = public_path().'/uploads/files/'.Helper::getFilePath($filename);
                $remote_url       = url('/').'/uploads/files/'.Helper::getFilePath($filename).$filename.'.'.$extension;

                $save = $file->move($destination_path . '/', $filename . '.' . $extension);

                if($save) {
                    $attachment = Attachment::create(['type' => 'file', 'user_id' => $user->id, 'originalname' => $originalname, 'mimetype' => $mimetype, 'filename' => $filename, 'size' => $size, 'extension' => $extension]);
                }

                $conversation->attachments()->sync([$attachment->id]);
            }

            $recipients = User::findByIds(...$data['users']);
            $conversation->addUser(...$recipients);
        }

        return redirect()->route('mercury::admin.conversations.index')->with('success', Lang::get('message.success.update'));
    }

    public function getModalDelete($id = null) {
        $error = ''; $model = '';
        $confirm_route = route('mercury::admin.conversations.delete', ['id' => $id]);

        return view('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function getDelete($id = null) {
        $conversation = Conversation::destroy($id);

        return redirect()->route('mercury::admin.conversations.index')->with('success', Lang::get('message.success.delete'));
    }

    /*
     * Pass data through ajax call
     */
    public function data(Request $request)
    {
        $conversations = Conversation::with('creator');

        if ($request->get('status')) {
            $conversations = $conversations->where('status', $request->get('status'));
        }

        $conversations = $conversations->latest('id');

        return Datatables::of($conversations)
            ->edit_column('created_at',function(Conversation $conversations) {
                return $conversations->created_at->diffForHumans();
            })
            ->edit_column('creator_data',function(Conversation $conversations) {
                return '<a href='. route('admin.users.show', $conversations->creator->id) .' target="_blank">'.(isset($conversations->creator->uuic) ? $conversations->creator->uuic : '').' - '.$conversations->creator->first_name.' '.$conversations->creator->last_name.' - '.$conversations->creator->email.'</a>';
            })
            ->add_column('actions',function($conversations) {
                $actions = '
                    <a href='. route('mercury::admin.conversations.show', $conversations->id) .' class="btn default"><i class="fa fa-info"></i> View</a>
                    <a href='. route('mercury::admin.conversations.edit', $conversations->id) .' class="btn default"><i class="fa fa-edit"></i> Edit</a>
                    <a href='. route('mercury::admin.conversations.confirm-delete', $conversations->id) .' data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
                ';
                return $actions;

            })->make(true);
    }

    /*
     * Pass data through ajax call
     */
    public function newConversationsData(Request $request)
    {
        $conversations = Conversation::with('creator');

        if ($request->get('status')) {
            $conversations = $conversations->where('status', $request->get('status'));
        }

        $conversations = $conversations->where('status', 'new')->latest('id');

        return Datatables::of($conversations)
            ->edit_column('created_at',function(Conversation $conversations) {
                return $conversations->created_at->diffForHumans();
            })
            ->edit_column('creator_data',function(Conversation $conversations) {
                return '<a href='. route('admin.users.show', $conversations->creator->id) .' target="_blank">'.(isset($conversations->creator->uuic) ? $conversations->creator->uuic : '').' - '.$conversations->creator->first_name.' '.$conversations->creator->last_name.' - '.$conversations->creator->email.'</a>';
            })
            ->add_column('actions',function($conversations) {
                $actions = '
                    <a href='. route('mercury::admin.conversations.show', $conversations->id) .' class="btn default"><i class="fa fa-info"></i> View</a>
                    <a href='. route('mercury::admin.conversations.edit', $conversations->id) .' class="btn default"><i class="fa fa-edit"></i> Edit</a>
                    <a href='. route('mercury::admin.conversations.confirm-delete', $conversations->id) .' data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
                ';
                return $actions;

            })->make(true);
    }


}
