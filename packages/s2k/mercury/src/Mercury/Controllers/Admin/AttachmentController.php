<?php

namespace S2K\Mercury\Controllers\Admin;

use S2K\Mercury\User;
use S2K\Mercury\Conversation;
use S2K\Mercury\Attachment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Responder;
use Sentinel;
use Lang;
use App\Helpers\Helper;
use Datatables;

class AttachmentController extends Controller {

    public function index() {
        return view('mercury::admin.attachments.index');
    }

    public function getModalDelete($id = null) {
        $error = ''; $model = '';
        $confirm_route = route('mercury::admin.attachments.delete', ['id' => $id]);

        return view('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function getDelete($id = null) {
        $attachment = Attachment::destroy($id);

        return redirect()->route('mercury::admin.attachments.index')->with('success', Lang::get('message.success.delete'));
    }

    /*
     * Pass data through ajax call  
     */
    public function data(Request $request)
    {
        $attachments = Attachment::with('user', 'conversations', 'conversationEvents.conversation');
        $attachments = $attachments->get();

        return Datatables::of($attachments)
            ->edit_column('created_at',function(Attachment $attachments) {
                return $attachments->created_at->diffForHumans();
            })
            ->edit_column('parent_data',function(Attachment $attachments) {

                if( count($attachments->conversations) ) {
                    return '<a href='. route('mercury::admin.conversations.show', $attachments->conversations()->first()->id) .' target="_blank">'.$attachments->conversations()->first()->title.'</a>';
                } elseif (count($attachments->conversationEvents)) {
                    return '<a href='. route('mercury::admin.conversations.show', $attachments->conversationEvents->first()->conversation->id) .' target="_blank">'.$attachments->conversationEvents->first()->conversation->title.'</a>';
                }
            })
            ->edit_column('file',function(Attachment $attachments) {
                return '<a href='. url('/').'/uploads/files/'.Helper::getFilePath($attachments->filename).$attachments->filename.'.'.$attachments->extension .' target="_blank">'.$attachments->originalname.'</a>';
            })
            ->edit_column('user_data',function(Attachment $attachments) {
                return '<a href='. route('admin.users.show', $attachments->user->id) .' target="_blank">'.(isset($attachments->user->uuic) ? $attachments->user->uuic : '').' - '.$attachments->user->first_name.' '.$attachments->user->last_name.' - '.$attachments->user->email.'</a>';
            })
            ->add_column('actions',function($attachments) {
                $actions = '
                    <a href='. route('mercury::admin.attachments.confirm-delete', $attachments->id) .' data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete order"></i></a>
                ';
                return $actions;

        })->make(true);
    }

}
