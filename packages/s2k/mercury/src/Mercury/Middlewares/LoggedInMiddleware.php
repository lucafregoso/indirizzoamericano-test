<?php

namespace S2K\Mercury\Middlewares;

//use S2K\Mercury\Controllers\MercuryController;

use Illuminate\Http\Request;
use Closure;

use Sentinel;

class LoggedInMiddleware {
  public function handle(Request $request, Closure $next) {
    if (!Sentinel::check()) {
      return MercuryController::makeResponse(
        $request,
        redirect()->route('login'),
        MercuryController::makeErrorResponse('Resource not accessible.', 403)
      );
    }

    return $next($request);
  }
}
