<?php

namespace S2K\Mercury\Middlewares;

use S2K\Mercury\Controllers\MercuryController;
use S2K\Mercury\Conversation;

use Illuminate\Http\Request;
use Closure;

use Sentinel;
use JWTAuth;

class AccessibleConversationMiddleware {
  public function handle(Request $request, Closure $next) {
    $conversation = Conversation::find($request->conversation_id);
    $user = JWTAuth::toUser($request->get('token'));

    if (!$conversation) {
      return MercuryController::makeResponse(
        $request,
        view('404'),
        MercuryController::makeErrorResponse('Resource not found.', 404)
      );
    } else if ($user->id !== $conversation->creator->id && !$conversation->users->contains('id', $user->id)) {
      return MercuryController::makeResponse(
        $request,
        redirect()->route('login'),
        MercuryController::makeErrorResponse('Resource not accessible.', 403)
      );
    } else {
      Sentinel::login($user);
      return $next($request);
    }
  }
}
