<?php

namespace S2K\Mercury;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use S2K\Mercury\ConversationEvent;
use App\User;

class Conversation extends Model {

  use SoftDeletes;

  public static $conversation_types = [
    'administration',
    'personal shopper',
    'warehouse assistant',
  ];

  public static $conversation_statuses = [
    'new',
    'active',
    'updated',
    'resolved',
    'closed',
  ];

  protected $fillable = ['title', 'type', 'status', 'creator_id', 'email_notifications'];

  protected $dates = ['deleted_at'];


  // protected static function boot() {
  //   parent::boot();
  //
  //   static::created(function($conversation) {
  //     $event = ConversationEvent::create([
  //       'type' => 'event',
  //       'detail' => 'create',
  //       'user_id' => $conversation->creator->id,
  //     ]);
  //
  //     $conversation->events()->save($event);
  //   });
  // }

  // Relationships
  public function creator() {
    return $this->belongsTo(User::class, 'creator_id');
  }

  public function users() {
    return $this->belongsToMany(User::class, 'conversation_user', 'conversation_id', 'user_id')->withTimestamps();
  }

  public function events() {
    return $this->hasMany(ConversationEvent::class);
  }

  public function attachments() {
    return $this->morphToMany(Attachment::class, 'attachment_poly');
  }

  public function orders()
  {
      return $this->morphedByMany('App\Order', 'conversation_relationships' ,'conversation_relationships', 'conversation_id','relation_id');
  }

  public function packages()
  {
      return $this->morphedByMany('App\Package', 'conversation_relationships' ,'conversation_relationships', 'conversation_id','relation_id');
  }


  // Utility
  public function resolve() {
    $this->status = 'resolved';
    $this->save();
  }

  public function restore() {
    $this->status = 'new';
    $this->save();
  }

  public function addUser(...$users) {
    $userCollection = collect($users);
    $filtered = $userCollection->reject(function($user) {
      return $user->id == $this->creator->id || $this->users->contains($user);
    });

    $filtered->each(function($user) {
      $this->users()->save($user);

      $event = ConversationEvent::create([
        'type' => 'event',
        'detail' => 'join',
        'user_id' => $user->id,
        'conversation_id' => $this->id,
      ]);
    });

    $this->load('users');
    return $this;
  }
}
