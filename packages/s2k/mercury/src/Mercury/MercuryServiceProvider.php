<?php

namespace S2K\Mercury;

use Illuminate\Support\ServiceProvider;

use Illuminate\Database\Eloquent\Relations\Pivot;
use S2K\Mercury\Conversation;
use S2K\Mercury\ConversationEvent;

use S2K\Mercury\Helpers\Responses;

class MercuryServiceProvider extends ServiceProvider {
    protected $packageName = 'mercury';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
      include __DIR__.'/../routes.php';

      // Register Views from your package
      $this->loadViewsFrom(__DIR__.'/../views', $this->packageName);

      // Register your asset's publisher
      $this->publishes([
          __DIR__.'/../assets' => public_path('vendor/'.$this->packageName),
      ], 'public');

      // Register your migration's publisher
      $this->publishes([
          __DIR__.'/../database/migrations/' => base_path('/database/migrations')
      ], 'migrations');

      // Publish your seed's publisher
      $this->publishes([
          __DIR__.'/../database/seeds/' => base_path('/database/seeds')
      ], 'seeds');

      // Publish your factories's publisher
      $this->publishes([
          __DIR__.'/../database/factories/' => base_path('/database/factories')
      ], 'factories');

      // Publish your config
      $this->publishes([
          __DIR__.'/../config/config.php' => config_path($this->packageName.'.php'),
      ], 'config');

      // Model Events
      Conversation::created(function($conversation) { // Add Conversation Event on Create
        $event = ConversationEvent::create([
          'type' => 'event',
          'detail' => 'create',
          'conversation_id' => $conversation->id,
          'user_id' => $conversation->creator->id,
        ]);

        return true;
      });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
      $this->mergeConfigFrom( __DIR__.'/../config/config.php', $this->packageName);
    }

}
