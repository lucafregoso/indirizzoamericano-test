<?php

namespace S2K\Mercury\Helpers;

use Illuminate\Http\Request;
use Response;

class Responder {
  static public function shouldReturnJSON(Request $request) {
    return ( $request->ajax() || $request->wantsJson() || $request->is('api/v1/*') );
  }

  static public function makeErrorResponse($message, $code) {
    return response()->json([
      'error' => $message,
    ], $code);
  }

  static public function makeResponse(Request $request, $standard, $json) {
    $standardResponse = $standard ?? view('blank_page');
    $jsonResponse = $json ?? self::makeErrorResponse('Blank response.', 501);

    if (self::shouldReturnJSON($request)) {
      return $jsonResponse;
    }

    return $standardResponse;
  }
}
