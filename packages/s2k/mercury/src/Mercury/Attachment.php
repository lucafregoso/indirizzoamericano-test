<?php

namespace S2K\Mercury;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use S2K\Mercury\Conversation;
use S2K\Mercury\ConversationEvent;
use App\User;
use Sentinel;
use Hash;
use Storage;
use File;
use Hashids;
use Carbon\Carbon;

class Attachment extends Model {

  use SoftDeletes;

  public static $attachment_types = [
    'picture',
    'text',
    'file',
  ];

  public static $attachmnet_statuses = [
    'undetermined',
    'relevant',
    'useless',
  ];

  protected $fillable = ['type', 'status', 'filename', 'originalname', 'mimetype', 'description', 'user_id', 'size', 'extension'];

  protected $dates = ['deleted_at'];


  // Relationships
  public function user() {
    return $this->belongsTo(User::class);
  }

  public function conversationEvents() {
    return $this->morphedByMany(ConversationEvent::class, 'attachment_poly', 'attachment_polies', 'attachment_id', 'attachment_poly_id');
  }

  public function conversations() {
    return $this->morphedByMany(Conversation::class, 'attachment_poly', 'attachment_polies', 'attachment_id', 'attachment_poly_id');
  }

  // Extras
	public static function findByFilename(...$filenames) {
		$attachments = [];

		foreach($filenames as $filename) {
			$attachment = self::where('filename', $filename)->first();
			$attachments[] = $attachment;
		}

		return collect($attachments);
	}

  // Utilities
  public function persist($file) {
    $user = Sentinel::getUser();
    $extension = $file->getClientOriginalExtension();

    $currentTime = Carbon::now()->getTimestamp();
    $fileHash = str_random(8);
    $fullFilename = $currentTime.'.'.$user->username.'.'.$file->getBasename().'.'.$fileHash.'.'.$extension;

		Storage::disk('local')->put($fullFilename, File::get($file));

		$this->mime = $file->getClientMimeType();
		$this->filename_original = $file->getClientOriginalName();
		$this->filename = $fullFilename;

    $this->save();

    return $this;
  }

  public function url() {
    return Storage::disk('local')->url($this->filename);
  }
}
