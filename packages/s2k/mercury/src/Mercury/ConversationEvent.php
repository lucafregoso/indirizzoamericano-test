<?php

namespace S2K\Mercury;

use Illuminate\Database\Eloquent\Model;

use S2K\Mercury\Conversation;
use S2K\Mercury\Attachment;
use App\User;

class ConversationEvent extends Model {
  public static $event_types = [
    'message',
    'event',
  ];

  protected $fillable = ['type', 'content', 'detail', 'data', 'user_id', 'conversation_id'];

  public function user() {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function conversation() {
    return $this->belongsTo(Conversation::class, 'conversation_id');
  }

  public function attachments() {
    return $this->morphToMany(Attachment::class, 'attachment_poly');
  }
}
