var elixir = require('laravel-elixir');

elixir.config.appPath = 'src';
elixir.config.viewPath = 'src/views';
elixir.config.assetsPath = 'src/assets';

elixir(function(mix) {
  mix.phpUnit();
});
