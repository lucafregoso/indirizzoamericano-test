<?php

namespace S2K\Mercury\Controllers\Admin;

use App\User;
use S2K\Mercury\Conversation;
use S2K\Mercury\ConversationEvent;
use S2K\Mercury\Attachment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Responder;
use Sentinel;
use App\Helpers\Helper;
use Mail;

class ConversationEventController extends Controller {

    public function store(Request $request) {
        $data = $request->all();
        $user = Sentinel::getUser();
        $conversation = Conversation::where('id', $data['conversation_id'])->first();

        $conversationEventData = [
            'detail'          => isset($data['detail']) ? $data['detail'] : null,
            'content'         => $data['content'],
            'type'            => 'message',
            'user_id'         => $user->id,
            'conversation_id' => $data['conversation_id'],
        ];
        $conversationEvent = ConversationEvent::create($conversationEventData);

        if (!$conversationEvent) {
            return Responder::makeResponse( $request, view('500'), Responder::makeErrorResponse('Server error.', 500));
        }

        if ($file = $request->file('attachment')) {
            $filename         = md5(uniqid(microtime(), true));
            $path             = Helper::getFilePath($filename);
            $originalname     = $file->getClientOriginalName();
            $extension        = $file->getClientOriginalExtension();
            $mimetype         = $file->getClientMimeType();
            $size             = $file->getClientSize();
            $destination_path = public_path().'/uploads/files/'.Helper::getFilePath($filename);
            $remote_url       = url('/').'/uploads/files/'.Helper::getFilePath($filename).$filename.'.'.$extension;

            $save = $file->move($destination_path . '/', $filename . '.' . $extension);

            if($save) {
                $attachment = Attachment::create(['type' => 'file', 'user_id' => $user->id, 'originalname' => $originalname, 'mimetype' => $mimetype, 'filename' => $filename, 'size' => $size, 'extension' => $extension]);
                $conversationEvent->attachments()->save($attachment);
            }
        }

        if ($conversation->email_notifications === 1) {
            $owner_ticket = User::where('id', $conversation->creator_id)->first();
            $notification_email_data = [
                'owner_ticket' => $owner_ticket,
                'content' => $data['content'],
            ];

            //Send Email Notification to user
            Mail::queue('emails.ticket-notification', $notification_email_data, function ($m) use ($owner_ticket, $conversation) {
                $m->to($owner_ticket->email, $owner_ticket->first_name . ' ' . $owner_ticket->last_name);
                $m->subject('Ticket ' . $conversation->title);
            });
        }

        return Responder::makeResponse(
            $request,
            redirect()->route('mercury::admin.conversations.show', $data['conversation_id']),
            response()->json($conversationEvent)
        );
    }

}
