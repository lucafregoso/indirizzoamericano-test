<?php
/**
* Language file for form fields for user account management
*
*/

return array(

    'password'				=> 'Password',
    'email'					=> 'Email',
    'newemail'				=> 'Nuova Email',
    'confirmemail'			=> 'Conferma Email',
    'firstname'				=> 'Nome',
    'lastname'				=> 'Cognome',
    'newpassword'			=> 'Nuova Password',
    'website'				=> 'Website',
    'country'				=> 'Stato',
    'gravataremail'			=> 'Email Gravatar',
    'changegravatar'		=> 'Cambia il tuo avatar su Gravatar.com',
    'oldpassword'			=> 'Password attuale',
    'confirmpassword'		=> 'Conferma Password',

);
