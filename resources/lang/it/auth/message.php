<?php
/**
* Language file for auth error messages
*
*/

return array(

    'account_already_exists'    => 'Esiste già un account con questa email.',
    'account_not_found'         => 'Email o password errata.',
    'account_email_not_found'   => 'Email errata.',
    'account_not_activated'     => 'Questo account non è attivato.',
    'account_suspended'         => 'Account sospeso a causa di troppi tentativi di login. Riprova dopo [:delay] secondi',
    'account_banned'            => 'Questo account è stato bannato.',

    'signin' => array(
        'error'   => 'C\'è stato un problema durante il login, prova di nuovo.',
        'success' => 'Login effettuato con successo.',
    ),

    'login' => array(
        'error'   => 'C\'è stato un problema durante il login, prova di nuovo.',
        'success' => 'Login effettuato con successo.',
    ),

    'signup' => array(
        'error'   => 'C\'è stato un problema durante la creazione del tuo account, prova di nuovo.',
        'success' => 'Account creato con successo. Controlla la tua email per l\'attivazione.',
    ),

        'forgot-password' => array(
            'error'   => 'C\'è stato un problema durante la creazione di un codice di reset per la password, prova di nuovo.',
            'success' => 'Email per il recupero della password spedita con successo.',
        ),

        'forgot-password-confirm' => array(
            'error'   => 'C\'è stato un problema durante la modifica della password, prova di nuovo.',
            'success' => 'La tua password è stata modificata con successo.',
        ),

    'activate' => array(
        'error'   => 'C\' stato un problema durante l\'attivazione del tuo account, prova di nuovo.',
        'success' => 'Il tuo account è stato attivato con successo.',
    ),

    'contact' => array(
        'error'   => 'C\'è stato un problema durante l\'invio dei dati, prova di nuovo.',
        'success' => 'I tuoi dati sono stati inviati con successo.',
    ),
);
