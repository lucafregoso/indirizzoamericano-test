<?php

/**
* Language file for user delete modal
*
*/
return array(

    'body'			=> 'Sei sicuro di voler spostare l\'utente nel cestino?',
    'cancel'		=> 'Cancella',
    'confirm'		=> 'Elimina',
    'title'         => 'Elimina utente',

);
