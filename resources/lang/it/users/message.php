<?php
/**
* Language file for user error/success messages
*
*/

return [

    'user_exists'              => 'Utente già esistente',
    'user_not_found'           => 'L\'utente [:id] non esiste.',
    'user_login_required'      => 'Il campo Login è richiesto.',
    'user_password_required'   => 'Password richiesta.',
    'insufficient_permissions' => 'Permessi insufficienti.',
    'banned'              => 'bannato',
    'suspended'             => 'sospeso',

    'success' => [
        'create'    => 'Utente creato con successo.',
        'update'    => 'Utente aggiornato con successo.',
        'delete'    => 'Utente eliminato con successo.',
        'ban'       => 'Utente bannato con successo.',
        'unban'     => 'Utente sbannato con successo.',
        'suspend'   => 'Utente sospeso con successo.',
        'unsuspend' => 'Utente riattivato con successo.',
        'restored'  => 'Utente ripristinato con successo.'
    ],

    'error' => [
        'create'    => 'C\'è stato un problema durante la creazione dell\'utente. Prova di nuovo.',
        'update'    => 'C\'è stato un problema durante l\'aggiornamento dell\'utente. Prova di nuovo.',
        'delete'    => 'C\'è stato un problema durante l\'eliminazione dell\'utente. Prova di nuovo.',
        'unsuspend' => 'C\'è stato un problema durante la riattivazione dell\'utente. Prova di nuovo.',
        'file_type_error'   => 'Sono ammesse solo le estensioni jpg, jpeg, bmp e png.',
    ],

];

