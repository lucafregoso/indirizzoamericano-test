<?php

/**
 * Language file for delete modal
 *
 */
return array(

    'title'         => 'Send payment',
    'body'			=> 'Are you sure to send payment for this order? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Confirm',

);
