<?php
/**
* Language file for group section titles
*
*/

return array(

    'create'			=> 'Crea Nuovo Gruppo',
    'edit' 				=> 'Modifica Group',
    'management'	=> 'Gestisci Gruppi',
    'groups' => 'Gruppi',
    'groups_list' => 'Lista Gruppi',

);
