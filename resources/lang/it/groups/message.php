<?php
/**
* Language file for group error/success messages
*
*/

return array(

    'group_exists'        => 'Il gruppo esiste già!',
    'group_not_found'     => 'Il gruppo [:id] non esiste.',
    'group_name_required' => 'Il campo Nome è richiesto',
    'users_exists'        => 'Il gruppo contiene utenti e non può essere eliminato',

    'success' => array(
        'create' => 'Il gruppo è stato creato con successo.',
        'update' => 'Il gruppo è stato creato con successo.',
        'delete' => 'Il gruppo è stato creato con successo.',
    ),

    'delete' => array(
        'create' => 'C\'è stato un problema durante la creazione del gruppo. Prova di nuovo.',
        'update' => 'C\'è stato un problema durante la creazione del gruppo. Prova di nuovo.',
        'delete' => 'C\'è stato un problema durante la creazione del gruppo. Prova di nuovo.',
    ),

    'error' => array(
        'group_exists' => 'Esiste già un gruppo con questo nome.',
        'group_role_exists' => 'Esiste già un ruolo con questo slug, scegli un altro nome',
        'no_role_exists' => 'Non esiste un ruolo con questo id'
    ),

);
