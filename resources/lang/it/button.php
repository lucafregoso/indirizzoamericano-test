<?php
/**
* Language file for submit buttons across the site
*
*/
return array(

    'edit'    			=> 'Modifica',
    'delete'  			=> 'Elimina',
    'restore' 			=> 'Ripristina',
    'publish' 			=> 'Pubblica',
    'save'	  			=> 'Salva',
    'submit'	  		=> 'Invia',
    'cancel'  			=> 'Cancella',
    'create'  			=> 'Crea',
    'back'    			=> 'Indietro',
    'signin'  			=> 'Accedi',
    'forgotpassword' 	=> 'Ho dimenticato la mia password',
    'rememberme'		=> 'Ricordami',
    'signup'			=> 'Iscriviti',
    'update'			=> 'Aggiorna',

);
