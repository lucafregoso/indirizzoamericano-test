<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Le password devono essere di almeno 6 caratteri e devono coincidere.",

	"user" => "Non è stato trovato alcun utente con questo indirizzo email.",

	"token" => "Token di reset password non valido.",

	"sent" => "Password reminder spedito!",

);
