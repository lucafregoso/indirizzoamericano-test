<?php
/**
 * Language file for item error/success messages
 *
 */

return array(

    'exists'        => 'Oggetto già esistente',
    '_not_found'     => 'L\'oggetto [:id] non esiste',
    '_name_required' => 'Il campo Nome è richiesto',

    'success' => array(
        'create' => 'Oggetto creato con successo.',
        'update' => 'Oggetto creato con successo.',
        'delete' => 'Oggetto creato con successo.',
        'restored'  => 'Oggetto ripristinato con successo.'
    ),

    'delete' => array(
        'create' => 'C\'è stato un problema durante la creazione dell\'oggetto. Prova di nuovo.',
        'update' => 'C\'è stato un problema durante la creazione dell\'oggetto. Prova di nuovo.',
        'delete' => 'C\'è stato un problema durante la creazione dell\'oggetto. Prova di nuovo.',
    ),

    'error' => array(
        'item_exists' => 'Esiste già un oggetto con questo nome.',
    ),

);
