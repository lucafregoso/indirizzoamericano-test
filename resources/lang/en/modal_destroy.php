<?php

/**
 * Language file for delete modal
 *
 */
return array(

    'title'         => 'Destroy item',
    'body'			=> 'Are you sure to destroy this item? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',

);
