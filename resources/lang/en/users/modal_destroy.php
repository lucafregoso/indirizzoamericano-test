<?php

/**
* Language file for user delete modal
*
*/
return array(

    'body'			=> 'Are you sure to destroy this user? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Destroy',
    'title'         => 'Destroy User',

);
