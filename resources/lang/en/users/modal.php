<?php

/**
* Language file for user delete modal
*
*/
return array(

    'body'			=> 'Are you sure you want to move user to the trash?',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',
    'title'         => 'Delete User',

);
