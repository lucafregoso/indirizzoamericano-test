<?php
return [

    'static.explanation' => 'explanation',
    'static.shipping'    => 'shipping',
    'static.why'         => 'why',
    'static.rates'       => 'rates',
    'static.options'       => 'options',
    'static.contacts'       => 'contacts',
    'static.support'       => 'support',
    'static.track'       => 'track',
    'static.temp'       => 'temp',
];