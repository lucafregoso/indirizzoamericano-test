@extends('layouts/default')

{{-- Page title --}}
@section('title')
Home
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">
    <div class="wrap columns cf">
        <div class="column m-1of3 t-1of3 d-1of4 header-image-small">
            <img src="{{ asset('assets/library/images/homepage/plus2.png') }}" alt=""/>
        </div>
        <div class="column m-2of3 t-2of3 d-3of4">
            <h3 class="light-blue bold italic">Ciao! Benvenuto in <br/>Indirizzo Americano</h3>
        </div>
    </div>

    
    <div class="wrap columns cf">
        <div class="column m-all t-1of3 d-1of4">
            
        </div>
        <div class="column m-all t-all d-3of4">
            
            <!--<h3>Status</h3> 
            @if(Sentinel::check())
                <p>Sei loggato - {{ $user->email }} - <a href="{{ route('logout') }}">Logout</a></p>
            @else
                <p>Non sei loggato</p>
            @endif
            -->

            <!--<h3>Messages</h3> -->
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif


            <!-- forgot passw -->
            @if (session('error'))
                <div class="error-box">
                    {{ session('error') }}
                </div>
            @endif
            


            @if(isset($errors) && count($errors->all()))
                <div class="error-box"> 
                    @foreach ($errors->all() as $error)
                      <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>


    <div class="wrap spaceT30 columns cf">
        <div class="column m-all t-all d-1of4">
            <!-- spacer -->
        </div>
        <div class="column m-all t-1of2 d-1of3 spaceB50">
            <div class="auth-intro spaceB10">
                <h2 class="bold italic">Accedi</h2>
                <p>Inserisci e-mail e password per accedere</p>
            </div>
            <div class="auth-box light-blue-bg">
                {!! Form::open(['url' => route('login'), 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder'=>'Email']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::password('password', ['class' => 'form-control auth-pass', 'placeholder'=>'Password']) !!}
                        <p class="auth-forgot spaceB30 text-right"><a href="{{ route('forgot-password') }}">Hai dimenticato al password?</a></p>
                    </div>

                    <!-- Form actions -->
                    <div class="form-group text-center">
                        {!! Form::submit('Accedi', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="column hidden-xs d-1of12">
            <!-- spacer -->
        </div>
        <div class="column m-all t-1of2 d-1of3">
            <div class="auth-intro spaceB10">
                <h2 class="bold italic">Crea il tuo account</h2>
                <p>Potrai usufruire di servizi personalizzati e vantaggi esclusivi: acquisti più rapidi, creazione whishlist e monitoraggio ordini.</p>
            </div>
            <div class="auth-box light-blue-bg">
                {!! Form::open(['url' => route('register'), 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        {!! Form::text('first_name', old('first_name'), ['class' => 's2k-required form-control', 'placeholder'=>'Nome' ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('last_name', old('last_name'), ['class' => 's2k-required form-control', 'placeholder'=>'Cognome']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::date('dob', old('dob') ? old('dob') : null, ['id' => 'datepicker', 'class' => 's2k-required form-control', 'placeholder'=>'Data di nascita gg/mm/aaaa']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('tax', old('tax'), ['class' => 's2k-required form-control', 'placeholder'=>'C.Fis / P.IVA']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('phone', old('phone'), ['class' => 's2k-required form-control', 'placeholder'=>'Telefono']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('address', old('address'), ['class' => 's2k-required form-control', 'placeholder'=>'Indirizzo']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('city', old('city'), ['class' => 's2k-required form-control', 'placeholder'=>'Città']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('zip_code', old('zip_code'), ['class' => 's2k-required form-control', 'placeholder'=>'CAP']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('state', old('state'), ['class' => 's2k-required form-control', 'placeholder'=>'Stato']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('email', old('email'), ['class' => 's2k-required form-control', 'placeholder'=>'Email']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::password('password', ['class' => 's2k-required form-control', 'placeholder'=>'Password']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::password('password_confirmation', ['class' => 's2k-required form-control', 'placeholder'=>'Ripeti la password']) !!}
                    </div>

                    <div class="form-group spaceT10">
                        <label class="control-label cf">
                            {!! Form::checkbox('privacy', 1, null, ['class' => 's2k-input-checkbox form-control']) !!}
                            <span class="s2k-input-checkbox-style to-left"></span>
                            <span class="to-left checkbox-text">Ho letto e accetto le clausole “informativa sulla privacy”</span>
                        </label>
                    </div>

                    <div class="form-group spaceT10">
                        <label class="control-label cf">
                            {!! Form::checkbox('newsletter', 1, null, ['class' => 's2k-input-checkbox form-control']) !!}
                            <span class="s2k-input-checkbox-style to-left"></span>
                            <span class="to-left checkbox-text">Desidero ricevere offerte e promozioni in anteprima</span>
                        </label>
                    </div>

                    <!-- Form actions -->
                    <div class="form-group spaceT30 text-center">
                        {!! Form::submit('Registrati', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>





</section><!-- .page-content -->




@stop

{{-- footer scripts --}}

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      maxDate: 0
    });
} );
</script>

@section('footer_scripts')
@stop
