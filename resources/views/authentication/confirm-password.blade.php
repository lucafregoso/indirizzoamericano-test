@extends('layouts/default')

{{-- Page title --}}
@section('title')
Home
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">

    <div class="wrap columns cf">
        <div class="column m-1of3 t-1of3 d-1of4 header-image-small">
            <img src="{{ asset('assets/library/images/homepage/plus2.png') }}" alt=""/>
        </div>
        <div class="column m-2of3 t-2of3 d-3of4">
            <h3 class="light-blue">Ciao! Benvenuto in <br/>Indirizzo Americano</h3>
        </div>
    </div>

    <div class="wrap columns cf">
        <div class="column m-all t-1of3 d-1of4">
        </div>
        <div class="column m-all t-all d-3of4">

            <!--<h3>Messages</h3> -->
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <!-- forgot passw -->
            @if (session('error'))
                <div class="error-box">
                    {{ session('error') }}
                </div>
            @endif

            @if(isset($errors) && count($errors->all()))
                <div class="error-box"> 
                    @foreach ($errors->all() as $error)
                      <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>

    <div class="wrap spaceT30 columns cf">
        <div class="column m-all t-all d-1of4">
            <!-- spacer -->
        </div>
        <div class="column m-all t-1of2 d-1of3 spaceB50">
            @if (isset($passwordResetCode))
                <div class="panel-body">

                    <div class="auth-intro spaceB10">
                        <h2>Conferma Nuova Password</h2>
                    </div>
                    <div class="auth-box light-blue-bg">
                        {!! Form::open(['url' => route('forgot-password-confirm', [$userId, $passwordResetCode]), 'class' => 'form-horizontal']) !!}

                            <div class="form-group">
                                {!! Form::password('password', null, ['class' => 'form-control', 'placeholder'=>'Password']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::password('password_confirm', null, ['class' => 'form-control', 'placeholder'=>'Password Confirm']) !!}
                            </div>

                            <!-- Form actions -->
                            <div class="form-group">
                                {!! Form::submit('Conferma', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @endif
        </div>
    </div>
</section><!-- .page-content -->
@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
