<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Indirizzo Americano - description">

        <title>
            @section('title')
            | Indirizzo Americano
            @show
        </title>

        <link rel="shortcut icon" href="{{ asset('assets/library/favicon.ico') }}">
        <link rel="icon" href="{{ asset('assets/library/favicon.ico') }}">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/library/css/styles.css') }}">

        <!--page level css-->
        @yield('header_styles')
        <!--end of page level css-->
    </head>

    <body>
        <!-- Header -->
        @if (Request::is('storage*') || Request::is('conversations*'))
            @include('layouts.header-storage')
        @else
            @include('layouts.header')
        @endif

        <!-- Content -->
        @yield('content')

        <!-- Footer -->
        @include('layouts.footer')

        <!-- Footer Section Start -->
        @yield('footer_scripts')

        <!-- STICKY MENU -->
        <script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery("document").ready(function($){

                var menu = $('#header');

                $(window).scroll(function () {
                    if ($(this).scrollTop() > 136) {
                        menu.addClass("fixed-menu");
                        menu.slideDown(300);
                    } else {
                        menu.removeClass("fixed-menu");
                        menu.removeAttr("style");
                    }
                });

                $(".newsletter-sub-btn").on("click", function(e) {
                    e.preventDefault();
                    $.ajax({
                        method: "POST",
                        url: "{{ route('newsletters') }}",
                        data: {
                            email: $('#newsletter-field').val(),
                        },
                        success: function (data) {
                            $(".newsletter-result").show();
                        }
                    })
                });

            });
        </script>
    </body>
</html>
