<header id="header">
    <div class="header-secondary-level">
        <div class="wrap">
            <div class="columns cf">
                <div class="column m-all t-1of2 d-1of2 header-secondary-level-links">
                    <ul>
                        <li {{ Request::fullUrl() == route('static.support') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.support') }}">Support</a></li>
                        <li {{ Request::fullUrl() == route('static.support') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.support') }}">F.A.Q.</a></li>
                    </ul>
                </div>
                <div class="column m-all t-1of2 d-1of2 header-secondary-level-icons">
                    <a href="mailto:{{ $_warehouse_info->email }}"><span class="icon icon-mail"></span></a>
                    <a href="{{ $_warehouse_info->facebook }}" target="_blank"><span class="icon icon-facebook"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-primary-level">
        <div class="wrap">
            <div class="columns cf">
                <div class="column m-1of2 t-1of3 d-1of4 s2k-fixed-hidden">
                    <h1 class="main-logo">
                        <a href="{{ route('home') }}"><img src="{{ asset('assets/library/images/indirizzo-americano.png') }}" alt="Indirizzo Americano" title="Indirizzo Americano"></a>
                    </h1>
                </div>
                <!-- fixed logo small -->
                <div class="column m-1of12 t-1of12 d-1of12 s2k-fixed-visible">
                    <h1 class="main-logo">
                        <a href="{{ route('home') }}"><img src="{{ asset('assets/library/images/indirizzo-americano-small.png') }}" alt="Indirizzo Americano" title="Indirizzo Americano"></a>
                    </h1>
                </div>
                <div class="column d-7of12 header-primary-level-main-menu">
                    <nav class="main-navigation">
                        <ul>
                            <li {{ Request::fullUrl() == route('home') ? 'class=current-menu-item' : '' }}><a href="{{ route('home') }}">Home</a></li>
                            <li {{ Request::fullUrl() == route('static.explanation') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.explanation') }}">Come funziona</a></li>
                            <li {{ Request::fullUrl() == route('static.why') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.why') }}">Perchè sceglierci</a></li>
                            <li {{ Request::fullUrl() == route('static.rates') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.rates') }}">Tariffe e dazi doganali</a></li>
                            <li {{ Request::fullUrl() == route('static.contacts') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.contacts') }}">Contatti</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- fixed menu top -->
                <div class="column m-1of2 t-1of3 d-1of6 header-secondary-level-links-fixed s2k-fixed-visible">
                    <ul>
                        <li {{ Request::fullUrl() == route('static.support') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.support') }}">Support</a></li>
                        <li {{ Request::fullUrl() == route('static.support') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.support') }}">F.A.Q.</a></li>
                    </ul>
                </div>
                <div class="column m-1of4 t-1of2 d-1of6 text-right header-primary-level-login">
                    <a class="red-button-like" href="{{ route('auth') }}">Accedi</a>
                </div>
                <div class="column m-1of4 t-1of6 text-right header-primary-level-mobile-menu-button">
                    <a href="#"><span class="icon icon-menu"></span></a>
                </div>
            </div>
        </div>
    </div>
</header>