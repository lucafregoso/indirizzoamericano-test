<footer>
    <div class="footer-primary-level">
        <div class="wrap">
            <div class="columns cf">
                <div class="column m-1of3 t-1of3 d-1of4 footer-primary-level-logo">
                    <a class="footer-logo" href="#"><img src="{{ asset('assets/library/images/indirizzo-americano-w.png') }}" alt="Indirizzo Americano" title="Indirizzo Americano"></a>
                </div>
                <div class="column m-2of3 t-1of3 footer-primary-level-newsletter-top">
                    <form action="#" class="newsletter-footer">
                        <input class="newsletter-field" type="email" name="newsletter" placeholder="iscriviti alla newsletter">
                        <input class="newsletter-sub-btn" value="" type="submit">
                    </form>
                </div>
                <div class="column m-1of2 t-1of3 footer-primary-level-social-top">
                    Seguici su facebook <a href="#"><span class="social-footer icon icon-facebook"></span></a>
                </div>

            </div>
            <div class="columns cf">
                <div class="column m-1of3 t-1of3 d-1of4 footer-primary-level-links">
                    <p>
                        {{ $_warehouse_info->address }}<br/>
                        {{ $_warehouse_info->city }}, {{ $_warehouse_info->zip_code }} {{ $_warehouse_info->state }}<br/>
                        Phone: {{ $_warehouse_info->phone }}<br/>
                        Fax: {{ $_warehouse_info->fax }}
                    </p>	
                </div>
                <div class="column m-1of3 t-1of3 d-1of6 footer-primary-level-links">
                    <ul>
                        <li {{ Request::fullUrl() == route('home') ? 'class=current-menu-item' : '' }}><a href="{{ route('home') }}">Home</a></li>
                        <li {{ Request::fullUrl() == route('static.explanation') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.explanation') }}">Come funziona</a></li>
                        <li {{ Request::fullUrl() == route('static.why') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.why') }}">Perchè sceglierci</a></li>
                        <li {{ Request::fullUrl() == route('static.rates') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.rates') }}">Tariffe e dazi doganali</a></li>
                        <li {{ Request::fullUrl() == route('auth') ? 'class=current-menu-item' : '' }}><a href="{{ route('auth') }}">Registrati</a></li>
                    </ul>	
                </div>
                <div class="column m-1of3 t-1of3 d-1of4 footer-primary-level-links">
                    <ul>
                        <li {{ Request::fullUrl() == route('static.support') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.support') }}">F.A.Q.</a></li>
                        <li {{ Request::fullUrl() == route('static.contacts') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.contacts') }}">Contatti</a></li>
                    </ul>	
                </div>
                <div class="column m-all t-1of2 d-1of3 footer-primary-level-newsletter-social">
                    <form action="#" class="newsletter-footer">
                        <input class="newsletter-field" id="newsletter-field" type="email" name="newsletter" placeholder="iscriviti alla newsletter" value="">
                        <input class="newsletter-sub-btn" value="" type="submit">
                    </form>
                    <div class="newsletter-result" style="display:none">
                        <span>Iscrizione avvenuta con successo.</span><br>
                    </div>
                    Seguici su facebook <a href="{{ $_warehouse_info->facebook }}" target="_blank"><span class="social-footer icon icon-facebook"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-secondary-level">
        <div class="wrap">
            <div class="columns cf">
                <div class="column m-all t-all d-all text-center footer-secondary-level-links">
                    Powered by:  <a target="_blank" href="#">Indivisual</a> | <a target="_blank" href="#">Creative Engine</a> // <a target="_blank" href="#">S2K Agency</a>
                </div>
            </div>
        </div>
    </div>
</footer>