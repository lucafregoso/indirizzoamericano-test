<header>
    <div class="header-secondary-level">
        <div class="wrap">
            <div class="columns cf">
                <div class="column m-all t-1of2 d-1of2 header-secondary-level-links">
                    <ul>
                        <li {{ Request::fullUrl() == route('static.support') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.support') }}">Support</a></li>
                        <li {{ Request::fullUrl() == route('static.support') ? 'class=current-menu-item' : '' }}><a href="{{ route('static.support') }}">F.A.Q.</a></li>
                    </ul>
                </div>
                <div class="column m-all t-1of2 d-1of2 header-secondary-level-icons">
                    <a href="mailto:{{ $_warehouse_info->email }}"><span class="icon icon-mail"></span></a>
                    <a href="{{ $_warehouse_info->facebook }}" target="_blank"><span class="icon icon-facebook"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-primary-level">
        <div class="wrap">
            <div class="columns cf">
                <div class="column m-1of2 t-1of3 d-1of4">
                    <h1 class="main-logo">
                        <a href="{{ route('home') }}"><img src="{{ asset('assets/library/images/indirizzo-americano.png') }}" alt="Indirizzo Americano" title="Indirizzo Americano"></a>
                    </h1>
                </div>
                <div class="column d-7of12 header-primary-level-main-menu">
                    <nav class="main-navigation main-navigation-login">
                        <ul>
                            <li {{ Request::fullUrl() == route('storage.dashboard') ? 'class=current-menu-item' : '' }}><a href="{{ route('storage.dashboard') }}">Home</a></li>
                            <li {{ Request::fullUrl() == route('storage.orders') ? 'class=current-menu-item' : '' }}><a href="{{ route('storage.orders') }}">I miei ordini</a></li>
                            <li {{ Request::fullUrl() == route('storage.packages') ? 'class=current-menu-item' : '' }}><a href="{{ route('storage.packages') }}">I miei pacchi</a></li>
                            <li {{ Request::fullUrl() == route('storage.personal-shopper') ? 'class=current-menu-item' : '' }}><a href="{{ route('storage.personal-shopper') }}">Personal Shopper</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="column m-1of4 t-1of2 d-1of6 text-right header-primary-level-login">
                    <a class="red-button-like" href="{{ route('user.logout') }}">Logout</a>
                </div>
                <div class="column m-1of4 t-1of6 text-right header-primary-level-mobile-menu-button">
                    <a href="#"><span class="icon icon-menu"></span></a>
                </div>
            </div>
        </div>
    </div>
</header>