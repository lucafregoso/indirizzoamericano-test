@extends('layouts/default')

{{-- Page title --}}
@section('title')
Profile
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">

            <div class="AAA">
                <div class="columns">
                    <div class="column m-all t-all d-all">
                        <div class="auth-intro spaceB10">
                            <h2 class="bold italic">Modifica profilo</h2>
                            <p>Modifica le informazioni del tuo profilo.</p>
                        </div>
                    </div>
                </div>

                @if(isset($errors) && count($errors->all()))
                    <div class="error-box">
                        @foreach ($errors->all() as $error)
                          <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif

                {!! Form::model($user, ['route' => ['user.profile.edit'], 'method' => 'post', 'files' => true]) !!}

                    <div class="columns">
                        <div class="column m-all t-1of2 d-1of2 spaceB30">
                            <div class="profile-image-upload">
                                @if ($user->pic)
                                    <img src="{{ \App\Helpers\Helper::getAvatarPublicPath($user->pic) }}" alt=""/>
                                @else
                                    <img src="{{ asset('assets/library/images/storage/profile.png') }}" alt=""/>
                                @endif
                            </div>

                            <div class="form-group">
                                {!! Form::file('avatar', old('avatar'), ['class' => 's2k-required form-control' ]) !!}
                            </div>
                        </div>
                        <div class="column m-all t-1of2 d-1of2">
                            <div class="auth-box light-blue-bg">
                                <div class="form-group">
                                    {!! Form::text('first_name', old('first_name'), ['class' => 's2k-required form-control', 'placeholder'=>'Nome' ]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('last_name', old('last_name'), ['class' => 's2k-required form-control', 'placeholder'=>'Cognome']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::date('dob', old('dob') ? old('dob') : null, ['class' => 's2k-required form-control', 'placeholder'=>'Data di nascita gg/mm/aaaa']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('tax', old('tax'), ['class' => 's2k-required form-control', 'placeholder'=>'C.Fis / P.IVA']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('phone', old('phone'), ['class' => 's2k-required form-control', 'placeholder'=>'Telefono']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('address', old('address'), ['class' => 's2k-required form-control', 'placeholder'=>'Indirizzo']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('city', old('city'), ['class' => 's2k-required form-control', 'placeholder'=>'Città']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('zip_code', old('zip_code'), ['class' => 's2k-required form-control', 'placeholder'=>'CAP']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('state', old('state'), ['class' => 's2k-required form-control', 'placeholder'=>'Stato']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::text('email', old('email'), ['class' => 's2k-required form-control', 'placeholder'=>'Email']) !!}
                                </div>

                                <!-- Form actions -->
                                <div class="form-group spaceT30 text-center">
                                    {!! Form::submit('Update', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                                </div>
                            </div>
                        </div>
                    </div>




                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>
@stop

@section('footer_scripts')
@stop
