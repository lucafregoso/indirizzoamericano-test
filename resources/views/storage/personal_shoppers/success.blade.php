@extends('layouts/default')

{{-- Page title --}}
@section('title')
Storage
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">

            <div class="columns">
                <div class="column m-1of6 t-1of8 d-1of8">
                    <img src="{{ asset('assets/library/images/storage/personal-shopper.png') }}" alt=""/>
                </div>
                <div class="column m-5of6 t-7of8 d-7of8">
                    <h2 class="italic bold">
                        Stiamo controllando il tuo ordine,<br/>
                        te lo spediremo al più presto!
                    </h2>
                </div>
            </div>

        </div>
    </div>
</div>
@stop

@section('footer_scripts')
@stop
