@extends('layouts/default')

{{-- Page title --}}
@section('title')
Storage
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">

            <div class="columns">
                <div class="column m-1of6 t-1of8 d-1of8">
                    <img src="{{ asset('assets/library/images/storage/personal-shopper.png') }}" alt=""/>
                </div>
                <div class="column m-5of6 t-7of8 d-7of8">
                    <h2 class="italic bold">
                        Ci siamo quasi!<br/>
                        Prima di procedere con la tua richiesta <br/>
                        controlla che sia tutto ok!
                    </h2> 

                    <p class="bold italic blue spaceTB20">
                        <a class="blue" href="/uploads/Indirizzo_Americano_Merci_Consentite.pdf" target="_blank">Leggi quali sono le merci consentite e i dazi doganali</a>
                        <br/>
                        Ricordati che il costo di spedizione non è incluso.
                    </p>
                </div>
            </div>

            <div class="columns">
                <div class="column-no-pad m-all t-all d-all spaceB30">
                    <hr/>
                </div>
            </div>

            {!! Form::open(['url' => route('storage.personal-shopper-confirm', $order->id), 'class' => 'form-horizontal']) !!}
                <div class="columns spaceTB30">
                    <div class="column m-all t-1of8 d-1of8 spaceB30">
                        <p class="bold blue">
                            Ordine n.<br/>
                            {{ $order->id }}
                        </p>
                    </div>
                    <div class="column m-all t-7of8 d-7of8">
                        <p class="bold italic blue">Riepilogo ordine</p>

                        <p class="spaceTB30">
                            {{ $order->reference }} <br/><br/>
                            {{ $order->note }}<br/><br/>

                            @foreach($order->products as $product)
                                {{ $product->article_code }} <br/>{{ $product->size }}, {{ $product->color }}, {{ $product->quanity }}, {{ $product->note }}<br/><br/>
                            @endforeach
                        </p>
                    </div>
                </div>

                <div class="columns">
                    <div class="column-no-pad m-all t-all d-all spaceB30">
                        <hr/>
                    </div>
                </div>

                <div class="columns">
                    <div class="column m-1of8 t-1of8 d-1of8 text-center">
                        <div class="spaceT20"></div>
                    </div>
                    <div class="column m-5of8 t-5of8 d-5of8">
                        <h4 class="light-blue italic bold">
                            Potrai tenere il tuo ordine sotto controllo <br/>
                            nella tua pagina utente!
                        </h4>
                    </div>
                    <div class="column m-2of8 t-2of8 d-2of8 text-right">
                        <!-- Form actions -->
                        <div class="form-group spaceT10">
                            {!! Form::submit('Invia richiesta', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
@stop
