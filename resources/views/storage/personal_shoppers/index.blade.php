@extends('layouts/default')

{{-- Page title --}}
@section('title')
Storage
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">

            <!-- *** errors *** -->
            @if(isset($errors) && count($errors->all()))
                <div class="spaceB30">
                    <div class="error-box">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                </div>
            @endif

            <div class="columns">
                <div class="column m-1of6 t-1of8 d-1of8">
                    <img src="{{ asset('assets/library/images/storage/personal-shopper.png') }}" alt=""/>
                </div>
                <div class="column m-5of6 t-7of8 d-7of8">
                    <h2 class="italic bold">Ciao sono il tuo personal shopper, <br/>ecco come posso aiutarti!</h2>

                    <p class="bold italic blue spaceTB20">
                        Se il tuo sito preferito non ti permette di acquistare perche è necessaria una carta di credito americana,
                        non disperare, lo posso fare io per te! <br/>
                        <br/>
                        Compila i campi qui sotto, al resto penserò io!
                    </p>
                </div>
            </div>

            {!! Form::open(['url' => route('storage.personal-shopper'), 'class' => 'form-horizontal form-personal-shopper']) !!}
            <div class="columns spaceTB30">
                <div class="column m-all t-1of8 d-1of8 spaceB30">
                    <p class="bold blue">
                    </p>
                </div>
                <div class="column m-all t-3of8 d-3of8">
                    <div class="columns">
                        <div class="column m-1of6 t-1of4 d-1of4">
                            <label for="reference" class="blue">dove</label>
                        </div>
                        <div class="column m-5of6 t-3of4 d-3of4">
                            {!! Form::text('reference', null, ['class' => 'form-control pack-value-num-input s2k-required' ]) !!}
                        </div>
                    </div>
                </div>
                <div class="column m-all t-1of2 d-1of2">
                    <div class="columns">
                        <div class="column m-1of6 t-1of6 d-1of6">
                            <label for="notes" class="blue">note</label>
                        </div>
                        <div class="column m-5of6 t-5of6 d-5of6">
                            {!! Form::textarea('note', null, ['class' => 'form-control pack-value-num-input', 'placeholder' => 'Indica qui come arrivare al tuo prodotto dal sito dove vuoi comprare.']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="column m-all t-1of8 d-1of8">

                </div>
                <div class="column m-all t-7of8 d-7of8">
                    <div id="personal-shopper-products">
                        <!-- itemCounter -->
                        {!! Form::hidden('itemCounter', 0, ['id' => 'itemCounter']) !!}

                        <div class="personal-shopper-product">
                            <div class="columns spaceB20">
                                <div class="column m-1of4 t-1of4 d-1of4">
                                    <p class="bold italic blue">Prodotto</p>
                                </div>
                                <div class="column m-3of4 t-2of4 d-2of4 text-right">
                                    <a class="personal-shopper-remove-prd" href="javascript:;" style="display:none">Rimuovi prodotto</a>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="column m-1of4 t-1of4 d-1of4">
                                    <label for="products[0][quantity]" class="blue">quantità</label>
                                </div>
                                <div class="column m-3of4 t-1of4 d-1of4">
                                    {!! Form::text('products[0][quantity]', null, ['class' => 'form-control pack-value-num-input s2k-required']) !!}
                                </div>
                                <div class="column m-1of4 t-1of4 d-1of4">
                                    <label for="products[0][article_code]" class="blue">codice articolo</label>
                                </div>
                                <div class="column m-3of4 t-1of4 d-1of4">
                                    {!! Form::text('products[0][article_code]', null, ['class' => 'form-control pack-value-num-input s2k-required']) !!}
                                </div>
                            </div>
                            <div class="columns">
                                <div class="column m-1of4 t-1of4 d-1of4">
                                    <label for="products[0][size]" class="blue">misura / taglia</label>
                                </div>
                                <div class="column m-3of4 t-1of4 d-1of4">
                                    {!! Form::text('products[0][size]', null, ['class' => 'form-control pack-value-num-input']) !!}
                                </div>
                                <div class="column m-1of4 t-1of4 d-1of4">
                                    <label for="products[0][color]" class="blue">colore</label>
                                </div>
                                <div class="column m-3of4 t-1of4 d-1of4">
                                    {!! Form::text('products[0][color]', null, ['class' => 'form-control pack-value-num-input']) !!}
                                </div>
                            </div>
                            <div class="columns">
                                <div class="column m-all t-all d-all text-right">
                                    <p class="italic">*campi obbligatori</p>
                                </div>
                            </div>
                            <div class="columns spaceT20">
                                <div class="column m-1of4 t-1of4 d-1of4">
                                    <label for="products[0][note]" class="blue">note</label>
                                </div>
                                <div class="column m-3of4 t-3of4 d-3of4">
                                    {!! Form::textarea('products[0][note]', null, ['class' => 'form-control pack-value-num-input', 'placeholder' => 'Indica qui delle specifiche o dettagli relativi al prodotto']) !!}
                                </div>
                            </div>
                        </div><!-- .personal-shopper-product -->
                    </div><!-- #personal-shopper-products -->

                    <div class="columns">
                        <div class="column-no-pad m-all t-all d-all spaceB30 text-right">
                            <a class="personal-shopper-more-prd" href="javascript:;"><span>+</span>Aggiungi prodotti</a>
                        </div>
                    </div>

                    <div class="columns">
                        <div class="column-no-pad m-all t-all d-all spaceB30">
                            <hr/>
                        </div>
                    </div>

                    <div class="columns">
                        <div class="column m-1of4 t-1of6 d-1of4 text-center">
                            <div class="spaceT20">

                            </div>
                        </div>
                        <div class="column m-1of2 t-2of3 d-2of4">
                            <h4 class="light-blue italic bold">
                                Controlla che tutto sia corretto <br/>
                                e invia la tua richiesta!
                            </h4>
                        </div>
                        <div class="column m-1of4 t-1of6 d-1of4 text-right">
                            <!-- Form actions -->
                            <div class="form-group spaceT10">
                                {!! Form::submit('Invia richiesta', ['class'=>'btn btn-responsive btn-primary btn-sm submit-personal-shopper']) !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $(document).on("click",".submit-personal-shopper", function(e){
        e.preventDefault(); 
        var validation = true;
        $(document).find('.s2k-required').each(function(index, input) {
            if ($(input).val() === '') {
                validation = false;
            }
        });

        if (validation === true) {
            $('.form-personal-shopper').submit();
        } else {
            console.log('validation KO');
        }
    });

    function childRecursive(element, func){
        // Applies that function to the given element.
        func(element);
        var children = element.children();
        if (children.length > 0) {
            children.each(function (){
                // Applies that function to all children recursively
                childRecursive($(this), func);
            });
        }
    }

    // Expects format to be xxx-#[-xxxx] (e.g. item-1 or item-1-name)
    function getNewAttr(str, newNum){
        // Split on -
        var arr = str.split('[');
        // Change the 1 to wherever the incremented value is in your id
        arr[1] = newNum+']';
        // Smash it back together and return
        return arr.join('[');
    }

    // Written with Twitter Bootstrap form field structure in mind
    // Checks for id, name, and for attributes.
    function setCloneAttr(element, value){
        // Check to see if the element has an id attribute
        if (element.attr('id') !== undefined){
            // If so, increment it
            element.attr('id', getNewAttr(element.attr('id'),value));
        } else { /*If for some reason you want to handle an else, here you go */}
        // Do the same with name...
        if(element.attr('name') !== undefined){
            element.attr('name', getNewAttr(element.attr('name'),value));
        } else {}
        // And don't forget to show some love to your labels.
        if (element.attr('for') !== undefined){
            element.attr('for', getNewAttr(element.attr('for'),value));
        } else {}
    }

    // Sets an element's value to ''
    function clearCloneValues(element){
        if (element.attr('value') !== undefined){
            element.val('');
        }
    }

    $( document ).ready(function() {
        // Accepts an element and a function
        $('.personal-shopper-more-prd').click(function(){
            //increment the value of our counter
            $('#itemCounter').val(Number($('#itemCounter').val()) + 1);
            //clone the first .item element
            var newItem = $('div.personal-shopper-product').first().clone();
            //recursively set our id, name, and for attributes properly
            childRecursive(newItem,
                // Remember, the recursive function expects to be able to pass in
                // one parameter, the element.
                function(e){
                    setCloneAttr(e, $('#itemCounter').val());
            });
            // Clear the values recursively
            childRecursive(newItem,
                function(e){
                    clearCloneValues(e);
            });

            newItem.find('.form-control').each(function(index, input) {
                $(input).val('');
            });

            newItem.find('.personal-shopper-remove-prd').show();
            // Finally, add the new div.item to the end
            newItem.appendTo($('#personal-shopper-products'));
        });

        $(document).on("click",".personal-shopper-remove-prd", function(e){
            e.preventDefault(); 
            $(this).parents('.personal-shopper-product').remove();
        });
    });
</script>

@section('footer_scripts')
@stop
