@extends('layouts/default')

{{-- Page title --}}
@section('title')
Dashboard
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">
            <div class="storage-messages" style="{{ array_has(Request::all(), 'notifications') ? 'display: true' : 'display: none' }}">
                @foreach ($notifications as $notification)
                    <div class="storage-message">
                        <a class="storage-message-close" href="javascript:;" data-id="{{ $notification->id }}"><span>X</span></a>
                        <p>
                            {{ $notification->content }}
                        </p>
                    </div>
                @endforeach
            </div>

            <h2 class="italic bold">New Dashboard</h2>

            <div class="columns">
                <div class="column-no-pad m-all t-all d-all spaceT20 spaceB50">
                    <hr/>
                </div>
            </div>

        </div>
    </div>
</div>
@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>

<!-- lightbox -->
<link  href="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.js') }}"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        $( ".storage-order-short" ).click(function() {
          $(this).toggleClass('storage-order-short-open');
          $(this).next().slideToggle();
        });

        $(".s2k-pack-value").click(function() {
            $('#pack-value-num').html($(this).data("pack"));
            $('.pack-value-num-input').attr('value', $(this).data("pack"));
        });

        $(".storage-message-close").click(function() {
            var notification_id = $(this).data('id');
            $.ajax({
                method: "POST",
                url: "{{ route('storage.notifications.update') }}",
                data: { 
                        id: notification_id,
                },
                success: function (data) {
                    //
                }
            });

            $(this).parent().remove();
        });

        $('.state-notifications').click(function(e) {
            e.preventDefault();
            $(".storage-messages").toggle();
        });

    });
</script>

@section('footer_scripts')
@stop
