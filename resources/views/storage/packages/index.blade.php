@extends('layouts/default')

{{-- Page title --}}
@section('title')
Storage
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">

            <!-- *** errors *** -->
            @if(isset($errors) && count($errors->all()))
                <div class="spaceB30">
                    <div class="error-box">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                </div>
            @endif

            {!! Form::open(['url' => route('storage.packages'), 'class' => 'form-horizontal']) !!}

            <div id="storagePackContainer">
                <div class="columns">
                    <div class="column m-1of6 t-1of8 d-1of8">
                        <img src="{{ asset('assets/library/images/storage/storage.png') }}" alt=""/>
                    </div>
                    <div class="column m-5of6 t-7of8 d-7of8">
                        <h2 class="bold italic">I pacchi nel tuo Storage</h2>
                    </div>
                </div>

                <div class="storage-pack-list">
                    <div class="storage-pack-list-head-row spaceT30 spaceB20">
                        <div class="columns">
                            <div class="column m-1of4 t-1of6 d-1of6">

                            </div>
                            <div class="column m-1of2 t-2of3 d-2of3">
                                <p class="bold italic blue">I prodotti</p>
                            </div>
                            <div class="column m-1of4 t-1of6 d-1of6 text-center">
                                <p class="bold italic blue">seleziona quelli da spedire</p>
                            </div>
                        </div>
                    </div><!-- .storage-pack-list-head-row -->

                    @foreach($packages as $package)

                        <div class="storage-pack-list-row spaceB30">
                            <div class="columns">
                                <div class="column m-1of4 t-1of6 d-1of6">
                                    <div class="storage-pack-image">
                                        @if ($package->broken == 1)
                                            <a class="storage-pack-alert" data-fancybox="" data-selector="" data-src="#pack-alert" href="javascript:;">
                                                @if ($package->haveServicePhoto())
                                                    @php $photo = $package->servicePhoto()->first()->photos->first(); @endphp
                                                    <img src="{{ url('/').'/uploads/files/'.\App\Helpers\Helper::getFilePath($photo->filename).$photo->filename.'.'.$photo->extension }}" width="100">
                                                @else
                                                    <img class="storage-no-image" src="{{ asset('assets/library/images/storage/storage-no-image.png') }}" alt=""/>
                                                @endif
                                            </a>
                                        @else
                                            @if ($package->haveServicePhoto())
                                                @php $photo = $package->servicePhoto()->first()->photos->first(); @endphp
                                                <img src="{{ url('/').'/uploads/files/'.\App\Helpers\Helper::getFilePath($photo->filename).$photo->filename.'.'.$photo->extension }}" width="100">
                                            @else
                                                <a data-fancybox="" data-selector="" data-src="#picture-request-{{ $package->id }}" href="javascript:;" data-package-id="{{ $package->id }}">
                                                    <img class="storage-no-image" src="{{ asset('assets/library/images/storage/storage-no-image.png') }}" alt=""/>
                                                </a>
                                            @endif
                                        @endif
                                    </div>
                                    <a class="red italic s2k-pack-value" data-fancybox="" data-pack="{{ $package->id }}" data-src="#pack-value" href="javascript:;">valore pacco</a>

                                </div>

                                <div class="column m-1of2 t-2of3 d-2of3">
                                    <p>Codice identificativo: {{ $package->barcode }}</p>
                                    <p>Venditore: {{ $package->reseller }}</p>
                                    @php 
                                        $serviceRequests = $package->serviceRequests()->get(); 
                                        $services_item = collect([]);
                                        foreach ($serviceRequests as $serviceRequest) {
                                            $services_item->push($serviceRequest->service->name);
                                        }
                                    @endphp
                                    <p>Servizi: {{ count($services_item) ? $services_item->implode(', ') : '' }}</p>
                                    <p>Valore: {{ $package->value ? $package->value.'€' : ''}}</p>
                                </div>
                                <div class="column m-1of4 t-1of6 d-1of6 text-center">
                                    <div class="form-group spaceT10">
                                        <label class="control-label cf">
                                            {!! Form::checkbox('packages[]', $package->id, null, ['class' => 's2k-input-checkbox form-control']) !!}
                                            <span class="s2k-input-checkbox-style"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .storage-pack-list-row -->

                        <div id="picture-request-{{ $package->id }}" class="s2k-lightbox">
                            Fotografiamo il contenuto di ogni pacco che riceviamo per voi, così puoi controllare che sia corretto e integro, le foto vengono caricate nella vostra area utente con un costo di soli 2$ a pacco.
                            <div class="text-right">
                                <div class="form-group spaceT30">
                                        <a href="{{ route('storage.packages.services.photo', $package->id) }}" class="red-button-like">Attiva il servizio</a>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div><!-- .storage-pack-list -->

                <div class="columns">
                    <div class="column-no-pad m-all t-all d-all spaceT20 spaceB50">
                        <hr/>
                    </div>
                </div>

                <div class="storage-pack-list-row spaceB30">
                    <div class="columns">
                        <div class="column m-1of4 t-1of6 d-1of6">

                        </div>
                        <div class="column m-1of2 t-2of3 d-2of3">
                            <h3 class="light-blue italic bold">
                                Controlla Tariffe e Dazi,<br/>
                                e ricordati di scegliere di indicare<br/>
                                il modo di spedizione del tuo pacco!
                            </h3>
                        </div>
                        <div class="column m-1of4 t-1of6 d-1of6 text-center">
                            <a class="s2k-next-box red-button-like" data-current="storagePackContainer" data-next="sendOptions" href="javascript:;">Prosegui</a>
                        </div>
                    </div>
                </div><!-- .storage-pack-list-row -->
            </div><!-- #storagePackContainer -->

            <div id="sendOptions">
                <div class="columns">
                    <div class="column m-1of6 t-1of8 d-1of8">
                        <img src="{{ asset('assets/library/images/storage/storage.png') }}" alt=""/>
                    </div>
                    <div class="column m-5of6 t-7of8 d-7of8">
                        <h2>Scegli come spedire</h2>
                    </div>
                </div>

                <div class="storage-pack-list">
                    <div class="storage-pack-list-head-row spaceT30 spaceB20">
                        <div class="columns">
                            <div class="column m-1of4 t-1of6 d-1of6">

                            </div>
                            <div class="column m-1of2 t-2of3 d-2of3">
                                <p class="bold italic blue">Opzioni di spedizione</p>
                            </div>
                            <div class="column m-1of4 t-1of6 d-1of6 text-center">
                                <p class="bold italic blue">seleziona quelli da spedire</p>
                            </div>
                        </div>
                    </div><!-- .storage-pack-list-head-row -->

                    @foreach($services as $service)
                        <div class="storage-pack-list-row spaceB30">
                            <div class="columns">
                                <div class="column m-1of4 t-1of6 d-1of6">

                                </div>
                                <div class="column m-1of2 t-2of3 d-2of3">
                                    <p>
                                        {{  $service->name }}<br/>
                                        {{  $service->description }}
                                    </p>
                                </div>
                                <div class="column m-1of4 t-1of6 d-1of6 text-center">
                                    <div class="form-group spaceT10">
                                        <label class="control-label cf">
                                            {!! Form::checkbox('services[]', $service->id, null, ['class' => 's2k-input-checkbox form-control', 'id' => 'service-'.$service->slug]) !!}
                                            <span class="s2k-input-checkbox-style"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .storage-pack-list-row -->
                    @endforeach

                    <div class="auth-box order-address-box light-blue-bg" style="display:none">
                        <h2>Spedisci a un altro indirizzo</h2>
                        <div class="form-group">
                            {!! Form::text('extra_address[address]', old('extra_address[address]'), ['class' => 's2k-required form-control', 'placeholder'=>'Indirizzo' ]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('extra_address[city]', old('extra_address[city]'), ['class' => 's2k-required form-control', 'placeholder'=>'Città' ]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('extra_address[zip_code]', old('extra_address[zip_code]'), ['class' => 's2k-required form-control', 'placeholder'=>'CAP' ]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('extra_address[state]', old('extra_address[state]'), ['class' => 's2k-required form-control', 'placeholder'=>'Stato' ]) !!}
                        </div>
                    </div>

                </div><!-- .storage-pack-list -->

                <div class="columns">
                    <div class="column-no-pad m-all t-all d-all spaceT20 spaceB50">
                        <hr/>
                    </div>
                </div>

                <div class="storage-pack-list-row spaceB30">
                    <div class="columns">
                        <div class="column m-1of4 t-1of6 d-1of6 text-center">
                            <div class="spaceT20">
                                <a class="s2k-next-box red-button-like" data-current="sendOptions" data-next="storagePackContainer" href="javascript:;">Indietro</a>
                            </div>
                        </div>
                        <div class="column m-1of2 t-2of3 d-2of3">
                            <h3 class="light-blue italic bold">
                                Il tuo ordine sarà preso in carico<br/>
                                e spedito al più presto!
                            </h3>
                        </div>
                        <div class="column m-1of4 t-1of6 d-1of6 text-center">
                            <!-- Form actions -->
                            <div class="form-group spaceT20">
                                {!! Form::submit('Invia richiesta', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                            </div>
                        </div>
                    </div>
                </div><!-- .storage-pack-list-row -->

            </div><!-- #sendOptions -->
            {!! Form::close() !!}

            <div id="pack-alert" class="s2k-lightbox">
                Oh no il tuo pacco è danneggiato!
                <div class="text-right">
                    <div class="form-group spaceT30">
                        {{-- {!! Form::submit('Apri un ticket', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!} --}}
                    </div>
                </div>
            </div>

            <!-- valore pacco popup // uno per tutti gli ordini -->
            <div id="pack-value" class="s2k-lightbox">
                <p class="spaceB20">Assegna un valore al tuo pacco:</p>
                {!! Form::open(['url' => route('storage.packages.value'), 'class' => 'form-horizontal']) !!}
                    <!-- input nascosto viene popolato con l'attributo DATA-PACK del link che apre il popup -->
                    <div class="form-group tax-form-field">
                        {!! Form::hidden('package', null, ['class' => 'form-control pack-value-num-input']) !!}
                    </div>
                    <div class="form-group tax-form-field">
                        {!! Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Valore pacco']) !!}
                    </div>
                    <div class="text-right">
                        <div class="form-group spaceT20">
                            {!! Form::submit('Assegna', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div><!-- #pack-value -->

        </div>
    </div>
</div>
@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>

<link  href="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.js') }}"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        $(".s2k-next-box").click(function() {
            $('#'+$(this).data("current")).hide();
            $('#'+$(this).data("next")).show();
            $('html, body').animate({scrollTop: $('#'+$(this).data("next")).offset().top}, 1000);
        });

        $(".s2k-pack-value").click(function() {
            $('#pack-value-num').html($(this).data("pack"));
            $('.pack-value-num-input').attr('value', $(this).data("pack"));
        });

        $('#service-extra_address').click(function() {
            $(".order-address-box").toggle(this.checked);
        });
    });
</script>



@section('footer_scripts')
@stop
