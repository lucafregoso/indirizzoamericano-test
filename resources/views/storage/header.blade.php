<div class="wrap spaceTB30 cf">
    <div class="columns spaceB20">
        <div class="column m-1of3 t-1of6 d-1of6">
            <a href="{{ route('storage.notifications.index') }}" class="storage-profile-img">
                <!-- without class .profile-messages-active is inactive style -->
                <span class="profile-messages {{ ($badge_notifications > 0) ? 'profile-messages-active' : '' }}">{{ $badge_notifications }}</span>
                @if ($user->pic)
                    <img src="{{ \App\Helpers\Helper::getAvatarPublicPath($user->pic) }}" alt=""/>
                @else
                    <img src="{{ asset('assets/library/images/storage/profile.png') }}" alt=""/>
                @endif
            </a>
        </div>
        <div class="column m-2of3 t-5of12 d-5of12">
            <h4 class="light-blue bold italic">
                Ciao! Ben Tornato {{ $user->first_name }}<br/>
                @if ($badge_notifications > 0)
                    Ci sono {{ $badge_notifications }} messaggi da leggere <br/><br/>
                    @if (Request::is('storage/dashboard'))
                        <a class="state-notifications" href="javascript:;">clicca qui!</a>
                    @else
                        <a class="state-notifications" href="{{ route('storage.dashboard') }}?notifications">clicca qui!</a>
                    @endif
                @else 
                    Al momento non ci sono messaggi.<br/><br/>
                    Buona giornata!
                @endif
            </h4>
        </div>
        <div class="column m-all t-5of12 d-5of12">
            <div class="columns spaceB20">
                <div class="column m-1of3 t-1of3 d-1of3 storage-menu">
                    <a {{ Request::fullUrl() == route('storage.orders') ? 'class=storage-current-block' : '' }} href="{{ route('storage.orders') }}">
                        <img src="{{ asset('assets/library/images/storage/storage.png') }}" alt=""/>
                    </a>
                </div>
                <div class="column m-1of3 t-1of3 d-1of3 storage-menu">
                    <a {{ Request::fullUrl() == route('storage.packages') ? 'class=storage-current-block' : '' }} href="{{ route('storage.packages') }}">
                        <img src="{{ asset('assets/library/images/storage/storage.png') }}" alt=""/>
                    </a>
                </div>
                <div class="column m-1of3 t-1of3 d-1of3 storage-menu">
                    <a {{ Request::fullUrl() == route('storage.personal-shopper') ? 'class=storage-current-block' : '' }} href="{{ route('storage.personal-shopper') }}">
                        <img src="{{ asset('assets/library/images/storage/personal-shopper.png') }}" alt=""/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>