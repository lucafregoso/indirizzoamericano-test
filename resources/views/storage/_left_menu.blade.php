<div class="column m-all t-1of6 d-1of6">
    <div class="storage-user-profile-infos blue">
        {{ '@'.$user->first_name }}.{{ $user->last_name }}<br/>
        <br/>
        Il tuo indirizzo: <br/>
        {{ $user->address }}<br/>
        {{ $user->city }}, {{ $user->zip_code }}, {{ $user->state }}<br/>
        <br/>
        Indirizzo Americano: <br/>
        {{ $warehouse->address }}, {{ $warehouse->city }}, <br/>
        {{ $warehouse->code }}, {{ $warehouse->state }}<br/>
        <hr/>
        <a href="{{ route('user.profile.edit') }}">Modifica profilo</a><br/>
        <a href="{{ route('storage.options') }}">Modifica le tue preferenze di spedzione</a><br/><br/>
        <a href="{{ route('storage.orders') }}">Storico Spedizioni</a>
    </div>
</div>