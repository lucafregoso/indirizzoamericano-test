@extends('layouts/default')

{{-- Page title --}}
@section('title')
Storage
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">

            <h2 class="italic bold">Le tue notifiche</h2>

                    <div class="storage-pack-list">
                        <div class="storage-pack-list-head-row spaceT30 spaceB20">
                            <div class="columns">
                                <div class="column m-1of4 t-1of6 d-1of6">
                                    <p class="bold italic blue">Data</p>
                                </div>
                                <div class="column m-1of2 t-2of3 d-2of3">
                                    <p class="bold italic blue">Notifica</p>
                                </div>
                            </div>
                        </div><!-- .storage-pack-list-head-row -->

                        @foreach ($notifications as $notification)
                            <div class="storage-pack-list-row spaceB30">
                                <div class="columns">
                                    <div class="column m-1of4 t-1of6 d-1of6">
                                        {{ $notification->created_at->diffForHumans() }}
                                    </div>

                                    <div class="column m-1of2 t-2of3 d-2of3">
                                        {{ $notification->content }}
                                    </div>
                                </div>
                            </div><!-- .storage-pack-list-row -->
                        @endforeach
                    </div><!-- .storage-pack-list -->

            <div class="columns">
                <div class="column-no-pad m-all t-all d-all spaceT20 spaceB50">
                    <hr/>
                </div>
            </div>


        </div>
    </div>
</div>
@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>

<!-- lightbox -->
<link  href="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.js') }}"></script>

@section('footer_scripts')
@stop
