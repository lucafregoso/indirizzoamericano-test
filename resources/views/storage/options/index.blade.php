@extends('layouts/default')

{{-- Page title --}}
@section('title')
Storage
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">

            <!-- *** errors *** -->
            @if(isset($errors) && count($errors->all()))
                <div class="spaceB30">
                    <div class="error-box">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                </div>
            @endif

            {!! Form::open(['url' => route('storage.options'), 'class' => 'form-horizontal']) !!}

                <div class="columns">
                    <div class="column m-1of6 t-1of8 d-1of8">
                        <img src="{{ asset('assets/library/images/storage/storage.png') }}" alt=""/>
                    </div>
                    <div class="column m-5of6 t-7of8 d-7of8">
                        <h2>Preferenze di spedizione</h2>
                    </div>
                </div>

                <div class="storage-pack-list">
                    <div class="storage-pack-list-head-row spaceT30 spaceB20">
                        <div class="columns">
                            <div class="column m-1of4 t-1of6 d-1of6">

                            </div>
                            <div class="column m-1of2 t-2of3 d-2of3">
                                <p class="bold italic blue">Opzioni di spedizione</p>
                            </div>
                            <div class="column m-1of4 t-1of6 d-1of6 text-center">
                                <p class="bold italic blue">seleziona i servizi</p>
                            </div>
                        </div>
                    </div><!-- .storage-pack-list-head-row -->

                    @foreach($services as $service)
                        <div class="storage-pack-list-row spaceB30">
                            <div class="columns">
                                <div class="column m-1of4 t-1of6 d-1of6">

                                </div>
                                <div class="column m-1of2 t-2of3 d-2of3">
                                    <p>
                                        {{  $service->name }}<br/>
                                        {{  $service->description }}
                                    </p>
                                </div>
                                <div class="column m-1of4 t-1of6 d-1of6 text-center">
                                    <div class="form-group spaceT10">
                                        <label class="control-label cf">
                                            {!! Form::checkbox('services[]', $service->id, $related_services->contains($service->id) ? true : false, ['class' => 's2k-input-checkbox form-control', 'id' => 'service-'.$service->slug]) !!}
                                            <span class="s2k-input-checkbox-style"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .storage-pack-list-row -->
                    @endforeach

                </div><!-- .storage-pack-list -->

                <div class="columns">
                    <div class="column-no-pad m-all t-all d-all spaceT20 spaceB50">
                        <hr/>
                    </div>
                </div>

                <div class="storage-pack-list-row spaceB30">
                    <div class="columns">
                        <div class="column m-1of4 t-1of6 d-1of6 text-center"></div>
                        <div class="column m-1of2 t-2of3 d-2of3"></div>
                        <div class="column m-1of4 t-1of6 d-1of6 text-center">
                            <!-- Form actions -->
                            <div class="form-group spaceT20">
                                {!! Form::submit('Aggiorna', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                            </div>
                        </div>
                    </div>
                </div><!-- .storage-pack-list-row -->

            {!! Form::close() !!}

        </div>
    </div>
</div>
@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>

<link  href="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.js') }}"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        //
    });
</script>

@section('footer_scripts')
@stop
