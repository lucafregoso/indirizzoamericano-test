@extends('layouts/default')

{{-- Page title --}}
@section('title')
Storage
@parent
@stop

{{-- content --}}
@section('content')

@include('storage/header')

<div class="wrap spaceB30 cf">
    <div class="columns spaceB20">

        @include('storage/_left_menu')

        <div class="column m-all t-5of6 d-5of6">
            <div class="storage-messages" style="{{ array_has(Request::all(), 'notifications') ? 'display: true' : 'display: none' }}">
                @foreach ($notifications as $notification)
                    <div class="storage-message">
                        <a class="storage-message-close" href="javascript:;" data-id="{{ $notification->id }}"><span>X</span></a>
                        <p>
                            {{ $notification->content }}
                        </p>
                    </div>
                @endforeach
            </div>

            <h2 class="italic bold">I tuoi ordini</h2>


            @if(count($orders))
            <div class="storage-orders">

                @foreach($orders as $order)
                    <div id="order-{{ $order->id }}" class="storage-order">
                        <div class="storage-order-short">
                            <div class="columns">
                                <div class="column m-all t-1of4 d-1of4 bold blue">
                                    {{ Carbon\Carbon::parse($order->created_at)->format('d - m - Y') }}
                                </div>
                                <div class="column m-all t-1of4 d-1of4 bold blue">
                                    {{ $order->id }}
                                </div>
                                <div class="column m-all t-1of4 d-1of4 bold blue">
                                    @if ($order->payments->contains('status', 1))
                                        {{ $order->payments()->where('payments.status', 1)->first()->total }} €
                                    @endif
                                </div>
                                <div class="column m-all t-1of4 d-1of4 bold blue">
                                    {{ \App\Helpers\Helper::getOrderStatus($order->status) }}
                                </div>
                            </div>
                        </div><!-- .storage-order-short -->

                        <div class="storage-order-container">
                            <div class="storage-order-detail">
                                <div class="columns">
                                    <div class="column m-all t-all d-all bold blue spaceB10">
                                        DETTAGLI ORDINE
                                    </div>
                                </div>

                                <div class="columns">
                                    <div class="column m-all t-1of4 d-1of4">
                                        <p class="bold blue">email</p>
                                        <p>{{ $order->user->email }}</p>

                                        <div class="spaceTB20">
                                            <p class="bold blue">metodo di pagamento</p>
                                            <p>PayPal</p>
                                        </div>
                                    </div>
                                    <div class="column m-all t-1of4 d-1of4">
                                        <p class="bold blue">opzioni di consegna</p>
                                        @foreach($order->serviceRequests as $service)
                                            <p>{{ $service->service->name }}</p>
                                        @endforeach

                                        <div class="spaceTB20">
                                            <p class="bold blue">Indirizzo di spedizione</p>
                                            <p>
                                                {{ $order->user->first_name }} {{ $order->user->last_name }}<br/>
                                                {{ $order->user->address }}<br/>
                                                {{ $order->user->state }}<br/>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="column m-all t-1of4 d-1of4">
                                        <div class="spaceB20">
                                            <p class="bold blue">codice fiscale</p>
                                            <p>{{ $order->user->tax }}</p>
                                        </div>
                                    </div>
                                    <div class="column m-all t-1of4 d-1of4">
                                        <div class="spaceB20 bold blue">
                                            <a href="#" target="_blank">traccia ordine</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @foreach($order->packages()->get() as $package)
                                <div class="storage-order-detail">
                                    <div class="columns">
                                        <div class="column m-all t-all d-all">
                                            <div class="spaceB20">
                                                <p class="bold blue">RIEPILOGO</p>
                                                <p><span class="bold blue">Pacco codice:</span> {{ $package->barcode }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="columns">
                                        <div class="column m-all t-1of4 d-1of4">
                                            @if($package->haveServicePhoto())
                                                @php $photo = $package->servicePhoto()->first()->photos->first(); @endphp
                                                <img src="{{ url('/').'/uploads/files/'.\App\Helpers\Helper::getFilePath($photo->filename).$photo->filename.'.'.$photo->extension }}" width="350">
                                            @else
                                                <img src="http://placehold.it/350x350" alt=""/>
                                            @endif
                                            <!--<a class="red s2k-pack-value" data-fancybox="" data-pack="123450" data-src="#pack-value" href="javascript:;">valore pacco</a>-->
                                        </div>
                                        <div class="column m-all t-1of4 d-1of2">
                                            <p class="spaceB20">
                                                <span class="bold blue">Descrizione: </span><br>
                                                {{ $package->description }}
                                            </p>
                                        </div>
                                        <div class="column m-all t-1of4 d-1of4">
                                            <p class="spaceB20">
                                                <span class="bold blue">Rivenditore: </span><br>
                                                {{ $package->reseller }}<br>
                                                <span class="bold blue">Valore: </span><br>
                                                {{ $package->value }}<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="storage-order-detail">
                                <div class="columns">
                                    <div class="column m-all t-all d-all text-right">
                                        Totale ordine:
                                        @if ($order->payments->contains('status', 1))
                                            {{ $order->payments()->where('payments.status', 1)->first()->total }} €
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div><!-- .storage-order-container -->
                    </div><!-- .storage-order -->
                @endforeach
            </div><!-- .storage-orders -->
            @endif

            <div class="columns">
                <div class="column-no-pad m-all t-all d-all spaceT20 spaceB50">
                    <hr/>
                </div>
            </div>

            <!-- valore pacco popup // uno per tutti gli ordini -->
            <div id="pack-value" class="s2k-lightbox">
                <p class="spaceB20">Assegna un valore al tuo pacco numero: <span id="pack-value-num"></span></p>

                <!-- input nascosto viene popolato con l'attributo DATA-PACK del link che apre il popup -->
                <div class="form-group tax-form-field">
                    {!! Form::hidden('codice-pacco', null, ['class' => 'form-control pack-value-num-input']) !!}
                </div>
                <div class="form-group tax-form-field">
                    {!! Form::text('valore-pacco', null, ['class' => 'form-control', 'placeholder' => 'Valore pacco']) !!}
                </div>
                <div class="text-right">
                    <div class="form-group spaceT20">
                        {!! Form::submit('Assegna', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
                    </div>
                </div>
            </div><!-- #pack-value -->

        </div>
    </div>
</div>
@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>

<!-- lightbox -->
<link  href="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/library/js/fancybox3/jquery.fancybox.min.js') }}"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        $( ".storage-order-short" ).click(function() {
          $(this).toggleClass('storage-order-short-open');
          $(this).next().slideToggle();
        });

        $(".s2k-pack-value").click(function() {
            $('#pack-value-num').html($(this).data("pack"));
            $('.pack-value-num-input').attr('value', $(this).data("pack"));
        });

        $(".storage-message-close").click(function() {
            var notification_id = $(this).data('id');
            $.ajax({
                method: "POST",
                url: "{{ route('storage.notifications.update') }}",
                data: { 
                        id: notification_id,
                },
                success: function (data) {
                    //
                }
            });

            $(this).parent().remove();
        });

        $('.state-notifications').click(function(e) {
            e.preventDefault();
            $(".storage-messages").toggle();
        });

    });
</script>

@section('footer_scripts')
@stop
