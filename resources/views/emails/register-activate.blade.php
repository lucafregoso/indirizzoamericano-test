@extends('emails/layouts/default')

@section('content')
<p>Hello {!! $user->first_name !!},</p>
<p>Welcome to SiteNameHere!</p>

@if (isset($password))
    <p>This is your account details:</p>
    <p>Email : {!! $user->email !!}</p>
    <p>Password : {!! $password !!}</p>
@endif

<p>Please click on the following link to confirm your SiteNameHere account:</p>

<p><a href="{!! $activationUrl !!}">{!! $activationUrl !!}</a></p>

<p>Best regards,</p>

<p>@lang('general.site_name') Team</p>
@stop
