@extends('emails/layouts/default')

@section('content')
<p>Hello {!! $user->first_name !!},</p>
<p>Order {{ $order->id }}</p>

Link Payment:
<a href="{{ $link }}" target="_blank">{{ $link }}</a>

<p>@lang('general.site_name') Team</p>
@stop
