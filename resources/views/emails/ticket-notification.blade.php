@extends('emails/layouts/default')

@section('content')
<p>Hello {!! $owner_ticket->first_name !!},</p>
<p>Answer:</p>

{!! $content !!}

<p>Best regards,</p>

<p>@lang('general.site_name') Team</p>
@stop
