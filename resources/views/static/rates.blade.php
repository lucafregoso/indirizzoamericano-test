@extends('layouts/default')

{{-- Page title --}}
@section('title')
Tariffe e dazi doganali
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">

    <article class="wrap columns cf">
        <div class="column m-all t-all d-5of6 center">
            <header class="text-center spaceB20">
                <h1 class="bold italic">Shopping senza frontiere, <br/>a prezzi ridotti!</h1>
            </header>
        </div>
    </article>


    <div class="wrap spaceB50">
        <div class="columns columns-center cf spaceT50">
            <div class="column m-all t-1of2 d-5of12 spaceB30">
                <h2 class="text-center spaceB20 bold italic">Servizi <br/>gratuiti</h2>
                <div class="rates-blu-box block-same-height">
                    <ul>
                        <li>Iscrizione</li>
                        <li>Ottimizzazione spazio</li>
                        <li>Giacenza illimitata</li>
                        <li>Spedisci subito</li>
                        <li>Rimozione documenti di accompagnamento e fatture</li>
                    </ul>
                </div>
            </div>
            <div class="column m-all t-1of2 d-5of12 spaceB30">
                <h2 class="text-center spaceB20 bold italic">Servizi a<br/>pagamento</h2>
                <div class="rates-red-box red block-same-height">
                    <ul>
                        <li>Foto del contenuto del pacco</li>
                        <li>assicurazione oltre i 100$</li>
                        <li>verifica DOA (dead on arrival)</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="wrap spaceB50">
        <div class="columns columns-center cf spaceT30">
            <div class="column m-all t-all d-5of6 spaceB30">
                <h2 class="text-center bold italic">Tariffe</h2>
                <div class="spaceT10">
                    <p class="blue spaceB20 bold italic">
                        Con il nostro calcolatore potrai avere una stima dei costi della tua spedizione, è importante sapere come viene calcolato il costo, leggi qui sotto per avere un’esperienza migliore con i tuoi acquisti!
                    </p>
                    <p class="spaceB20">
                        Quando il pacco è pronto per la spedizione viene pesato e misurato sui tre lati.Il risultato dato dalla moltiplicazione della lunghezza dei 3 lati diviso un coefficente stabilito dal vettore restituisce il "peso volumetrico" .  Attenzione!  Viene  preso  in  considerazione  il  valore  più  alto  tra  “peso  reale”  e  “peso
                        volumetrico” per determinare il costo della spedizione.

                        In alcuni casi la destinazione viene considerata dal corriere finale “zona remota” o zona disagiata” (molto probabilmente se abiti in queste zone lo sai già) , in questo caso è necessario applicare un sovrapprezzo di xxx$  che è il puro costo aggiuntivo del corriere
                    </p>
                    <p class="blue spaceB20 bold italic">
                        Eccoti un esempio per capire meglio!
                    </p>
                    <p>
                        spedire un orso di polistirolo del peso di 1 kg ma dalle dimensioni di 1 metro per lato restituirà un peso volumetrico maggiore di 1kg e per lo spazio che occupa in stiva pagherà la quota relativa al peso volumetrico, cioè al maggiore spazio occupato. Un mattone del peso di 2 kg ma dalle dimensioni piccole pagherà il maggior peso rispetto allo spazio occupato perchè il peso inciderà sul costo del trasporto.
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="wrap spaceB50">
        <div class="columns cf spaceT30">
            <div class="column m-all text-center t-all d-1of6 spaceB30">
                <img src="{{ asset('assets/library/images/explanation/tax1.png') }}" alt="" class="tax-image" />
            </div>
            <div class="column m-all t-all d-3of4 spaceB30">
                <h2 class="text-center bold italic">Calcola i costi di spedizione</h2>
                <p>
                    Trovi due calcolatori uno in libbre (LBS) e pollici (INCHES) e uno in kg e cm per tua comodità
                    ATTENZIONE tutte le nostre spedizioni vengono calcolate in lbs e pollici e i prezzi sono sempre espressi in dollari americani (USD).
                </p>


                <!-- ******************** TO DO FORM ROUTE ************************ -->
                    <div class="spaceT30 spaceB20">
                        <div class="columns cf">
                            <div class="column m-1of3 t-1of4 d-1of5">
                                <label for="cap" class="tax-cap-label blue bold italic">Inserisci<br/>il CAP</label>
                            </div>
                            <div class="column m-2of3 t-1of3 d-1of4">
                                <div class="form-group tax-form-field">
                                    {!! Form::text('cap', null, ['id' => 'spedition-zip_code', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="column m-1of3 t-1of4 d-1of5">
                                <label for="cap" class="tax-cap-label blue bold italic">Costo di<br/>spedizione</label>
                            </div>
                            <div class="column m-2of3 t-1of3 d-1of4">
                                <div class="form-group tax-form-field">
                                    <span id="spedition-value" class="tax-cap-label blue bold italic"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tax-blu-box block-spedition-eu">
                        <div class="columns cf">
                            <div class="column m-all t-1of2 d-1of2">
                                <p class="spaceB10 bold italic blue">Dimensione cm</p>
                                <div class="columns cf">
                                    <div class="column column-no-pad-left m-1of3 t-1of3 d-1of3">
                                        <div class="form-group tax-form-field">
                                            {!! Form::text('base-cm', null, ['id' => 'spedition-width', 'class' => 'form-control']) !!}
                                            <label for="base-cm">base</label>
                                        </div>
                                    </div>
                                    <div class="column m-1of3 t-1of3 d-1of3">
                                        <div class="form-group tax-form-field">
                                            {!! Form::text('altezza-cm', null, ['id' => 'spedition-height', 'class' => 'form-control']) !!}
                                            <label for="altezza-cm">altezza</label>
                                        </div>
                                    </div>
                                    <div class="column m-1of3 t-1of3 d-1of3">
                                        <div class="form-group tax-form-field">
                                            {!! Form::text('profondita-cm', null, ['id' => 'spedition-depth', 'class' => 'form-control']) !!}
                                            <label for="profondita-cm">profondità</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column m-all t-1of4 d-1of4">
                                <p class="spaceB10 bold italic blue">Peso kg</p>
                                <div class="form-group">
                                    {!! Form::text('peso-kg', null, ['id' => 'spedition-weight', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="column m-all t-1of4 d-1of4">
                                <!-- Form actions -->
                                <div class="form-group text-center">
                                    {!! Form::submit('Calcola', ['class'=>'btn btn-responsive btn-tax-rate-submit btn-sm spedition-check-eu']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tax-blu-box block-spedition-usa">
                        <div class="columns cf">
                            <div class="column m-all t-1of2 d-1of2">
                                <p class="spaceB10 bold italic blue">Dimensione pollici</p>
                                <div class="columns cf">
                                    <div class="column column-no-pad-left m-1of3 t-1of3 d-1of3">
                                        <div class="form-group tax-form-field">
                                            {!! Form::text('base-pl', null, ['id' => 'spedition-width', 'class' => 'form-control']) !!}
                                            <label for="base-cm">base</label>
                                        </div>
                                    </div>
                                    <div class="column m-1of3 t-1of3 d-1of3">
                                        <div class="form-group tax-form-field">
                                            {!! Form::text('altezza-pl', null, ['id' => 'spedition-height', 'class' => 'form-control']) !!}
                                            <label for="altezza-cm">altezza</label>
                                        </div>
                                    </div>
                                    <div class="column m-1of3 t-1of3 d-1of3">
                                        <div class="form-group tax-form-field">
                                            {!! Form::text('profondita-pl', null, ['id' => 'spedition-depth', 'class' => 'form-control']) !!}
                                            <label for="profondita-cm">profondità</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column m-all t-1of4 d-1of4">
                                <p class="spaceB10 bold italic blue">Peso lb</p>
                                <div class="form-group">
                                    {!! Form::text('peso-lb', null, ['id' => 'spedition-weight', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="column m-all t-1of4 d-1of4">
                                <!-- Form actions -->
                                <div class="form-group text-center">
                                    {!! Form::submit('Calcola', ['class'=>'btn btn-responsive btn-tax-rate-submit btn-sm spedition-check-usa']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>



    <div class="wrap spaceB50">
        <div class="columns columns-center cf">
            <div class="column m-all t-all d-5of6">
                <h2 class="text-center bold italic">Merci consentite</h2>
                <div class="spaceT10 text-center">
                    <p class="blue spaceB20 bold italic">
                        Know before you shop. Due to US export regulations and carrier restrictions, international shoppers are not able to export certain items. Before you buy items from the USA, please be sure they are not on this “Do Not Ship” list.
                    </p>
                    <p>
                        <a href="/uploads/Indirizzo_Americano_Merci_Consentite.pdf" class="red-button-like" target="_blank">Scarica il PDF</a>
                    </p>
                </div>
            </div>
        </div>
    </div>




    <div class="wrap spaceTB50">
        <div class="columns columns-center cf">
            <div class="column m-all t-all d-5of6">
                <h2 class="text-center bold italic">Dazi doganali</h2>
                <div class="spaceT10">
                    <p class="blue bold italic blue text-center spaceB20">
                        Leggete attentamente questa pagina perchè la vostra esperienza di shopping on line sia la più piacevole possibile.
                    </p>
                    <p>
                        Come ogni spedizione in ingresso da un paese extraeuropeo la vostra è soggetta a possibili dazi e controlli. Questo non vi deve spaventare perchè vi offriamo ogni supporto per semplificare il tutto. I prodotti con valore dichiarato fino a 22€ non pagano ne dazio ne iva, nessun costo aggiuntivo., mentre i prodotti di valore fino a 150€ pagano l’iva . Per quelli oltre i 150€ è prevista iva e eventuale dazio in base alla categoria di appartenenza del prodotto. Eventuali Iva e dazio sono da pagare in contanti al corriere al momento della consegna.
                        <br/><br/>
                        Al momento in cui chiuderete la vostra spedizione avrete un campo in cui indicare il valore di ciò che stiamo spedendovi , questo valore fa fede anche per l’assicurazione (fino a 100$ è gratuita)
                        <br/><br/>
                        Vi ricordiamo che ci sono dei prodotti che NON è possibile spedire, leggete attentamente il pdf delle MERCI CONSENTITE. Per ogni chiarimento potete contattarci via email o su FB.
                    </p>
                </div>
            </div>
        </div>
    </div>





</section><!-- .page-content -->




@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/library/js/jquery.matchHeight.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $( document ).ready(function() {
      $('.block-same-height').matchHeight();
  });
</script>


{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript">
    $( document ).ready(function() {

        $(document).on("click",".spedition-check-eu", function(e){
            e.preventDefault(); 
            var parent_block = $(this).parents('.block-spedition-eu');

            $fix_weight = $('#spedition-weight', parent_block).val() ? clearChar($('#spedition-weight', parent_block).val()) : null;
            $fix_weight = toLbs($fix_weight);
            if ($fix_weight) {
                if ($fix_weight < 1) {
                    $fix_weight = 1;
                }
            }

            $.ajax({
                method: "POST",
                url: "{{ route('spedition.price') }}",
                data: { 
                        zip_code: $('#spedition-zip_code').val() ? $('#spedition-zip_code').val() : 0,
                        width: $('#spedition-width', parent_block).val() ? toInch(clearChar($('#spedition-width', parent_block).val())) : null,
                        height: $('#spedition-height', parent_block).val()  ? toInch(clearChar($('#spedition-height', parent_block).val())) : null,
                        depth: $('#spedition-depth', parent_block).val() ? toInch(clearChar($('#spedition-depth', parent_block).val())) : null,
                        weight: $fix_weight,
                },
                success: function (data) {
                    $("#spedition-value").text('$ '+data.price);
                }
            })
        });

        $(document).on("click",".spedition-check-usa", function(e){
            e.preventDefault(); 
            var parent_block = $(this).parents('.block-spedition-usa');

            $fix_weight = $('#spedition-weight', parent_block).val() ? clearChar($('#spedition-weight', parent_block).val()) : null;
            if ($fix_weight) {
                if ($fix_weight < 1) {
                    $fix_weight = 1;
                }
            }

            $.ajax({
                method: "POST",
                url: "{{ route('spedition.price') }}",
                data: { 
                        zip_code: $('#spedition-zip_code').val() ? $('#spedition-zip_code').val() : 0,
                        width: $('#spedition-width', parent_block).val() ? clearChar($('#spedition-width', parent_block).val()) : null,
                        height: $('#spedition-height', parent_block).val() ? clearChar($('#spedition-height', parent_block).val()) : null,
                        depth: $('#spedition-depth', parent_block).val() ? clearChar($('#spedition-depth', parent_block).val()) : null,
                        weight: $fix_weight,
                },
                success: function (data) {
                    $("#spedition-value").text('$ '+data.price);
                }
            })
        });

        function clearChar(n) {
            return n.replace(/,/g, ".");
        }

        function toInch(n) {
            var realFeet = ((n*0.393700) / 12);
            var inches = realFeet * 12;
            var rounded = Math.round( inches * 10 ) / 10;
            return rounded;
        }

        function toLbs(pK) {
            var nearExact = pK / 0.45359237;
            console.log(nearExact);
            var lbs = Math.floor(nearExact);
            console.log(lbs);
            return lbs;
        }


    });

</script>
@stop
