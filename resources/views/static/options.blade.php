@extends('layouts/default')

{{-- Page title --}}
@section('title')
Opzioni di spedizione
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">

	<div class="wrap columns cf">
        <div class="column m-all t-all d-all">
            <header class="text-center spaceB20">
                <h1 class="italic bold">Scegli come spedire il tuo pacco!</h1>
                <p class="blue italic bold text-center spaceTB20">
                	Ogni cliente ha esigenze uniche e vogliamo che le vostre spedizioni siano fatte su misura per voi.
                </p>
                <p class="text-center">
	                Un software creato appositamente ci permette di offrirvi numerose opzioni da aggiungere alla vostra spedizione per venire incontro alle esigenze di tutti. Se avete esigenze particolari non esitate e chiedere cercheremo di soddisfarvi!
					<br/><br/>
					Ecco cosa offriamo
				</p>
            </header>
        </div>
    </div>
    

    <div class="wrap spaceB50">
        <div class="columns cf spaceTB50">
	        <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
	            <img src="{{ asset('assets/library/images/explanation/why1.png') }}" alt=""/>
	        </div>
	        <div class="column m-all t-3of4 d-3of4">
	             <h2 class="italic bold">1. Foto del contenuto di ogni pacco</h2>
	             <p>
					2 /3 foto del contenuto di ogni pacco che riceviamo per voi, le foto vengono caricate nella vostra area utente (costo 2$ a pacco).<br/>
					Potete selezionare in anticipo per quale tracking effettuare la foto (da verificare se fattibile).<br/>
					<br/>
					Non compatibile con l’opzione spedisci subito.
	             </p>
	        </div>
        </div>

        <div class="columns cf spaceTB50">
	        <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
	            <img src="{{ asset('assets/library/images/explanation/why2.png') }}" alt=""/>
	        </div>
	        <div class="column m-all t-3of4 d-3of4">
	             <h2 class="italic bold">2. Rispedizione automatica del pacco</h2>
	             <p>
					Aspettate un pacco solo e non volete attendere la notifica? Dovete spedire temporaneamente 
					ad un altro indirizzo? Puoi farlo gratuitamente! Selezionate questa opzione gratuita e quando 
					lo riceveremo sapremo subito che è da spedire e lo prepareremo immediatamente, riceverete 
					solo la richiesta di pagamento della spedizione!
	             </p>
	        </div>
        </div>

        <div class="columns cf spaceTB50">
	        <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
	            <img src="{{ asset('assets/library/images/explanation/expl3.png') }}" alt=""/>
	        </div>
	        <div class="column m-all t-3of4 d-3of4">
	             <h2 class="italic bold">3. Ottimizza lo spazio e risparmia!</h2>
	             <p>
					Volete togliere la scatola originale del prodotto per risparmiare ulteriore spazio?<br/>
					Selezionate questa opzione gratuita e elimineremo anche la scatola, un ulteriore risparmio sul volume e quindi sul costo finale.
	             </p>
	        </div>
        </div>

        <div class="columns cf spaceTB50">
	        <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
	            <img src="{{ asset('assets/library/images/explanation/why5.png') }}" alt=""/>
	        </div>
	        <div class="column m-all t-3of4 d-3of4">
	             <h2 class="italic bold">4. Richiesta prova DOA (dead on arrival)</h2>
	             <p>
					Accendiamo il vostro apparecchio elettronico per verificare che all’arrivo sia funzionante, 
					si verifica solo l’accensione non le funzioni , costo 10$
	             </p>
	        </div>
        </div>

        <div class="columns cf spaceTB50">
	        <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
	            <img src="{{ asset('assets/library/images/explanation/why4.png') }}" alt=""/>
	        </div>
	        <div class="column m-all t-3of4 d-3of4">
	             <h2 class="italic bold">5. Assicurazione sul valore</h2>
	             <p>
					Assicurazione sul valore della spedizione oltre i 100$,  3$ di costo
	             </p>
	        </div>
        </div>
    </div>


    <div class="wrap spaceB50">
		<div class="columns cf">
	        <div class="column m-all text-center">
	            <a class="red-button-like" href="{{ route('auth') }}">Accedi al tuo profilo</a>
	        </div>
        </div>
    </div>


</section><!-- .page-content -->




@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
