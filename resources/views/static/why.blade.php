@extends('layouts/default')

{{-- Page title --}}
@section('title')
Perchè sceglierci
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">

    <article class="wrap columns cf">
        <div class="column m-all t-all d-5of6 center">
            <header class="text-center spaceB20">
                <h1 class="italic bold">Perchè scegliere Indirizzo Americano</h1>
                <p class="italic bold blue">
                    Ti offriamo un sito e supporto interamente in italiano con prezzi chiari e competitivi,
                    la spedizione è fatta su misura in base alle tue esigenze e i tuoi pacchi sono gestiti da un software di altissimo livello creato su misura per questo servizio. <br/>
                    <br/>
                    I nostri servizi
                </p>
            </header>
        </div>
    </article>


    <div class="wrap spaceB50">
        <div class="columns columns-center cf spaceT50">
            <div class="column m-all t-1of3 d-1of4 text-center why_img spaceB50">
                <img src="{{ asset('assets/library/images/explanation/why1.png') }}" alt=""/>
                <p class="why-text italic bold blue">Fotografiamo l’interno del tuo pacco per vedere che tutto sia integro e corretto</p>

                <div class="why-blu-box text-center bold blue">
                    Selezionalo al momento della spedizione
                </div>
            </div>
            <div class="column m-all t-1of3 d-1of4 text-center why_img spaceB50">
                <img src="{{ asset('assets/library/images/explanation/why2.png') }}" alt=""/>
                <p class="why-text italic bold blue">Rispedizione automatica del pacco</p>

                <div class="why-blu-box text-center bold blue">
                    Selezionalo al momento della spedizione
                </div>
            </div>
            <div class="column m-all t-1of3 d-1of4 text-center why_img spaceB50">
                <img src="{{ asset('assets/library/images/explanation/why3.png') }}" alt=""/>
                <p class="why-text italic bold blue">Ottimizzazione del pacco. Togliemo le scatole per ridurre lo spazio e farti risparmiare sul costo della spedizione!</p>

                <div class="why-blu-box text-center bold blue">
                    Selezionalo al momento della spedizione
                </div>
            </div>
        </div>
    </div>

    <div class="wrap spaceB50">
        <div class="columns columns-center cf spaceB50">
            <div class="column m-all t-1of3 d-1of4 text-center why_img spaceB50">
                <img src="{{ asset('assets/library/images/explanation/why4.png') }}" alt=""/>
                <p class="why-text italic bold blue">Se il tuo pacco è di valore superiore ai 100$ ti diamo la possibilità di assciurarlo!</p>

                <a class="red-button-like" href="{{ route('storage.packages') }}">Attiva <br/>l'assicurazione</a>
            </div>
            <div class="column m-all t-1of3 d-1of4 text-center why_img spaceB50">
                <img src="{{ asset('assets/library/images/explanation/why5.png') }}" alt=""/>
                <p class="why-text italic bold blue">Verifichiamo che il vostro apparecchio elettronico per verificare che all’arrivo si accenda</p>

                <a class="red-button-like" href="{{ route('storage.packages') }}">Richiedi la prova DOA <br/>(dead on arrival)</a>
            </div>
            <div class="column m-all t-1of3 d-1of4 text-center why_img spaceB50">
                <img src="{{ asset('assets/library/images/explanation/why6.png') }}" alt=""/>
                <p class="why-text italic bold blue">Personal Shopper ti permette di acquistare anche nei negozi dove viene richiesta una carta di credito americana.</p>

                <a class="red-button-like" href="{{ route('storage.personal-shopper') }}">Richiedi il <br/>personal shopper</a>
            </div>
        </div>
    </div>


    <div class="wrap columns cf">
        <div class="column m-all">
            <hr/>
        </div>
    </div>


    <div class="wrap spaceB50">
        <div class="columns cf">
            <div class="column m-all t-all d-5of6 center text-center">
                <h2 class="bold italic">Risparmio garantito!</h2>
                <p class="bold italic blue">Sono diversi i fattori che vi faranno risparmiare negli acquisti negli USA , questo non significa però che SEMPRE si risparmia, è una valutazione che dovrete fare voi come in ogni acquisto.</p>
            </div>
        </div>
        <div class="columns columns-center risparmio-list cf spaceTB30">
            <div class="column m-all t-1of2 d-5of12">
                <ul>
                    <li>
                        Molto spesso i prezzi dei prodotti sono più bassi perchè la tassazione è minore e il mercato è molto competitivo, questo determina già dei prezzi di partenza più bassi.
                    </li>
                    <li>
                        Ci sono ricorrenze in cui vengono applicati sconti molto forti, seguiteci su Facebook per essere aggiornati.
                    </li>
                    <li>
                        Conviene spedire più articoli insieme per ottimizzare la spedizione. Infatti apriamo i vostri pacchi e riduciamo il volume finale portando un forte risparmio sulla spedizione.
                    </li>
                </ul>
            </div>
            <div class="column m-all t-1of2 d-5of12">
                <ul>
                    <li>
                        Tenete d’occhio il tasso di cambio del dollaro, può farvi risparmiare ancora qualcosa.
                    </li>
                    <li>
                        Alcuni prodotti sono addirittura introvabili in Europa e questo rende ancora più interessante lo shopping
                    </li>
                    <li>
                        L’offerta di prodotti negli USA è davvero impensabile!
                    </li>
                </ul>
            </div>
        </div>
    </div>





    <div class="light-grey-bg">
        <div class="wrap spaceTB30">
            <div class="columns cf">
                <div class="column m-all t-all d-5of6 center text-center">
                    <h2 class="bold italic">Cosa dicono i nostri clienti</h2>
                </div>
            </div>
            <div class="columns cf columns-middle">
                <div class="column m-1of12 t-1of12 d-1of12 buttons-slider">
                    <div id="prev" class="arrow-left-slider-clients"></div>
                </div>
                <div class="column m-5of6 t-5of6 d-5of6 text-center">
                    <div id="slides-clients-comments" class="red cf">
                        <ul class="spaceT20">
                            <li class="slide">
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br/><br/>- Nome Utente -
                            </li>
                            <li class="slide">
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br/><br/>- Nome Utente -
                            </li>
                            <li class="slide">
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br/><br/>- Nome Utente -
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="column m-1of12 t-1of12 d-1of12 buttons-slider text-right">
                    <div id="next" class="arrow-right-slider-clients"></div>
                </div>

            </div>
        </div>
    </div>


    <article class="wrap columns spaceT30 cf">
        <div class="column m-all t-all d-all">
            <header class="text-center spaceB20">
                <h2 class="bold italic">Se sei soddisfatto lo siamo anche noi!</h2>
            </header>
            <section class="home-brands">
                <div class="columns columns-center">
                    <div class="column m-1of2 t-1of4 d-1of4 text-center">
                        <div class="home-contacts-img">
                            <img src="{{ asset('assets/library/images/homepage/like.png') }}" alt=""/>
                        </div>
                        <div class="home-plus-text">
                            <p class="bold italic blue">Condividi la tua esperienza sulla nostra pagina facebook!</p>

                            <a class="red-button-like" href="{{ $_warehouse_info->facebook }}" target="_blank">like us on facebook</a>
                        </div>
                    </div>
                    <div class="column m-1of2 t-1of4 d-1of4 text-center">
                        <div class="home-contacts-img">
                            <img src="{{ asset('assets/library/images/homepage/contacts.png') }}" alt=""/>
                        </div>
                        <div class="home-plus-text">
                            <p class="bold italic blue">Dicci come possiamo migliorare il nostro servizio!</p>

                            <a class="red-button-like" href="{{ route('static.contacts') }}">contattaci</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </article>



</section><!-- .page-content -->




@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<!-- slider commenti clienti -->
<script type="text/javascript">
    $(document).ready(function () {
      //rotation speed and timer
      var speed = 5000;

      var run = setInterval(rotate, speed);
      var slides = $('.slide');
      var container = $('#slides-clients-comments ul');
      var elm = container.find(':first-child').prop("tagName");
      var item_width = container.width();
      var previous = 'prev'; //id of previous button
      var next = 'next'; //id of next button
      slides.width(item_width); //set the slides to the correct pixel width
      container.parent().width(item_width);
      container.width(slides.length * item_width); //set the slides container to the correct total width
      container.find(elm + ':first').before(container.find(elm + ':last'));
      resetSlides();


      //if user clicked on prev button

      $('.buttons-slider div').click(function (e) {
        //slide the item

        if (container.is(':animated')) {
          return false;
        }
        if (e.target.id == previous) {
          container.stop().animate({
            'left': 0
          }, 1500, function () {
            container.find(elm + ':first').before(container.find(elm + ':last'));
            resetSlides();
          });
        }

        if (e.target.id == next) {
          container.stop().animate({
            'left': item_width * -2
          }, 1500, function () {
            container.find(elm + ':last').after(container.find(elm + ':first'));
            resetSlides();
          });
        }

        //cancel the link behavior
        return false;

      });

      //if mouse hover, pause the auto rotation, otherwise rotate it
      container.parent().mouseenter(function () {
        clearInterval(run);
      }).mouseleave(function () {
        run = setInterval(rotate, speed);
      });


      function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
          'left': -1 * item_width
        });
      }

    });
    //a simple function to click next link
    //a timer will call this function, and the rotation will begin

    function rotate() {
      $('#next').click();
    }
</script>


@section('footer_scripts')
@stop
