@extends('layouts/default')

{{-- Page title --}}
@section('title')
Support
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">


	<div class="wrap spaceB50">
        <div class="columns columns-center cf spaceT50">
	        <div class="column m-all t-1of2 d-5of12 spaceB30">
	            <div class="suport-box text-center block-same-height">
					<h2 class="text-center spaceB20 bold italic">Problemi <br/>con la spedizione?</h2>
					<p class="text-center"><img class="supp-img" src="{{ asset('assets/library/images/explanation/supp1.png') }}" alt=""/></p>
					<a href="#" class="red-button-like" target="_blank">Apri un Ticket</a>
	            </div>
	        </div>
	        <div class="column m-all t-1of2 d-5of12 spaceB30">
	            <div class="suport-box text-center block-same-height">
					<h2 class="text-center spaceB20 bold italic">Il tuo pacco <br/>è danneggiato o errato?</h2>	
					<p class="text-center"><img class="supp-img" src="{{ asset('assets/library/images/explanation/supp2.png') }}" alt=""/></p>
					<a href="#" class="red-button-like" target="_blank">Apri un Ticket</a>
	            </div>
	        </div>
        </div>
	</div>

	<div class="wrap columns cf">
        <div class="column m-all">
            <hr/>
        </div>
    </div>


	<div class="wrap spaceT50">
        <div class="columns cf">
	        <div class="column m-all t-all d-all center text-center">
	            <h2 class="bold italic">Le domande più frequenti dei nostri utenti!</h2>
	        </div>
	    </div>
        <div class="columns columns-center cf spaceT50">
	        <div class="column m-all t-1of3 d-1of3 spaceB50">
	            <ul>
					<li>Che indirizzo devo indicare quando mi registro?</li>
					<li>P.O. Box è una casella postale?</li>
					<li>Devo pagare un abbonamento mensile?</li>
					<li>Ci sono dazi doganali?</li>
					<li>Fate voi la dichiarazione doganale?</li>
					<li>È un sito sicuro?</li>
					<li>Come posso pagare?</li>
	            </ul>
	        </div>
	        <div class="column m-all t-1of3 d-1of3 spaceB50">
	            <ul>
					<li>Un articolo è arrivato rotto cosa posso fare?</li>
					<li>Cosa fare in caso di un prodotto arrivato sbagliato?</li>
					<li>Come posso contattarvi?</li>
					<li>Che indirizzo metto quando faccio un ordine su un sito americano?</li>
					<li>Il venditore accetta solo carte di credito americane, come posso comprare?</li>
	            </ul>
	        </div>
	        <div class="column m-all t-1of3 d-1of3 spaceB50">
	            <ul>
					<li>Posso spedire ad un indirizzo differente temporaneamente?</li>
					<li>Ci sono articoli che non si possono spedire?</li>
					<li>Posso controllare prima di spedire che quello che è arrivato sia giusto?</li>
					<li>Quanto posso tenere i pacchi presso il vostro magazzino?</li>
	            </ul>
	        </div>
        </div>
	</div>



</section><!-- .page-content -->




@stop

{{-- footer scripts --}}
<script src="{{ asset('assets/library/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/library/js/jquery.matchHeight.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $( document ).ready(function() {
      $('.block-same-height').matchHeight();
  });
</script>


@section('footer_scripts')
@stop
