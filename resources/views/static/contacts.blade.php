@extends('layouts/default')

{{-- Page title --}}
@section('title')
Contatti
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">

	<div class="wrap columns spaceB50 cf">
        <div class="column m-all t-all d-5of6 center">
            <header class="text-center spaceB20">
                <img class="contacts-img" src="{{ asset('assets/library/images/homepage/contacts.png') }}" alt=""/>
                <h1 class="bold italic">Ciao, come possiamo aiutarti?</h1>
                <p class="blue bold italic">
                	Se hai delle domande, sei nel posto giusto! 
				</p>
            </header>
        </div>
    </div>


	<div class="wrap spaceT50">
        <div class="columns cf">
	        <div class="column m-all t-all d-all center text-center">
	            <h2 class="bold italic">Prima di contattarci leggi le FAQ!</h2>
	        </div>
	    </div>
        <div class="columns columns-center cf spaceT50">
	        <div class="column m-all t-1of3 d-1of3 spaceB50">
	            <ul>
					<li>Che indirizzo devo indicare quando mi registro?</li>
					<li>P.O. Box è una casella postale?</li>
					<li>Devo pagare un abbonamento mensile?</li>
					<li>Ci sono dazi doganali?</li>
					<li>Fate voi la dichiarazione doganale?</li>
					<li>È un sito sicuro?</li>
					<li>Come posso pagare?</li>
	            </ul>
	        </div>
	        <div class="column m-all t-1of3 d-1of3 spaceB50">
	            <ul>
					<li>Un articolo è arrivato rotto cosa posso fare?</li>
					<li>Cosa fare in caso di un prodotto arrivato sbagliato?</li>
					<li>Come posso contattarvi?</li>
					<li>Che indirizzo metto quando faccio un ordine su un sito americano?</li>
					<li>Il venditore accetta solo carte di credito americane, come posso comprare?</li>
	            </ul>
	        </div>
	        <div class="column m-all t-1of3 d-1of3 spaceB50">
	            <ul>
					<li>Posso spedire ad un indirizzo differente temporaneamente?</li>
					<li>Ci sono articoli che non si possono spedire?</li>
					<li>Posso controllare prima di spedire che quello che è arrivato sia giusto?</li>
					<li>Quanto posso tenere i pacchi presso il vostro magazzino?</li>
	            </ul>
	        </div>
        </div>
	</div>


	<div class="wrap spaceT50">
        <div class="columns cf">
	        <div class="column m-all t-all d-all center text-center">
	            <h2 class="spaceB20 bold italic">Contattaci su Facebook per il Costumer Service!</h2>
	            <a href="#"><img class="contacts-img" src="{{ asset('assets/library/images/homepage/like.png') }}" alt=""/></a>
	        </div>
	    </div>
	</div>	


	<div class="wrap spaceT50">
        <div class="columns cf">
	        <div class="column m-all t-all d-all center text-center">
	            <h2 class="bold italic">Hai domande sulle spedizioni o sui nostri servizi? Scrivici!</h2>
	            <p class="blue spaceT20 bold italic">
					Per informazioni scirvi a: <a href="mailto:info@indirizzoamericano.it">info@indirizzoamericano.it</a>
					<br/><br/>
					Per problemi coi pagapenti: <a href="mailto:amministrazione@indirizzoamericano.it">amministrazione@indirizzoamericano.it</a>
	            </p>
	        </div>
	    </div>
	</div>	




</section><!-- .page-content -->




@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
