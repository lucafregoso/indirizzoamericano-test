@extends('layouts/default')

{{-- Page title --}}
@section('title')
TEMP
@parent
@stop

{{-- content --}}
@section('content')


<div class="wrap spaceTB30 cf">
    <div class="columns spaceB20">
        <div class="column m-all t-all d-all text-center">
            <h1 class="bold italic">I tuoi ticket</h1>
        </div>
    </div>        
</div>



<div class="wrap spaceB30 cf">
    <div class="columns spaceB10">
        <div class="column m-1of12 t-1of12 d-1of12">
            
        </div>
        <div class="column m-11of12 t-11of12 d-11of12">
            <div class="tickets-header hidden-xs">
                <div class="columns">
                    <div class="column m-1of12 t-1of4 d-1of4">
                        <h5 class="bold italic">Tipologia di ticket</h5>
                    </div>
                    <div class="column m-1of12 t-1of4 d-1of3">
                        <h5 class="bold italic">Oggetto</h5>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        <h5 class="bold italic">Stato del ticket</h5>
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        <h5 class="bold italic">Risposte</h5>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        <h5 class="bold italic">data</h5>
                    </div>
                </div>
            </div>
        </div>
    </div> 


    <div class="columns spaceB10 column-middle">
        <div class="column m-1of6 t-1of12 d-1of12 text-center ">
            <span class="ticket-alert">!</span>
        </div>
        <div class="column m-5of6 t-11of12 d-11of12">
            <div class="ticket-collapsed">
                <div class="columns column-middle">
                    <div class="column-no-pad m-all t-1of4 d-1of4">
                        <a href="#">
                            <div class="ticket-open ticket-shipping ticket-reference">
                                Ticket Spedizione <br/>
                                n° 12345
                            </div>
                        </a>
                    </div>
                    <div class="column m-all t-1of4 d-1of3">
                        <p class="">
                            Titolo, oggetto del ticket
                        </p>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        aperto
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        3 post
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        gg/mm/aa
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <div class="columns column-middle spaceB10">
        <div class="column m-1of6 t-1of12 d-1of12 text-center">
            <span class="ticket-alert">!</span>
        </div>
        <div class="column m-5of6 t-11of12 d-11of12">
            <div class="ticket-collapsed">
                <div class="columns column-middle">
                    <div class="column-no-pad m-all t-1of4 d-1of4">
                        <a href="#">
                            <div class="ticket-open ticket-pacco ticket-reference">
                                Ticket Pacco <br/>
                                n° 12345
                            </div>
                        </a>
                    </div>
                    <div class="column m-1of12 t-1of4 d-1of3">
                        <p class="">
                            Titolo, oggetto del ticket
                        </p>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        aperto
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        3 post
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        gg/mm/aa
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="columns column-middle spaceB10">
        <div class="column m-1of6 t-1of12 d-1of12 text-center">
            <span class="ticket-ok"></span>
        </div>
        <div class="column m-5of6 t-11of12 d-11of12">
            <div class="ticket-collapsed">
                <div class="columns column-middle">
                    <div class="column-no-pad m-all t-1of4 d-1of4">
                        <a href="#">
                            <div class="ticket-closed ticket-pacco ticket-reference">
                                Ticket Pacco <br/>
                                n° 12345
                            </div>
                        </a>
                    </div>
                    <div class="column m-1of12 t-1of4 d-1of3">
                        <p class="">
                            Titolo, oggetto del ticket
                        </p>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        aperto
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        3 post
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        gg/mm/aa
                    </div>
                </div>
            </div>
        </div>
    </div> 


    <div class="columns column-middle spaceB10">
        <div class="column m-1of6 t-1of12 d-1of12 text-center">
            <span class="ticket-ok"></span>
        </div>
        <div class="column m-5of6 t-11of12 d-11of12">
            <div class="ticket-collapsed">
                <div class="columns column-middle">
                    <div class="column-no-pad m-all t-1of4 d-1of4">
                        <a href="#">
                            <div class="ticket-closed ticket-shipping ticket-reference">
                                Ticket Pacco <br/>
                                n° 12345
                            </div>
                        </a>
                    </div>
                    <div class="column m-1of12 t-1of4 d-1of3">
                        <p class="">
                            Titolo, oggetto del ticket
                        </p>
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        aperto
                    </div>
                    <div class="column m-1of12 t-1of12 d-1of12 text-center">
                        3 post
                    </div>
                    <div class="column m-1of12 t-1of6 d-1of6 text-center">
                        gg/mm/aa
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>





<br/>
<br/>
<br/>
<br/>
==========<br/>
<br/>
NUOVA PAGINA<br/>
<br/>
==========<br/>
<br/>
<br/>
<br/>
<br/>



<div class="wrap spaceTB30 cf">
    <div class="columns spaceB20">
        <div class="column m-all t-all d-all text-center">
            <h2 class="bold italic spaceB30">Questo ticket contiene 2 post, ultimo in data 18/12/2016</h2>

            <a href="#" class="bold italic">< Torna all'elenco dei ticket</a>
        </div>
    </div>        
</div>




<div class="wrap spaceB30 cf">
    <div class="ticket-head">
        <div class="columns spaceTB10 column-middle">
            <div class="column m-all t-1of4 d-2of3">
                Titolo, oggetto del ticket
            </div>
            <div class="column-no-pad m-1of2 t-1of4 d-1of4">
                <div class="ticket-pacco ticket-reference">
                    Ticket Pacco n° 12345
                </div>
            </div> 
            <div class="column m-1of2 t-1of12 d-1of12 text-center">
                <span class="ticket-alert">!</span>
            </div>
        </div>
    </div>
    <div class="ticket-first">
        <div class="columns spaceTB10">
            <div class="column m-1of5 t-1of12 d-1of12 text-center">
                <div class="storage-profile-img">
                    <img src="{{ asset('assets/library/images/storage/profile.png') }}" alt=""/>
                </div>
            </div>
            <div class="column m-4of5 t-11of12 d-11of12">
                <div class="ticket-user">@nomeutente</div>
                <div class="ticket-date">18 dicembre 2016, ore 3.00 pm</div>
                <div class="ticket-text-cont">
                    testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket
                </div>
            </div>
        </div>
    </div>

    <div class="ticket-answer">
        <div class="columns spaceTB10">
            <div class="column m-1of5 t-1of12 d-1of12 text-center">
                <div class="ticket-profile-img">
                    <img src="{{ asset('assets/library/images/indirizzo-americano-small.png') }}" alt=""/>
                </div>
            </div>
            <div class="column m-4of5 t-11of12 d-11of12">
                <div class="ticket-user">Indirizzo Americano</div>
                <div class="ticket-date">18 dicembre 2016, ore 3.00 pm</div>
                <div class="ticket-text-cont">
                    testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket testo del ticket
                </div>
            </div>
        </div>
    </div>




    <div class="ticket-answer-form">
        <div class="columns spaceTB10">
            <div class="column m-all t-all d-all">
                <div class="bold italic spaceB10">Rispondi a Indirizzo Americano</div>

                {!! Form::textarea('note', null, ['class' => 'form-control pack-value-num-input', 'placeholder' => 'Scrivi qui la tua risposta.']) !!}
            </div>
        </div>
        <div class="columns spaceB10">
            <div class="column m-1of2 t-1of2 d-1of2">
                <label class="control-label cf">
                    {!! Form::checkbox('email-answer', 1, null, ['class' => 's2k-input-checkbox form-control']) !!}
                    <span class="s2k-input-checkbox-style to-left"></span>
                    <span class="to-left checkbox-text spaceT5">Avvisami delle risposte via mail</span>
                </label>
            </div>
            <div class="column m-1of2 t-1of2 d-1of2 text-right">
                <!-- Form actions -->
                {!! Form::submit('Invia', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
            </div>
        </div>
    </div>

</div>






<br/>
<br/>
<br/>
<br/>
==========<br/>
<br/>
NUOVA PAGINA<br/>
<br/>
==========<br/>
<br/>
<br/>
<br/>
<br/>





<div class="wrap spaceTB30 cf">
    <div class="columns spaceB20">
        <div class="column m-all t-all d-all text-center">
            <h1 class="bold italic">Apri un ticket</h1>
        </div>
    </div>        
</div>


<div class="wrap spaceB30 cf">
    <div class="ticket-answer-form">
        <div class="columns spaceTB10">
            <div class="column m-all t-all d-all">
                <div class="bold italic spaceB10">Crea il tuo ticket</div>
                
                {!! Form::text('ticket-object', null, ['class' => 'form-control', 'placeholder' => 'Oggetto']) !!}
            </div>
        </div>
        <div class="columns spaceB10">
            <div class="column m-1of2 t-1of2 d-1of2">
                {{ Form::select('ticket-type', ['Ticket pacco', 'Ticket spedizione', 'Ticket personal shopper']) }}
            </div>
            <div class="column m-1of2 t-1of2 d-1of2">
                {!! Form::text('ticket-reference', null, ['class' => 'form-control', 'placeholder' => 'Riferimento pacco']) !!}
            </div>
        </div>
        <div class="columns spaceB10">
            <div class="column m-all t-all d-all">
                {!! Form::textarea('note', null, ['class' => 'form-control pack-value-num-input', 'placeholder' => 'Scrivi qui la tua risposta.']) !!}
            </div>
        </div>
        <div class="columns spaceB10">
            <div class="column m-1of2 t-1of2 d-1of2">
                <label class="control-label cf">
                    {!! Form::checkbox('email-answer', 1, null, ['class' => 's2k-input-checkbox form-control']) !!}
                    <span class="s2k-input-checkbox-style to-left"></span>
                    <span class="to-left checkbox-text spaceT5">Avvisami delle risposte via mail</span>
                </label>
            </div>
            <div class="column m-1of2 t-1of2 d-1of2 text-right">
                <!-- Form actions -->
                {!! Form::submit('Invia', ['class'=>'btn btn-responsive btn-primary btn-sm']) !!}
            </div>
        </div>
    </div>
</div>




@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
