@extends('layouts/default')

{{-- Page title --}}
@section('title')
Come funziona
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">

    <article class="wrap columns cf">
        <div class="column m-all t-all d-all">
            <header class="text-center spaceB20">
                <h1 class="italic bold">Come funziona? Guarda il video</h1>
            </header>
            <section class="home-video">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/pllt7JlI5fM" frameborder="0" allowfullscreen></iframe>
            </section>
        </div>
    </article>

    <div class="wrap columns cf">
        <div class="column m-all">
            <hr/>
        </div>
    </div>


    <div class="wrap spaceB50">
        <div class="columns cf spaceTB50">
            <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
                <img src="{{ asset('assets/library/images/explanation/expl1.png') }}" alt=""/>
                <a class="red-button-like" href="{{ route('auth') }}">Registrati</a>
            </div>
            <div class="column m-all t-3of4 d-3of4">
                 <h2 class="italic bold">1. Ottieni il tuo Indirizzo Americano</h2>
                 <p>
                    Ti offriamo un sito e supporto interamente in italiano con prezzi chiari e competitivi,
                    la spedizione è fatta su misura in base alle tue esigenze e i tuoi pacchi sono gestiti da un software di altissimo livello creato su misura per questo servizio.
                 </p>
            </div>
        </div>

        <div class="columns cf spaceTB50">
            <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
                <img src="{{ asset('assets/library/images/explanation/expl2.png') }}" alt=""/>
                <a class="red-button-like" href="{{ route('storage.personal-shopper') }}">Richiedi il <br/>personal shopper</a>
            </div>
            <div class="column m-all t-3of4 d-3of4">
                 <h2 class="italic bold">2. Fai acquisti on line sui tuoi siti preferiti negli USA</h2>
                 <p>
                    Se il tuo sito preferito non ti permette di acquistare perche è necessaria una carta
                    di credito americana, non disperare, lo posso fare io per te!
                    <br/>
                    <br/>
                    <a class="red-link" href="{{ route('static.options') }}">Scopri tutti i servizi</a>
                 </p>
            </div>
        </div>

        <div class="columns cf spaceTB50">
            <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
                <img src="{{ asset('assets/library/images/explanation/expl3.png') }}" alt=""/>
                <a class="red-button-like" href="{{ route('static.options') }}">Scopri<br/>i vantaggi</a>
            </div>
            <div class="column m-all t-3of4 d-3of4">
                 <h2 class="italic bold">3. Scegli quando e come ricevere la spedizione</h2>
                 <p>
                    Quando riceveremo i tuoi ordini sarai immediatamente informato e potrai decidere se farteli recapitare o lasciarli in deposito gratuito da noi in attesa di altri pacchi.<br/>
                    Una volta che ci avrai chiesto di spedire il tutto indicherai il valore della spedizione (per fini doganali e assicurativi) e appena pronto il pacco riceverai l’ammontare della spedizione.
                    <br/>
                    <br/>
                    <a class="red-link" href="{{ route('static.options') }}">Opzioni di Spedizione  |  Dazi Doganali  |  Tariffe</a>
                 </p>
            </div>
        </div>

        <div class="columns cf spaceTB50">
            <div class="column m-all t-1of4 d-1of4 text-center expl_img spaceB30">
                <img src="{{ asset('assets/library/images/explanation/expl4.png') }}" alt=""/>
                <a class="red-button-like" href="{{ route('static.track') }}">Track order</a>
            </div>
            <div class="column m-all t-3of4 d-3of4">
                 <h2 class="italic bold">4. Ricevi i tuoi acquisti velocemente e <br/>comodamente a casa tua senza pensieri</h2>
                 <p>
                    Dal momento del pagamento in 2 /3 giorni lavorativi riceverai il tutto a casa!
                 </p>
            </div>
        </div>
    </div>


    <div class="light-blue-bg spaceTB50">
        <div class="columns cf">
            <div class="column m-all text-center">
                <h2 class="italic bold">Non sei ancora convinto?</h2>
                <a class="red-button-like" href="{{ route('static.options') }}">Scopri i vantaggi</a>
            </div>
        </div>
    </div>


</section><!-- .page-content -->




@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
