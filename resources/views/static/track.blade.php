@extends('layouts/default')

{{-- Page title --}}
@section('title')
Track order
@parent
@stop

{{-- content --}}
@section('content')

<section class="page-content">

	<div class="wrap columns spaceB50 cf">
        <div class="column m-all t-all d-5of6 center">
            <header class="text-center spaceB20">
                <img class="track-img" src="{{ asset('assets/library/images/explanation/track.png') }}" alt=""/>
                <h1 class="bold italic">Rintraccia il tuo pacco  con il track order!</h1>
                <div class="light-grey2 text-center">
                	<p class="bold blue italic">
                        Una volta richiesta la spedizione del tuo pacco, potrai monitorarlo ogni volta che vuoi nella tua pagina utente grazie al tuo track order number!
                    </p>
                	<p class="spaceT50">
                		<a class="red-button-like" href="{{ route('auth') }}">Accedi</a>
                	</p>
				</div>
            </header>
        </div>
    </div>




</section><!-- .page-content -->




@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
