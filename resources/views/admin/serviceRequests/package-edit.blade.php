@extends('admin./layouts/default')

@section('title')
Service Request
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Service Request</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                 Dashboard
             </a>
         </li>
         <li>Service Request</li>
         <li class="active">Edit Request</li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Edit  Request
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($package, ['route' => ['admin.serviceRequests.update'], 'method' => 'post', 'files' => true]) !!}

            @foreach ($package->serviceRequests as $request)
                @include('admin.serviceRequests.fields')
            @endforeach

            <!-- Submit Field -->
            <div class="form-group col-sm-12 text-center">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{!! route('admin.packages.show', $package->id) !!}" class="btn btn-default">Back to package details</a>
                @if ($package->order)
                    <a href="{!! route('admin.orders.show', $package->order->id) !!}" class="btn btn-default">Back to order details</a>
                @endif
                <a href="{!! route('admin.packages.index') !!}" class="btn btn-default">Cancel</a>
            </div>

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript">
        $(document).ready(function() {
            var wrapper                 = ".photos_wrap";
            var add_photo_button     = ".add_photo_button";
            var photo                = $( ".photo" ).length;
            console.log(photo);

            $(document).on('click', add_photo_button, function(e){
                e.preventDefault();
                photo = photo + 1;
                var current_photo = photo;
                console.log(current_photo);

                    var input_create = '<div id="photo_'+ current_photo +'" class="photo photo_'+ current_photo +'" style="margin-top:20px">';
                            input_create += ' <table class="table table-bordered" id="table">';
                            input_create += ' <thead><tr>';
                            input_create += ' <th>Source</th>';
                            input_create += ' </tr></thead>';
                            input_create += ' <tbody class="photo_'+ current_photo +'_table"><tr>';
                            input_create += ' <input type="hidden" class="form-control" name="photos['+ current_photo +'][photo][id]" />';
                            input_create += ' <td><input type="file" class="form-control" name="photos['+ current_photo +'][photo][photo]" /></td>';
                            input_create += ' <td><a href="#" class="remove_field btn btn-link" data-photo="'+ current_photo +'">Remove photo</a></td>';
                            input_create += ' </tr></tbody></table>';
                        input_create += '</div>';

                    $(wrapper).append(input_create);

            });

            $(document).on("click",".remove_field", function(e){
                e.preventDefault(); 
                $('.photo_'+$(this).data('photo')).remove(); 
                //question--;
            })

        });
    </script>

@stop
