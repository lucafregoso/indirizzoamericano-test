<div class="form-group">
    <div class="input_fields_wrap">
        <button class="add_photo_button btn btn-primary">Add Photo</button>
        <div class="photos_wrap"></div>
    </div>
</div>

<?php $zphoto = 0; ?>
@if ($request->photos)
@foreach($request->photos as $photo)
    <div id="photo_{{ $zphoto }}" class="photo photo_{{ $zphoto }}">
        <table class="table table-bordered" id="table">
            <thead>
                <tr>
                    <th>Source</th>
                </tr>
            </thead>
            <tbody class="photo_{{ $photo->id }}_table">
                <tr>
                    <input type="hidden" class="form-control" name="photos[{{ $photo->id }}][photo][id]" value="{{ $photo->id }}" />
                    <td><img src="{{ url('/').'/uploads/files/'.\App\Helpers\Helper::getFilePath($photo->filename).$photo->filename.'.'.$photo->extension }}" width="100"></td>
                    <td><a href="#" class="remove_field btn btn-link" data-photo="{{ $zphoto }}">Remove photo</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php $zphoto++; ?>
@endforeach
@endif