<div class="form-group">
    {!! Form::label('extra_address[address]', 'Address:') !!}
    {!! Form::text('extra_address[address]', old('extra_address[address]', $request->extra_address ? $request->extra_address->address : null), ['class' => 's2k-required form-control', 'placeholder'=>'Address' ]) !!}
</div>
<div class="form-group">
    {!! Form::label('extra_address[city]', 'City:') !!}
    {!! Form::text('extra_address[city]', old('extra_address[city]', $request->extra_address ? $request->extra_address->city : null), ['class' => 's2k-required form-control', 'placeholder'=>'City' ]) !!}
</div>
<div class="form-group">
    {!! Form::label('extra_address[zip_code]', 'Zip Code:') !!}
    {!! Form::text('extra_address[zip_code]', old('extra_address[zip_code]', $request->extra_address ? $request->extra_address->zip_code : null), ['class' => 's2k-required form-control', 'placeholder'=>'Zip Code' ]) !!}
</div>
<div class="form-group">
    {!! Form::label('extra_address[state]', 'State:') !!}
    {!! Form::text('extra_address[state]', old('extra_address[state]', $request->extra_address ? $request->extra_address->state : null), ['class' => 's2k-required form-control', 'placeholder'=>'State' ]) !!}
</div>