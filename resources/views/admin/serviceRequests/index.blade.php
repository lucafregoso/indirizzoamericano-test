@extends('admin./layouts/default')

@section('title')
Service Request
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Service Request</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Service Request</li>
        <li class="active">Service Request</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Service Request
                </h4>
            </div>
            <br />
            <div class="panel-body table-responsive">
                 <table class="table table-bordered " id="requests-table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>Reference</th>
                            <th>Services</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
 </div>
</section>
@stop


@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $(function() {
            
            @if (Request::is('admin/serviceRequests/orders'))
                var route = '{!! route('admin.serviceRequests.orders.data') !!}';
                var table = $('#requests-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: route,
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'reference', name: 'reference' },
                        { data: 'services', name: 'services' },
                        { data: 'created_at', name:'created_at'},
                        { data: 'actions', name: 'actions', orderable: false, searchable: false }
                    ]
                });
            @else
                var route = '{!! route('admin.serviceRequests.packages.data') !!}';
                var table = $('#requests-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: route,
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'reference', name: 'reference' },
                        { data: 'services', name: 'services' },
                        { data: 'created_at', name:'created_at'},
                        { data: 'actions', name: 'actions', orderable: false, searchable: false }
                    ]
                });
            @endif

            table.on( 'draw', function () {
                $('.livicon').each(function(){
                    $(this).updateLivicon();
                });
            } );
        });

    </script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
