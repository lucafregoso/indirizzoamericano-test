<!-- Panel block -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <h2>{{ $request->service->name }}</h2>
                </h3>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    {!! Form::hidden("requests[$request->id][status]") !!}

                        <!-- Status Field -->
                        <div class="form-group">
                            {!! Form::label('status', 'Done:') !!}
                            {!! Form::checkbox("requests[$request->id][status]", 1, ($request->status == 1) ? true : false ) !!}
                        </div>

                        @if ($request->service->slug == 'doa')
                            @include('admin.serviceRequests.components.doa')
                        @endif

                        @if ($request->service->slug == 'extra_address')
                            @include('admin.serviceRequests.components.extra_address')
                        @endif

                        <!-- Description Field -->
                        <div class="form-group">
                            {!! Form::label('note', 'Note:') !!}
                            {!! Form::textarea("requests[$request->id][note]", $request->data, ['class' => 'form-control', 'rows' => '5']) !!}
                        </div>

                        @if ($request->service->slug == 'photo')
                            @include('admin.serviceRequests.components.photo')
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Panel block -->



