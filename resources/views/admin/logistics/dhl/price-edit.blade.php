@extends('admin./layouts/default')

@section('title')
DHL Prices edit
@parent
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Services Edit</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                 Dashboard
             </a>
         </li>
         <li>DHL Prices edit</li>
         <li class="active">Edit DHL Prices </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      @include('flash::message')
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Edit  DHL Prices
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'admin.dhl.price']) !!}

            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="filters">
                        <th>Weight (lbs)</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($prices as $price)
                        <tr>
                            <td><input type="text" name="dhl[{{$price->id}}][weight]" value="{{ $price->weight }}"></td>
                            <td><input type="text" name="dhl[{{$price->id}}][price]" value="{{ $price->price }}"></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <!-- Submit Field -->
            <div class="form-group col-sm-12 text-center">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            </div>

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop