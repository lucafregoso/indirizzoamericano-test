@extends('admin./layouts/default')

@section('title')
Packages
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>Packages</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Packages</li>
        <li class="active">Create Packages </li>
    </ol>
</section>
<section class="content paddingleft_right15">
<div class="row">
 <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Create New  Package
            </h4></div>
        <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'admin.packages.store', 'files' => true]) !!}

            <!-- Barcode -->
            <div class="form-group col-sm-12">
                {!! Form::label('barcode', 'Barcode:') !!}
                {!! Form::text('barcode', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Description Field -->
            <div class="form-group col-sm-12 col-lg-12">
                {!! Form::label('description', 'Description:') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '5']) !!}
            </div>

            <!-- Reseller -->
            <div class="form-group col-sm-12">
                {!! Form::label('reseller', 'Reseller:') !!}
                {!! Form::text('reseller', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Broken Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('broken', 'Broken:') !!}
                {!! Form::checkbox('broken', 1) !!}
            </div>

            <!-- Problem Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('problem', 'Problem:') !!}
                {!! Form::checkbox('problem', 1) !!}
            </div>

            <div class="form-group col-sm-12">
                {!! Form::label('user_id', 'Users:') !!}
                <select class="form-control select2" id="select-user" name="user_id">
                    @foreach($users as $user)
                        <option value="{{$user->id}}" {{ (isset($package) && $package->user_id == $user->id) ? 'selected' : '' }}>{{$user->uuic}} - {{$user->first_name}} {{$user->last_name}} - {{$user->email}}</option>
                    @endforeach
                </select>
            </div>

            <h2>Services</h2>
            <div class="form-group col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($services as $service)
                            <tr>
                                <td>{{  $service->id }}</td>
                                <td>{{  $service->name }}</td>
                                <td>{{  $service->description }}</td>
                                <td>
                                    {!! Form::checkbox('services[]', $service->id, null, ['class' => '']) !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Submit Field -->
            <div class="form-group col-sm-12 text-center">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{!! route('admin.packages.index') !!}" class="btn btn-default">Cancel</a>
            </div>

        {!! Form::close() !!}
    </div>
  </div>
 </div>
</section>
 @stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript">
        $("#select-user").select2({
            theme:"bootstrap",
            placeholder:"Select user..."
        });
    </script>
@stop