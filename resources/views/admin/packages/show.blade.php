@extends('admin./layouts/default')

@section('title')
Packages
@parent
@stop

@section('content')
<section class="content-header">
    <h1>Packages Details</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Packages</li>
        <li class="active">Package Details</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="panel panel-primary">
        <div class="panel-heading clearfix">
            <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Package details
            </h4>
        </div>
        <!-- Panel block -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Package
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-4">
                            <strong>UID:</strong> {{ $package->uid }} <br>
                            <strong>Barcode:</strong> {{ $package->barcode }} <br>
                            <strong>Broken:</strong> {!! $package->broken == 1 ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>' !!} <br>
                            <strong>Problem:</strong> {!! $package->problem == 1 ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>' !!}  <br>
                            <strong>Created At:</strong> {{ $package->created_at->diffForHumans() }} <br>
                            <strong>Tickets:</strong><br>
                        </div>
                        <div class="col-md-4">
                            <strong>Descrption:</strong>  <br> {{ $package->descrption }}
                        </div>
                        <div class="col-md-4">
                            <strong>User Code:</strong> {{ $package->user->uuic }} <br>
                            <strong>First Name:</strong> {{ $package->user->first_name }} <br>
                            <strong>Last Name:</strong> {{ $package->user->last_name }} <br>
                            <strong>Address:</strong> {{ $package->user->address }} <br>
                            <strong>City:</strong> {{ $package->user->city }} <br>
                            <strong>Zip Code:</strong> {{ $package->user->zip_code }} <br>
                            <strong>State:</strong> {{ $package->user->state }} <br>
                            <a href="{{ route('admin.users.show', $package->user->id) }}" target="_blank">Other Details</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Panel block -->

        <!-- Panel block -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Service Packages
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr class="filters">
                                        <th>Service</th>
                                        <th>Done</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package->serviceRequests as $serviceRequest)
                                        <tr>
                                            <td>{{ $serviceRequest->service->name }}</td>
                                            <td>
                                                @if ($serviceRequest->status == 1)
                                                    <p class="text-success">Yes</p>
                                                @else
                                                    <a href="{{ route('admin.serviceRequests.packages.edit', $package->id) }}" class="btn btn-warning">Do it</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Panel block -->
    </div>
</section>
@stop
