<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $package->id !!}</p>
    <hr>
</div>

<!-- Barcode Field -->
<div class="form-group">
    {!! Form::label('barcode', 'Barcode:') !!}
    <p>{!! $package->barcode !!}</p>
    <hr>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $package->description !!}</p>
    <hr>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $package->value !!}</p>
    <hr>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $package->price !!}</p>
    <hr>
</div>

<!-- Broken Field -->
<div class="form-group">
    {!! Form::label('broken', 'Broken:') !!}
    <p>{!! $package->broken !!}</p>
    <hr>
</div>

<!-- User Field -->
<div class="form-group">
    {!! Form::label('user', 'User:') !!}
    <p>{!! isset($package->user->email) ? $package->user->email : '' !!}</p>
    <hr>
</div>

