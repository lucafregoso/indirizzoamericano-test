@extends('admin./layouts/default')

@section('title')
Packages
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Packages</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Packages</li>
        <li class="active">Packages List</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Packages List
                </h4>
                <div class="pull-right">
                    <a href="{{ route('admin.packages.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body table-responsive">
                 @include('admin.packages.table')
            </div>
        </div>
 </div>
</section>
@stop


@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $(function() {
            var route = '{!! route('admin.packages.data') !!}';
            @if (Request::is('admin/packages/service/photo'))
                route = '{!! route('admin.packages.photo.data') !!}';
            @endif
            var table = $('#packages-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: route,
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'uid', name: 'uid' },
                    { data: 'barcode', name: 'barcode' },
                    { data: 'description', name: 'description', "visible": false},
                    { data: 'reseller', name: 'reseller' },
                    { data: 'broken', name: 'broken' },
                    { data: 'services', name: 'services', "defaultContent": "", "render": "[, ]", searchable: false },
                    { data: 'user.first_name', name: 'user.first_name', "visible": false},
                    { data: 'user.last_name', name: 'user.last_name', "visible": false},
                    { data: 'user.uuic', name: 'user.uuic', "visible": false},
                    { data: 'user.email', name: 'user.email', "visible": false},
                    { data: 'user_data', name: 'user_data', searchable: false },
                    { data: 'created_at', name:'created_at'},
                    { data: 'actions', name: 'actions', orderable: false, searchable: false }
                ]
            });
            table.on( 'draw', function () {
                $('.livicon').each(function(){
                    $(this).updateLivicon();
                });
            } );
        });

    </script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
