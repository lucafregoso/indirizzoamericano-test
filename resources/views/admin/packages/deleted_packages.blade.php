@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Deleted packages
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
    <!-- end page css -->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <h1>Deleted packages</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                            Dashboard
                        </a>
                    </li>
                    <li><a href="#"> Packages</a></li>
                    <li class="active">Deleted packages</li>
                </ol>
            </section>
            <!-- Main content -->
         <section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <i class="livicon" data-name="responsive-menu" data-size="18" data-c="#ffffff" data-hc="#ffffff"></i>
                    Deleted Packages List
                </h4>
            </div>
            <div class="panel-body">
                <table class="table table-responsive" id="packages-table">
                    <thead>
                     <tr>
                        <th>ID</th>
                        <th>Barcode</th>
                        <th>Description</th>
                        <th colspan="3">Action</th>
                     </tr>
                    </thead>
                    <tbody>
                    @foreach($packages as $package)
                        <tr>
                            <td>{!! $package->id !!}</td>
                            <td>{!! $package->barcode !!}</td>
                            <td>{!! $package->description !!}</td>
                            <td>
                                 <a href="{{ route('admin.restore/package', $package->id) }}" class="btn default"><i class="fa fa-rotate-left"></i> Restore</a>
                                 <a href="{{ route('admin.packages.confirm-destroy', $package->id) }}" data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section("footer_scripts")
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
      </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#table').DataTable();

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
    </script>
@stop
