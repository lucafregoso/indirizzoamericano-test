@extends('admin./layouts/default')

@section('title')
Notifications
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Notifications</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Notifications</li>
        <li class="active">Create Notification</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Create new notification
                    </h4>
                </div>
                <div class="panel-body">
                     @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open(['url' => 'admin/notifications','files'=>true]) !!}

                    <div class="form-group col-sm-12">
                        {!! Form::label('content', 'Content: ') !!}
                        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- User Id Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('user_id', 'Users:') !!}
                        <select class="form-control select2" id="select-user" name="user_id">
                            @foreach($users as $user)
                                <option value="{{$user->id}}" {{ (isset($package) && $package->user_id == $user->id) ? 'selected' : '' }}>{{$user->uuic}} - {{$user->first_name}} {{$user->last_name}} - {{$user->email}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <a class="btn btn-danger" href="{{ route('admin.notifications.index') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript">
        $("#select-user").select2({
            theme:"bootstrap",
            placeholder:"Select user..."
        });
    </script>
@stop