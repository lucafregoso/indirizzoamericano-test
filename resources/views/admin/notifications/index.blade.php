@extends('admin./layouts/default')

@section('title')
Notifications
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Notifications</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Notifications</li>
        <li class="active">Notifications List</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Notifications List
                </h4>
            </div>
            <br />

            <div class="panel-body table-responsive">
                <table class="table table-bordered " id="notifications">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>Content</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>UUIC</th>
                            <th>User</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
 </div>
</section>
@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $(document).ready(function() {
            $('#notifications').dataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": "{{ URL::route('admin.notifications.data') }}",
                "columns": [
                    {data: 'id', name: 'notifications.id'},
                    {data: 'content', name: 'notifications.content'},
                    {data: 'user.first_name', name: 'user.first_name', "visible": false},
                    {data: 'user.last_name', name: 'user.last_name', "visible": false},
                    {data: 'user.uuic', name: 'user.uuic', "visible": false},
                    {data: 'user.email', name: 'user.email', "visible": false},
                    {data: 'user_data', name: 'user_data'},
                    {data: 'status', name: 'notifications.status'},
                    {data: 'created_at', name: 'notifications.created_at'},
                    {data: 'actions', name: 'actions'},
                ]
            });
        });
    </script>

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
