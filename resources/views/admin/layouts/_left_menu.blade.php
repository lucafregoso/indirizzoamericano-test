<ul id="menu" class="page-sidebar-menu">
    <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
        <a href="{{ route('admin.dashboard') }}">
            <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
    <li {!! (Request::is('admin/packages') || Request::is('admin/serviceRequests/packages') || Request::is('admin/packages/create') || Request::is('admin/packages/*') || Request::is('admin/deleted_packages') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="responsive-menu" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C" data-loop="true"></i>
            <span class="title">Packages {!! (isset($_count_packages_need_service) && $_count_packages_need_service != 0) ? '<span class="badge badge-danger">'.$_count_packages_need_service.'</span>' : '' !!}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/packages') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/packages') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Packages
                </a>
            </li>
            <li {!! (Request::is('admin/packages/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/packages/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New Package
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_packages') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_packages') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Deleted Packages
                </a>
            </li>
            <li {!! (Request::is('admin/serviceRequests/packages') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/serviceRequests/packages') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Service Requests {!! (isset($_count_packages_need_service) && $_count_packages_need_service != 0) ? '<span class="badge badge-danger">'.$_count_packages_need_service.'</span>' : '' !!}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/orders') || Request::is('admin/serviceRequests/orders') || Request::is('admin/orders/create') || Request::is('admin/orders/*') || Request::is('admin/deleted_orders') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="responsive-menu" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
               data-loop="true"></i>
            <span class="title">Orders {!! (isset($_badge_orders) && $_badge_orders != 0) ? '<span class="badge badge-danger">'.$_badge_orders.'</span>' : '' !!}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/orders') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/orders') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Orders
                </a>
            </li>
            <li {!! (Request::is('admin/orders/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/orders/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New Order
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_orders') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_orders') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Deleted Orders
                </a>
            </li>
            <li {!! (Request::is('admin/serviceRequests/orders') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/serviceRequests/orders') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Service Requests {!! (isset($_count_orders_need_service) && $_count_orders_need_service != 0) ? '<span class="badge badge-danger">'.$_count_orders_need_service.'</span>' : '' !!}
                </a>
            </li>
            <li {!! (Request::is('admin/orders/lists/new') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/orders/lists/new') }}">
                    <i class="fa fa-angle-double-right"></i>
                    New Orders List {!! (isset($_count_orders_new) && $_count_orders_new != 0) ? '<span class="badge badge-danger">'.$_count_orders_new.'</span>' : '' !!}
                </a>
            </li>
            <li {!! (Request::is('admin/orders/lists/paid') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/orders/lists/paid') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Paid Orders List {!! (isset($_count_orders_paid) && $_count_orders_paid != 0) ? '<span class="badge badge-danger">'.$_count_orders_paid.'</span>' : '' !!}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/personalShoppers') || Request::is('admin/personalShoppers/create') || Request::is('admin/personalShoppers/*') || Request::is('admin/deleted_personalShoppers') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="responsive-menu" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
               data-loop="true"></i>
            <span class="title">Personal Shopper</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/personalShoppers') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/personalShoppers') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Orders
                </a>
            </li>
            <li {!! (Request::is('admin/personalShoppers/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/personalShoppers/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New Order
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_personalShoppers') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_personalShoppers') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Deleted Orders
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/conversations') || Request::is('admin/conversations/*') || Request::is('admin/attachments/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="list-ul" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
            <span class="title">Conversations {!! (isset($_count_conversations_new) && $_count_conversations_new != 0) ? '<span class="badge badge-danger">'.$_count_conversations_new.'</span>' : '' !!}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Route::is('mercury::admin.conversations.index') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ route('mercury::admin.conversations.index') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Conversations
                </a>
            </li>
            <li {!! (Route::is('mercury::admin.conversations.create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ route('mercury::admin.conversations.create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New Conversation
                </a>
            </li>
            <li {!! (Request::is('admin/conversations/lists/new') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ route('mercury::admin.conversations.lists.new') }}">
                    <i class="fa fa-angle-double-right"></i>
                    New Conversations {!! (isset($_count_conversations_new) && $_count_conversations_new != 0) ? '<span class="badge badge-danger">'.$_count_conversations_new.'</span>' : '' !!}
                </a>
            </li>
            {{-- <li {!! (Route::is('mercury::admin.attachments.index') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ route('mercury::admin.attachments.index') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Attachments
                </a>
            </li> --}}
        </ul>
    </li>
    <li {!! (Request::is('admin/notifications') || Request::is('admin/notifications/create') || Request::is('admin/notifications/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="list-ul" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
            <span class="title">Notifications</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/notifications') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/notifications') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Notifications
                </a>
            </li>
            <li {!! (Request::is('admin/notifications/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/notifications/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Send Notification
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Users</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Users
                </a>
            </li>
            <li {!! (Request::is('admin/users/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New User
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Deleted Users
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/newsletters') ? 'class="active" id="active"' : '') !!}>
        <a href="{{ URL::route('admin.newsletters') }}">
            <i class="livicon" data-name="mail" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C" data-loop="true"></i>
            Newsletter
        </a>
    </li>
    <li {!! (Request::is('admin/settings') 
            || Request::is('admin/settings/warehouses')
            || Request::is('admin/settings/suggestions')
            || Request::is('admin/settings/services')
    ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="gear" data-size="18" data-c="#999" data-hc="#999"
            data-loop="true"></i>
            <span class="title">Settings</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/settings/warehouses') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/settings/warehouses') }}">
                    <i class="livicon" data-name="sitemap" data-size="18" data-c="#999" data-hc="#999"
                    data-loop="true"></i>
                    <span class="title">Warehouses</span>
                </a>
            </li>
            <li {{ (Request::is('admin/settings/suggestions') ? 'class="active" id="active"' : '') }}>
                <a href="{{ URL::to('admin/settings/suggestions') }}">
                    <i class="livicon" data-name="circle" data-size="18" data-c="#999" data-hc="#999"
                    data-loop="true"></i>
                    <span class="title">Suggestions</span>
                </a>
            </li>
            <li {{ (Request::is('admin/settings/services') ? 'class="active" id="active"' : '') }}>
                <a href="{{ URL::to('admin/settings/services') }}">
                    <i class="livicon" data-name="responsive-menu" data-size="18" data-c="#999" data-hc="#999"
                    data-loop="true"></i>
                    <span class="title">Services</span>
                </a>
            </li>
            <li {{ (Request::is('admin/settings/dhl/price') ? 'class="active" id="active"' : '') }}>
                <a href="{{ URL::to('admin/settings/dhl/price') }}">
                    <i class="livicon" data-name="responsive-menu" data-size="18" data-c="#999" data-hc="#999"
                    data-loop="true"></i>
                    <span class="title">DHL Price</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
