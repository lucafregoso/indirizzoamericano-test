@extends('admin./layouts/default')

@section('title')
Payment Orders
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Payment Order</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                 Dashboard
             </a>
         </li>
         <li>Payment Orders</li>
         <li class="active">Payment Order</li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
        <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Payment Order
                    </h4></div>
                <br />
            {!! Form::open(['route' => ['admin.payments.orders.store', $order->id]]) !!}
                <!-- Spedition -->
                <div class="row">

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Spedition
                                <button id="new-spedition" class="btn btn-primary spedition-check">Add New Spedition</button>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div id="spedition-duplicater" class="block-spedition-duplicater" class="col-lg-12 col-md-12 col-sm-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr class="filters">
                                            <th>Zip Code</th>
                                            <th>Width (inch)</th>
                                            <th>Height (inch)</th>
                                            <th>Depth (inch)</th>
                                            <th>Weight (lbs)</th>
                                            <th>Action</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="spedition-zip_code" placeholder=""></td>
                                            <td><input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="spedition-width" placeholder=""></td>
                                            <td><input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="spedition-height" placeholder=""></td>
                                            <td><input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="spedition-depth" placeholder=""></td>
                                            <td><input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="spedition-weight" placeholder=""></td>
                                            <td>
                                                <button class="btn btn-primary spedition-check">Check</button>
                                                <button class="btn btn-warning remove-spedition-duplicater" style="display:none">Remove</button>
                                            </td>
                                            <td class="col-md-2"><input type="text" class="form-control price spedition-price" id="spedition-price" value="0" readonly></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Total spedition -->
                        <div class="row">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-10"></div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        Total Speditions: <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="total-spedition" value="" name="spedition" readonly>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /row -->
                        <!-- /Total spedition -->
                    </div><!-- /panel -->
                </div><!-- /row -->
                <!-- /Spedition -->

                <!-- Service packages -->
                <div class="row">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Packages
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr class="filters">
                                            <th>Package</th>
                                            <th>Service</th>
                                            <th>Done</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $service = 0;
                                        @endphp
                                        @foreach ($order->packages as $package)
                                            <tr>
                                                <td rowspan="{{ $package->serviceRequests->count() + 1}}"><a href="{{ route('admin.packages.show', $package->id) }}" target="_blank">{{ $package->uid }}</a></td>
                                            </tr>
                                                @foreach ($package->serviceRequests as $serviceRequest)
                                                    <tr>
                                                        <td>{{ $serviceRequest->service->name }}</td>
                                                        <td>{!! $serviceRequest->status == 1 ? '<p class="text-success">Yes</p>' : '<p class="text-danger">No</p>' !!}</td>
                                                        <td class="col-md-2"><input type="text" class="form-control price" id="inlineFormInput" value="{{ $serviceRequest->service->price }}" readonly></td>
                                                    </tr>
                                                    @php
                                                        $service = $service + $serviceRequest->service->price;
                                                    @endphp
                                                @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /panel -->
                </div><!-- /row -->
                <!-- /Service packages -->

                <!-- Services order -->
                <div class="row">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Services Order
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr class="filters">
                                            <th>Order</th>
                                            <th>Service</th>
                                            <th>Done</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="{{ $order->serviceRequests->count() + 1}}"><a href="{{ route('admin.orders.show', $package->id) }}" target="_blank">{{ $order->uid }}</a></td>
                                        </tr>
                                            @foreach ($order->serviceRequests as $serviceRequest)
                                                <tr>
                                                    <td>{{ $serviceRequest->service->name }}</td>
                                                    <td>{!! $serviceRequest->status == 1 ? '<p class="text-success">Yes</p>' : '<p class="text-danger">No</p>' !!}</td>
                                                    <td class="col-md-2"><input type="text" class="form-control price" id="inlineFormInput" value="{{ $serviceRequest->service->price }}" readonly></td>
                                                </tr>
                                                @php
                                                    $service = $service + $serviceRequest->service->price;
                                                @endphp
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /panel -->
                </div><!-- /row -->
                <!-- /Services order -->

                <!-- Partial -->
                <div class="row">
                    <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-10 col-md-10 col-sm-10"></div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    Partial: <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="partial" value="" name="partial" readonly>
                                    Additional: <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="additional" name="additional" value="0">
                                </div>
                            </div>
                    </div>
                </div><!-- /row -->
                <!-- /partial -->
                <hr>
                <!-- Total -->
                <div class="row">
                    <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-10 col-md-10 col-sm-10"></div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    Total: <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="total" value="" name="total" readonly>
                                </div>
                            </div>
                    </div>
                </div><!-- /row -->
                <!-- /total -->

                <!-- Submit Field -->
                <div class="row">
                    <div class="panel-body">
                        <div class="form-group col-sm-12 text-center">
                            <input type="hidden" value="{{ $service }}" name="services" readonly>
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('admin.orders.show', $order->id) !!}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
   </section>
 @stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript">

    $( document ).ready(function() {
        //Partial
        partial = setPartial();

        var count_duplicate = 0;
        $("#new-spedition").on("click", function(e) {
            e.preventDefault();
            var original = document.getElementById('spedition-duplicater');
            var clone = original.cloneNode(true);
            clone.id = "spedition-duplicater-" + ++count_duplicate;

            original.parentNode.appendChild(clone);

            $('#'+clone.id).find('.form-control').each(function(index, input) {
                $(input).val('');
            });

            $('#'+clone.id).find('.remove-spedition-duplicater').show();
        });

        $(document).on("click",".remove-spedition-duplicater", function(e){
            e.preventDefault(); 
            count_duplicate--;
            $(this).parents('.block-spedition-duplicater').remove();
            setPartial();
            setTotalSpedition();
        });

        $("#additional").on("change paste keyup", function() {
            if (parseFloat($('#additional').val()) > 0) {
                $('#total').val(partial + parseFloat($('#additional').val()));
            } else {
                $('#total').val(partial);
            }
        });

        $(document).on("click",".spedition-check", function(e){
            e.preventDefault(); 
            var parent_block = $(this).parents('.block-spedition-duplicater');
            $.ajax({
                method: "POST",
                url: "{{ route('spedition.price') }}",
                data: { 
                        zip_code: $('#spedition-zip_code', parent_block).val(),
                        width: $('#spedition-width', parent_block).val(),
                        height: $('#spedition-height', parent_block).val(),
                        depth: $('#spedition-depth', parent_block).val(),
                        weight: $('#spedition-weight', parent_block).val(),
                },
                success: function (data) {
                    //console.log(data.price);
                    $('#spedition-price', parent_block).val(data.price);
                    partial = setPartial();

                    setTotalSpedition();
                }
            })
        });

        function setTotalSpedition() {
            //Set total speditions
            var total_spedition = 0.0;
            $('.spedition-price').each(function(){
                if (parseFloat(this.value) > 0) {
                    total_spedition += parseFloat(this.value);
                }
            });
            $('#total-spedition').val(total_spedition);
        }

        function setPartial() {
            var partial = 0.0;
            $('.price').each(function(){
                if (parseFloat(this.value) > 0) {
                    partial += parseFloat(this.value);
                }
            });
            //Set Partial
            $('#partial').val(partial);

            //Set Total
            if (parseFloat($('#additional').val()) > 0) {
                $('#total').val(partial + parseFloat($('#additional').val()));
            } else {
                $('#total').val(partial);
            }

            return partial;
        }
    });

</script>
@stop