<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $service->id !!}</p>
    <hr>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $service->name !!}</p>
    <hr>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $service->description !!}</p>
    <hr>
</div>

<!-- Image Id Field -->
<div class="form-group">
    {!! Form::label('image_id', 'Image Id:') !!}
    <p>{!! $service->image_id !!}</p>
    <hr>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $service->price !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>@if( $service->status  =='1') true @else false @endif</p>
    <hr>
</div>

<!-- Locale Field -->
<div class="form-group">
    {!! Form::label('locale', 'Locale:') !!}
    <p>{!! $service->locale !!}</p>
    <hr>
</div>

