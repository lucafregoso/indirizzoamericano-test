<table class="table table-responsive" id="services-table">
    <thead>
     <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Image</th>
        <th>Price</th>
        <th>Status</th>
        <th>Locale</th>
        <th colspan="3">Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($services as $service)
        <tr>
            <td>{!! $service->name !!}</td>
            <td>{!! $service->description !!}</td>
            <td>{!! isset($service->image->originalname) ? $service->image->originalname : '' !!}</td>
            <td>{!! $service->price !!}</td>
            <td>@if($service->status =='1') true @else false @endif</td>
            <td>{!! $service->locale !!}</td>
            <td>
                 <a href="{{ route('admin.services.show', $service->id) }}" class="btn default"><i class="fa fa-info"></i> View</a>
                 <a href="{{ route('admin.services.edit', $service->id) }}" class="btn default"><i class="fa fa-edit"></i> Edit</a>
                 <a href="{{ route('admin.services.confirm-delete', $service->id) }}" data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop