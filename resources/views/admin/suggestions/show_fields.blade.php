<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $suggestion->id !!}</p>
    <hr>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $suggestion->name !!}</p>
    <hr>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $suggestion->description !!}</p>
    <hr>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $suggestion->image->originalname !!}</p>
    <hr>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $suggestion->url !!}</p>
    <hr>
</div>

<!-- Blank Field -->
<div class="form-group">
    {!! Form::label('blank', 'Blank:') !!}
    <p>@if( $suggestion->blank  =='1') true @else false @endif</p>
    <hr>
</div>

