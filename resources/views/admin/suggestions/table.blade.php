<table class="table table-responsive" id="suggestions-table">
    <thead>
     <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Image</th>
        <th>Url</th>
        <th>Blank</th>
        <th colspan="3">Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($suggestions as $suggestion)
        <tr>
            <td>{!! $suggestion->name !!}</td>
            <td>{!! $suggestion->description !!}</td>
            <td>{!! isset($suggestion->image->originalname) ? $suggestion->image->originalname : '' !!}</td>
            <td>{!! $suggestion->url !!}</td>
            <td>@if($suggestion->blank =='1') true @else false @endif</td>
            <td>
                 <a href="{{ route('admin.suggestions.show', $suggestion->id) }}" class="btn default"><i class="fa fa-info"></i> View</a>
                 <a href="{{ route('admin.suggestions.edit', $suggestion->id) }}" class="btn default"><i class="fa fa-edit"></i> Edit</a>
                 <a href="{{ route('admin.suggestions.confirm-delete', $suggestion->id) }}" data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop