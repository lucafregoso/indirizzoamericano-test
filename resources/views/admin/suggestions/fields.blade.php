<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div> -->

<div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}

        <div class="fileinput fileinput-new" data-provides="fileinput">

            <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">

                @if(isset($suggestion->image))
                    <img src="{{ url('/').'/uploads/files/'.App\Helpers\Helper::getFilePath($suggestion->image->filename).$suggestion->image->filename.'.'.$suggestion->image->extension }}">
                @else
                    <img src="http://placehold.it/200x200" alt="profile pic">
                @endif
            </div>

            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
            <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input id="pic" name="image" type="file" class="form-control"/>
                </span>
                <a href="#" class="btn btn-danger fileinput-exists"
                   data-dismiss="fileinput">Remove</a>
            </div>
        </div>

</div>

<!-- Url Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Blank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blank', 'Blank:') !!}
    <label class="checkbox-inline">

        {!! Form::checkbox('blank', '1') !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.suggestions.index') !!}" class="btn btn-default">Cancel</a>
</div>
