<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $personalShopper->id !!}</p>
    <hr>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('reference', 'Reference:') !!}
    <p>{!! $personalShopper->reference !!}</p>
    <hr>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $personalShopper->note !!}</p>
    <hr>
</div>

<!-- User Field -->
<div class="form-group">
    {!! Form::label('user', 'User:') !!}
    <p><a href="{{ route('admin.users.show', $personalShopper->user->id) }} " target="_blank">{{ (isset($personalShopper->user->uuic) ? $personalShopper->user->uuic : '').' - '.$personalShopper->user->first_name.' '.$personalShopper->user->last_name.' - '.$personalShopper->user->email }}</a></p>
    
    <hr>
</div>

<h2>Products</h2>
@if ($personalShopper->products)

    <table class="table table-bordered">
    <thead>
     <tr>
        <th>ID</th>
        <th>Quantity</th>
        <th>Article Code</th>
        <th>Size</th>
        <th>Color</th>
        <th>Note</th>
     </tr>
    </thead>
        @foreach ($personalShopper->products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->quantity }}</td>
                <td>{{ $product->article_code }}</td>
                <td>{{ $product->size }}</td>
                <td>{{ $product->color }}</td>
                <td>{{ $product->note }}</td>
            </tr>
        @endforeach
    <tbody>
    </tbody>
</table>

@endif

