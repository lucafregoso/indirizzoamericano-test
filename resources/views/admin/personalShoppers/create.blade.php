@extends('admin./layouts/default')

@section('title')
Personal Shopper
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>Personal Shopper</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Personal Shopper</li>
        <li class="active">Create Order </li>
    </ol>
</section>
<section class="content paddingleft_right15">
<div class="row">
 <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Create New Order
            </h4></div>
        <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'admin.personalShoppers.store']) !!}

            <!-- Reference Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('reference', 'Reference:') !!}
                {!! Form::text('reference', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Note Field -->
            <div class="form-group col-sm-12 col-lg-12">
                {!! Form::label('note', 'Note:') !!}
                {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => '5']) !!}
            </div>

            <!-- User Id Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('user_id', 'Users:') !!}
                {!! Form::select('user_id', $users->toArray(), null, ['name'=>'user_id', 'class' => 'form-control select2', 'id' => 'select-user']) !!}
            </div>

            <div class="form-group col-sm-12">
                <div class="input_fields_wrap">
                    <h2>Products <button class="add_product_button btn btn-primary">Add Product</button> </h2>
                    <div class="products_wrap"></div>
                </div>
            </div>

            <!-- Submit Field -->
            <div class="form-group col-sm-12 text-center">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{!! route('admin.personalShoppers.index') !!}" class="btn btn-default">Cancel</a>
            </div>

        {!! Form::close() !!}
    </div>
  </div>
 </div>
</section>
 @stop

 {{-- page level scripts --}}
@section('footer_scripts')
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript">
        $("#select-user").select2({
            theme:"bootstrap",
            placeholder:"Select user..."
        });
    </script>

<script type="text/javascript">
    $(document).ready(function() {
        var max_products          = 100;
        var max_answers            = 100;
        var wrapper                = ".products_wrap";
        var add_product_button    = ".add_product_button";

        var product = 1;
        $(document).on('click', add_product_button, function(e){ 
            e.preventDefault();
            if(product < max_products){
                var current_product = product++;

                var input_create = '<div id="product_'+ current_product +'" class="product product_'+ current_product +' col-sm-12" style="margin-top:20px">';
                        input_create += ' <table class="table table-bordered" id="table">';
                        input_create += ' <thead><tr>';
                        input_create += ' <th>Quantity</th>';
                        input_create += ' <th>Article Code</th>';
                        input_create += ' <th>Size</th>';
                        input_create += ' <th>Color</th>';
                        input_create += ' <th>Note</th>';
                        input_create += ' </tr></thead>';
                        input_create += ' <tbody class="product_'+ current_product +'_table"><tr>';
                        input_create += ' <td><input type="text" class="form-control" name="products['+ current_product +'][quantity]" /></td>';
                        input_create += ' <td><input type="text" class="form-control" name="products['+ current_product +'][article_code]" /></td>';
                        input_create += ' <td><input type="text" class="form-control" name="products['+ current_product +'][size]" /></td>';
                        input_create += ' <td><input type="text" class="form-control" name="products['+ current_product +'][color]" /></td>';
                        input_create += ' <td><textarea class="form-control" name="products['+ current_product +'][note]" cols="50" rows="10"></textarea></td>';
                        input_create += ' <td><a href="#" class="remove_field btn btn-link" data-product="'+ current_product +'">Remove product</a></td>';
                        input_create += ' </tr></tbody></table>';
                    input_create += '</div>';

                $(wrapper).append(input_create);
            }
        });

        $(wrapper).on("click",".remove_field", function(e){
            e.preventDefault(); 
            $('.product_'+$(this).data('product')).remove(); 
            product--;
        });

    });
</script>
@stop
