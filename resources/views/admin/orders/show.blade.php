@extends('admin./layouts/default')

@section('title')
Orders
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/pages/user_profile.css') }}" rel="stylesheet"/>
@stop

@section('content')
<section class="content-header">
    <h1>Orders Details</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Orders</li>
        <li class="active">Orders Details</li>
    </ol>
</section>

       <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav  nav-tabs ">
                    <li class="active">
                        <a href="#tab1" data-toggle="tab">
                            <i class="livicon" data-name="list" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i>
                            Order Details</a>
                    </li>
                    <li>
                        <a href="#tab2" data-toggle="tab">
                            <i class="livicon" data-name="money" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i>
                            Order Payments</a>
                    </li>
                </ul>
                <div  class="tab-content mar-top">
                    <!-- Tab 1-->
                    <div id="tab1" class="tab-pane fade active in">
                        <!-- Panel block -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Order
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-4">
                                            <strong>UID:</strong> {{ $order->uid }} <br>
                                            <strong>Tracking:</strong> {{ $order->tracking }} <br>
                                            <strong>Created At:</strong> {{ $order->created_at->diffForHumans() }} <br>
                                            <strong>Status :</strong> {!! App\Helpers\Helper::getOrderStatus($order->status) !!} <br>
                                            <strong>Tickets :</strong><br>
                                        </div>
                                        <div class="col-md-4">
                                            <strong>User Code:</strong> {{ $order->user->uuic }} <br>
                                            <strong>First Name:</strong> {{ $order->user->first_name }} <br>
                                            <strong>Last Name:</strong> {{ $order->user->last_name }} <br>
                                            <strong>Address:</strong> {{ $order->user->address }} <br>
                                            <strong>City:</strong> {{ $order->user->city }} <br>
                                            <strong>Zip Code:</strong> {{ $order->user->zip_code }} <br>
                                            <strong>State:</strong> {{ $order->user->state }} <br>
                                            <a href="{{ route('admin.users.show', $order->user->id) }}" target="_blank">Other Details</a>
                                        </div>
                                        <div class="col-md-4">
                                            <strong>Total Payment:</strong>
                                            @if ($order->payments->contains('status', 1))
                                               {{ $order->payments()->where('payments.status', 1)->first()->total }}
                                            @endif
                                            <br>
                                            <strong>Paid:</strong>
                                            @if ($order->payments->contains('status', 1))
                                               {!! $order->payments()->where('payments.status', 1)->first()->paid == 1 ?  '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>' !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Panel block -->

                        <!-- Panel block -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Packages
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr class="filters">
                                                        <th>ID</th>
                                                        <th>UID</th>
                                                        <th>Barcode</th>
                                                        <th>Description</th>
                                                        <th>Reseller</th>
                                                        <th>Broken</th>
                                                        <th>Problem</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($order->packages as $package)
                                                        <tr>
                                                            <td>{{ $package->id }}</td>
                                                            <td>{{ $package->uid }}</td>
                                                            <td>{{ $package->barcode }}</td>
                                                            <td>{{ $package->description }}</td>
                                                            <td>{{ $package->reseller }}</td>
                                                            <td>{{ $package->broken }}</td>
                                                            <td>{{ $package->problem }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Panel block -->

                        <!-- Panel block -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Service Order
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr class="filters">
                                                        <th>Service</th>
                                                        <th>Done</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($order->serviceRequests as $serviceRequest)
                                                        <tr>
                                                            <td>{{ $serviceRequest->service->name }}</td>
                                                            <td>
                                                                @if ($serviceRequest->status == 1)
                                                                    <p class="text-success">Yes</p>
                                                                @else
                                                                    <a href="{{ route('admin.serviceRequests.orders.edit', $order->id) }}" class="btn btn-warning">Do it</a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Service Packages
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr class="filters">
                                                        <th>Package</th>
                                                        <th>Service</th>
                                                        <th>Done</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($order->packages as $package)
                                                        <tr>
                                                            <td rowspan="{{ $package->serviceRequests->count() + 1}}">{{ $package->barcode }}</td>
                                                        </tr>
                                                            @foreach ($package->serviceRequests as $serviceRequest)
                                                                <tr>
                                                                    <td>{{ $serviceRequest->service->name }}</td>
                                                                    <td>
                                                                        @if ($serviceRequest->status == 1)
                                                                            <p class="text-success">Yes</p>
                                                                        @else
                                                                            <a href="{{ route('admin.serviceRequests.packages.edit', $package->id) }}" class="btn btn-warning">Do it</a>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Panel block -->
                    </div>
                    <!-- /Tab 1-->

                    <!-- Tab 2-->
                    <div id="tab2" class="tab-pane fade">
                        <!-- Panel block -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Payments
                                            <div class="pull-right">
                                                <a href="{{ route('admin.payments.orders.index', $order->id) }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                                            </div>
                                        </h3>
                                        <br>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr class="filters">
                                                        <th>ID</th>
                                                        <th>Spedition</th>
                                                        <th>Services</th>
                                                        <th>Partial</th>
                                                        <th>Additional</th>
                                                        <th>Total</th>
                                                        <th>Sent</th>
                                                        <th>Paid</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $alreadySent = false;
                                                        if($order->payments->contains('status', 1)) {
                                                            $alreadySent = true;
                                                        }
                                                    @endphp
                                                    @foreach ($order->payments as $payment)
                                                        <tr>
                                                            <td>{{ $payment->id }}</td>
                                                            <td>{{ $payment->spedition }}</td>
                                                            <td>{{ $payment->services }}</td>
                                                            <td>{{ $payment->partial }}</td>
                                                            <td>{{ $payment->additional }}</td>
                                                            <td>{{ $payment->total }}</td>
                                                            <td>{!! $payment->status == 1 ? '<p class="text-success">Yes</p>' : '<p class="text-danger">No</p>' !!}</td>
                                                            <td>{!! $payment->paid == 1 ? '<p class="text-success">Yes</p>' : '<p class="text-danger">No</p>' !!}</td>
                                                            <td><a href="{{ route('admin.payments.orders.confirm-accept', [$order->id, $payment->id] ) }}" data-toggle="modal" data-target="#confirm_payment" class="btn btn-primary {{ $alreadySent == true ? 'disabled': '' }}">Send Payment</a></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Panel block -->
                    </div>
                    <!-- /Tab 2-->
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    <div class="modal fade" id="confirm_payment" tabindex="-1" role="dialog" aria-labelledby="confirm_payment" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
