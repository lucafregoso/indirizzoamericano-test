@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Deleted orders
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
    <!-- end page css -->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <h1>Deleted orders</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                            Dashboard
                        </a>
                    </li>
                    <li><a href="#"> orders</a></li>
                    <li class="active">Deleted orders</li>
                </ol>
            </section>
            <!-- Main content -->
         <section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <i class="livicon" data-name="responsive-menu" data-size="18" data-c="#ffffff" data-hc="#ffffff"></i>
                    Deleted orders List
                </h4>
            </div>
            <div class="panel-body">
                <table class="table table-responsive" id="orders-table">
                    <thead>
                     <tr>
                        <th>ID</th>
                        <th>Tracking</th>
                        <th>User</th>
                        <th>Status</th>
                        <th colspan="3">Action</th>
                     </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{!! $order->id !!}</td>
                            <td>{!! $order->tracking !!}</td>
                            <td>{!! isset($order->user->email) ? $order->user->email : '' !!}</td>
                            <td>{!! App\Helpers\Helper::getOrderStatus($order->status) !!}</td>
                            <td>
                                 <a href="{{ route('admin.restore/order', $order->id) }}" class="btn default"><i class="fa fa-rotate-left"></i> Restore</a>
                                 <a href="{{ route('admin.orders.confirm-destroy', $order->id) }}" data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section("footer_scripts")
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
      </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#table').DataTable();

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
    </script>
@stop
