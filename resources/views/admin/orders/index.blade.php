@extends('admin./layouts/default')

@section('title')
Orders
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Orders</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Orders</li>
        <li class="active">Orders List</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Orders List
                </h4>
                <div class="pull-right">
                    <a href="{{ route('admin.orders.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            
            @if (Request::is('admin/orders'))
                <div class="panel-body">
                    <form method="POST" id="search-form" class="form-inline" role="form">
                        <div class="form-group">
                            <label for="status">Filter Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">All</option>
                                @foreach($status as $key => $value)
                                    <option value="{{ $key }}">{{ App\Helpers\Helper::getOrderStatus($key) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>
                </div>
            @endif

            <div class="panel-body table-responsive">
                 @include('admin.orders.table')
            </div>
        </div>
 </div>
</section>
@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $(function() {
            var route = '{!! route('admin.orders.data') !!}';
            @if (Request::is('admin/orders/requests'))
                route = '{!! route('admin.orders.requests.data') !!}';
            @elseif (Request::is('admin/orders/lists/new'))
                route = '{!! route('admin.orders.lists.new.data') !!}';
            @elseif (Request::is('admin/orders/lists/paid'))
                route = '{!! route('admin.orders.lists.paid.data') !!}';
            @endif
            var table = $('#orders-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: route,
                    data: function (d) {
                        d.status = $('select[name=status]').val();
                    }
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'uid', name: 'uid'},
                    { data: 'tracking', name: 'tracking'},
                    { data: 'user.first_name', name: 'user.first_name', "visible": false},
                    { data: 'user.last_name', name: 'user.last_name', "visible": false},
                    { data: 'user.uuic', name: 'user.uuic', "visible": false},
                    { data: 'user.email', name: 'user.email', "visible": false},
                    { data: 'user_data', name: 'user_data', searchable: false},
                    { data: 'status', name: 'status'},
                    { data: 'services', name: 'services', "defaultContent": "", "render": "[, ]", searchable: false},
                    { data: 'created_at', name:'created_at'},
                    { data: 'actions', name: 'actions', orderable: false, searchable: false }
                ]
            });
            table.on( 'draw', function () {
                $('.livicon').each(function(){
                    $(this).updateLivicon();
                });
            } );

            $('#search-form').on('submit', function(e) {
                table.draw();
                e.preventDefault();
            });
        });

    </script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
