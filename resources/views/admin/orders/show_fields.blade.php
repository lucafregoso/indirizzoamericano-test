<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $order->id !!}</p>
    <hr>
</div>

<!-- Tracking Field -->
<div class="form-group">
    {!! Form::label('tracking', 'Tracking:') !!}
    <p>{!! $order->tracking !!}</p>
    <hr>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user', 'User:') !!}
    <p>{!! $order->user->email !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! App\Helpers\Helper::getOrderStatus($order->status) !!}</p>
    <hr>
</div>

<!-- Packages Field -->
<div class="form-group">
    {!! Form::label('packages', 'Packages:') !!}
    @foreach($order->packages()->get() as $package)
        <p>{!! $package->id !!}</p>
    @endforeach
    <hr>
</div>

