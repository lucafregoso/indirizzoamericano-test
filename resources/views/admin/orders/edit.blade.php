@extends('admin./layouts/default')

@section('title')
Orders
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Orders Edit</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                 Dashboard
             </a>
         </li>
         <li>Orders</li>
         <li class="active">Edit Orders </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Edit  Order
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($order, ['route' => ['admin.orders.update', $order->id], 'method' => 'patch']) !!}

            <!-- Tracking Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('tracking', 'Tracking Code:') !!}
                {!! Form::text('tracking', null, ['class' => 'form-control']) !!}
            </div>

            <!-- User Id Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('user_id', 'Users:') !!}
                <select class="form-control select2" id="select-user" name="user_id">
                    @foreach($users as $user)
                        <option value="{{$user->id}}" {{ (isset($order) && $order->user_id == $user->id) ? 'selected' : '' }}>{{$user->uuic}} - {{$user->first_name}} {{$user->last_name}} - {{$user->email}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-sm-12">
                {!! Form::label('packages', 'Packages:') !!}
                <select class="form-control select2" id="select-packages" name="packages[]" multiple>
                    @foreach($packages as $package)
                        <option value="{{$package->id}}" {{ (isset($related_packages) && $related_packages->contains($package->id)) ? 'selected' : '' }}>{{$package->uid}} - {{$package->barcode}}</option>
                    @endforeach
                </select>
            </div>

            <!-- Status Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('status', 'Status:') !!}
                {!! Form::select('status', $order_status, null, ['name'=>'status', 'class' => 'form-control']) !!}
            </div>

            <h2>Services</h2>
            <div class="form-group col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($services as $service)
                            <tr>
                                <td>{{  $service->id }}</td>
                                <td>{{  $service->name }}</td>
                                <td>{{  $service->description }}</td>
                                <td>
                                    {!! Form::checkbox('services[]', $service->id, (isset($related_services) && $related_services->contains($service->id)) ? true : false, ['class' => '']) !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Submit Field -->
            <div class="form-group col-sm-12 text-center">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{!! route('admin.orders.index') !!}" class="btn btn-default">Cancel</a>
            </div>

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript">
        $("#select-user").select2({
            theme:"bootstrap",
            placeholder:"Select user..."
        });

        $("#select-packages").select2({
            theme:"bootstrap",
            placeholder:"Select packages..."
        });
    </script>
@stop