<table class="table table-responsive" id="warehouses-table">
    <thead>
     <tr>
        <th>Name</th>
        <th>Address</th>
        <th>City</th>
        <th>State</th>
        <th>Zip Code</th>
        <th>Phone</th>
        <th>Fax</th>
        <th>Facebook</th>
        <th>Email</th>
        <th colspan="3">Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($warehouses as $warehouse)
        <tr>
            <td>{!! $warehouse->name !!}</td>
            <td>{!! $warehouse->address !!}</td>
            <td>{!! $warehouse->city !!}</td>
            <td>{!! $warehouse->state !!}</td>
            <td>{!! $warehouse->zip_code !!}</td>
            <td>{!! $warehouse->phone !!}</td>
            <td>{!! $warehouse->fax !!}</td>
            <td>{!! $warehouse->facebook !!}</td>
            <td>{!! $warehouse->email !!}</td>
            <td>
                 <a href="{{ route('admin.warehouses.edit', $warehouse->id) }}" class="btn default"><i class="fa fa-edit"></i> Edit</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop