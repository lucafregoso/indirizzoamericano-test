<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $warehouse->id !!}</p>
    <hr>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $warehouse->name !!}</p>
    <hr>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $warehouse->description !!}</p>
    <hr>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $warehouse->address !!}</p>
    <hr>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $warehouse->city !!}</p>
    <hr>
</div>

<!-- State Field -->
<div class="form-group">
    {!! Form::label('state', 'State:') !!}
    <p>{!! $warehouse->state !!}</p>
    <hr>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('zip_code', 'Zip Code:') !!}
    <p>{!! $warehouse->zip_code !!}</p>
    <hr>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $warehouse->phone !!}</p>
    <hr>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $warehouse->fax !!}</p>
    <hr>
</div>

<!-- Facebook Field -->
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:') !!}
    <p>{!! $warehouse->facebook !!}</p>
    <hr>
</div>

<!-- Facebook Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $warehouse->email !!}</p>
    <hr>
</div>