@extends('layouts/default')

{{-- Page title --}}
@section('title')
Suggestion
@parent
@stop

{{-- header styles --}}
@section('header_styles')
@stop

{{-- content --}}
@section('content')
<section class="page-content">
<iframe id="frame" src="" onload="this.width=screen.width;this.height=screen.height;"></iframe>
</section><!-- @end .page-content -->
@stop

{{-- footer scripts --}}
@section('footer_scripts')
<script>
    var route = '{{ route('suggestions.proxyme') }}';
    var url = '{{ $suggestion->url }}';
    document.getElementById('frame').src = route+'?url='+encodeURIComponent(url);
</script>
@stop
