@extends('layouts/default')

{{-- Page title --}}
@section('title')
Home
@parent
@stop

{{-- header styles --}}
@section('header_styles')
@stop

{{-- content --}}
@section('content')
<section class="page-content">
    <article class="wrap columns cf">
        <div class="column m-all t-all d-all">
            <header>
                <h1 class="bold italic">FAI SHOPPING ON LINE NEGLI STATI UNITI, AL RESTO PENSIAMO NOI!</h1>
            </header>
            <section class="spaceB50">
                Ti offriamo un sito con supporto interamente in italiano con un servizio studiato per garantirti prezzi chiari e competitivi.
                Spedizione fatta su misura alle tue esigenze, i tuoi pacchi sono gestiti da un software di altissimo livello creato su misura per questo servizio e nel caso ti servisse forniamo anche un  servizio di personalshopper a prezzi migliori del mercato.
            </section>
        </div>
    </article>

    <article class="wrap columns cf">
        <div class="column m-all t-all d-all">
            <header class="text-center spaceB20">
                <h2 class="bold italic">Come funziona? Guarda il video</h2>
            </header>
            <section class="home-video">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/pllt7JlI5fM" frameborder="0" allowfullscreen></iframe>
            </section>
        </div>
    </article>

    <div class="wrap columns spaceTB30 cf">
        <div class="column m-all t-1of3 d-1of3">
            <div class="columns home-plus-container spaceB20">
                <div class="column m-1of2 t-all d-all home-plus-img">
                    <img src="{{ asset('assets/library/images/homepage/plus1.png') }}" alt=""/>
                </div>
                <div class="column m-1of2 t-all d-all home-plus-text">
                    <p class="italic bold blue">Ottieni subito il tuo indirizzo americano gratuitamente.</p>

                    <a class="red-button-like" href="{{ route('auth') }}">registrati</a>
                </div>
            </div>
        </div>
        <div class="column m-all t-1of3 d-1of3">
            <div class="columns home-plus-container spaceB20">
                <div class="column m-1of2 t-all d-all home-plus-img">
                    <img src="{{ asset('assets/library/images/homepage/plus2.png') }}" alt=""/>
                </div>
                <div class="column m-1of2 t-all d-all home-plus-text">
                    <p class="italic bold blue">Fai shopping on line e fai spedire tutto al tuo nuovo indirizzo.</p>

                    <a class="red-button-like" href="{{ route('static.options') }}">scopri i vantaggi</a>
                </div>
            </div>
        </div>
        <div class="column m-all t-1of3 d-1of3">
            <div class="columns home-plus-container spaceB20">
                <div class="column m-1of2 t-all d-all home-plus-img">
                    <img src="{{ asset('assets/library/images/homepage/plus3.png') }}" alt=""/>
                </div>
                <div class="column m-1of2 t-all d-all home-plus-text">
                    <p class="italic bold blue">Uniremo i tuoi acquisti facendoti risparmiare sulla spedizione verso casa tua!</p>
                    <a class="red-button-like" href="{{ route('static.rates') }}">le nostre tariffe</a>
                </div>
            </div>
        </div>
    </div>

    <article class="wrap columns cf">
        <div class="column m-all t-all d-all">
            <hr/>
            <header class="text-center spaceB20">
                <h2 class="bold italic">Non sai dove cominciare?  Eccoti qualche suggerimento!</h2>
            </header>
            <section class="home-brands">
                <div class="columns columns-center">
                    @foreach ($suggestions as $suggestion)
                        <div class="column m-1of2 t-1of4 d-1of6">
                            <a href="{{ route('suggestions', $suggestion->id) }}">
                                <img src="{{ url('/').'/uploads/files/'.App\Helpers\Helper::getFilePath($suggestion->image->filename).$suggestion->image->filename.'.'.$suggestion->image->extension }}">
                            </a>
                        </div>
                    @endforeach
                    {{-- <div class="column m-1of2 t-1of4 d-1of6">
                        <img src="{{ asset('assets/library/images/homepage/hollister.png') }}" alt=""/>
                    </div>
                    <div class="column m-1of2 t-1of4 d-1of6">
                        <img src="{{ asset('assets/library/images/homepage/best-buy.png') }}" alt=""/>
                    </div>
                    <div class="column m-1of2 t-1of4 d-1of6">
                        <img src="{{ asset('assets/library/images/homepage/new-egg.png') }}" alt=""/>
                    </div>
                    <div class="column m-1of2 t-1of4 d-1of6">
                        <img src="{{ asset('assets/library/images/homepage/harley.png') }}" alt=""/>
                    </div> --}}
                </div>
            </section>
            <section class="home-brands-link text-center spaceT30 spaceB30">
                <a class="red-button-like" href="#">go shopping!</a>
            </section>
            <hr/>
        </div>
    </article>



    <article class="wrap columns spaceT30 cf">
        <div class="column m-all t-all d-all">
            <header class="text-center spaceB20">
                <h2 class="bold italic">Se sei soddisfatto lo siamo anche noi!</h2>
            </header>
            <section class="home-brands">
                <div class="columns columns-center">
                    <div class="column m-1of2 t-1of4 d-1of4 text-center">
                        <div class="home-contacts-img">
                            <img src="{{ asset('assets/library/images/homepage/like.png') }}" alt=""/>
                        </div>
                        <div class="home-plus-text">
                            <p class="italic bold blue">Condividi la tua esperienza sulla nostra pagina facebook!</p>

                            <a class="red-button-like" href="{{ $_warehouse_info->facebook }}" target="_blank">like us on facebook</a>
                        </div>
                    </div>
                    <div class="column m-1of2 t-1of4 d-1of4 text-center">
                        <div class="home-contacts-img">
                            <img src="{{ asset('assets/library/images/homepage/contacts.png') }}" alt=""/>
                        </div>
                        <div class="home-plus-text">
                            <p class="italic bold blue">Dicci come possiamo migliorare il nostro servizio!</p>

                            <a class="red-button-like" href="{{ route('static.contacts') }}">contattaci</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </article>
</section><!-- @end .page-content -->
@stop

{{-- footer scripts --}}
@section('footer_scripts')
@stop
