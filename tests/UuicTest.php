<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;
use App\Helpers\Helper;

class UuicTest extends TestCase
{
    use DatabaseMigrations;

    public function testIfUuicReturnString()
    {
        $uuic = Helper::generate_uuic();

        $this->assertInternalType('string', $uuic);
    }

    public function testCountUuicCharacters()
    {
        $uuic = Helper::generate_uuic();

        $this->assertGreaterThanOrEqual(6, strlen($uuic));
    }

    public function testGenerateMultipleCodeInArrayAndCheckIfUnique()
    {
        $stack = [];
        $last_code = null;
        for ($i=0; $i < 500000; $i++) { 
            $stack[] = $last_code = Helper::alg_uuic($last_code);
        }

        $this->assertEquals(count($stack), count(array_unique($stack)));
    }

    public function testGenerateUuicCodeSaveInDatabaseAndTestIfUnique()
    {
        $stack = [];
        for ($i=0; $i < 1000; $i++) { 
            //$new_user = factory(User::class)->create(['email' => $uemail]);
            $new_user = User::create(['email' => 'example+'.$i.'@example.com', 'password' => rand(), 'uuic' => Helper::generate_uuic() ]);
            $stack[] = $new_user->uuic;
        }

        $this->assertEquals(count($stack), count(array_unique($stack)));
    }

}
