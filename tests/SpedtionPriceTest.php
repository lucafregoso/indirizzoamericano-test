<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Helpers\Helper;

class SpeditionPriceTest extends TestCase
{

    use DatabaseMigrations;

    public function testIfReturnZeroWithoutParams()
    {
        $price = Helper::getSpeditionPrice();

        $this->assertEquals(0, $price);
    }

    public function testIfReturnZeroWhenPassANoNumberParamsWithOnlyOneParmas()
    {
        $price = Helper::getSpeditionPrice('test');

        $this->assertEquals(0, $price);
    }

    public function testIfReturnNumberWithAllParams()
    {
        $price = Helper::getSpeditionPrice(1, 1, 1, 1, 1);

        $this->assertInternalType('float', $price);
    }

    public function testIfReturnCorrectNumberWithAllParams()
    {
        $price = Helper::getSpeditionPrice(1, 1, 1, 1, 1);
        $human_result = 27.25;

        $this->assertEquals($human_result, $price);
    }

    public function testWeightIfReturnCorrectNumber()
    {
        $price = Helper::getSpeditionPrice(1, 1, 1, 1, 10);
        $human_result = 57.42;

        $this->assertEquals($human_result, $price);
    }

    public function testApproximationWeight()
    {
        $price = Helper::getSpeditionPrice(1, 1, 1, 1, 10.99);
        $human_result = 57.42;

        $this->assertEquals($human_result, $price);
    }

    public function testIfReturnCorrectNumberGreaterThenFirst()
    {
        $price1 = Helper::getSpeditionPrice(1, 1, 1, 1, 1);
        $price2 = Helper::getSpeditionPrice(2, 2, 2, 2, 2);

        $this->assertGreaterThanOrEqual($price1, $price2);
    }

    public function testIfVolumetricReturnANumber()
    {
        $helper = new Helper();
        $volumetric = $this->invokeMethod($helper, 'getVolumetricPrice', [1, 1, 1]);

        $this->assertInternalType('float', $volumetric);
    }

    public function testIfWeightReturnANumber()
    {
        $helper = new Helper();
        $weight = $this->invokeMethod($helper, 'getWeightPrice', [1]);

        $this->assertInternalType('float', $weight);
    }

    public function testIfReturnRightANumber()
    {
        $helper = new Helper();
        $weight_price = $this->invokeMethod($helper, 'getWeightPrice', [1]);
        $volumetric_price = $this->invokeMethod($helper, 'getVolumetricPrice', [1, 1, 1]);

        $human_choice = $weight_price;
        $cpu_choice = ($volumetric_price > $weight_price) ? $volumetric_price : $weight_price;

        $this->assertEquals($human_choice, $cpu_choice);
    }

}
