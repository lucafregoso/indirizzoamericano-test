<?php

namespace App\Repositories;

use App\Warehouse;
use InfyOm\Generator\Common\BaseRepository;

class WarehouseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'address',
        'city',
        'state',
        'zip_code',
        'phone',
        'fax',
        'facebook',
        'email',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Warehouse::class;
    }
}
