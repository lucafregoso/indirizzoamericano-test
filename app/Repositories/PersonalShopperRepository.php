<?php

namespace App\Repositories;

use App\PersonalShopper;
use InfyOm\Generator\Common\BaseRepository;

class PersonalShopperRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reference',
        'note'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PersonalShopper::class;
    }
}
