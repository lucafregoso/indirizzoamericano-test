<?php

namespace App\Repositories;

use App\Suggestion;
use InfyOm\Generator\Common\BaseRepository;

class SuggestionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'image',
        'url',
        'blank'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Suggestion::class;
    }
}
