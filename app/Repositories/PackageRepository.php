<?php

namespace App\Repositories;

use App\Package;
use InfyOm\Generator\Common\BaseRepository;

class PackageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'quantity',
        'color',
        'size',
        'price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Package::class;
    }
}
