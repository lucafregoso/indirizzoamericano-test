<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class PersonalShopperProduct extends Model
{
    use SoftDeletes;

    public $table = 'personal_shopper_products';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'personal_shopper_id',
        'quantity',
        'article_code',
        'size',
        'color',
        'note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'quantity' => 'string',
        'article_code' => 'string',
        'size' => 'string',
        'color' => 'string',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
