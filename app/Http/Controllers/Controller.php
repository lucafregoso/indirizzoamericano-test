<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use View;
use App\Package;
use App\Order;
use App\Warehouse;
use S2K\Mercury\Conversation;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //Service Package
        $_count_packages_need_service = Package::whereHas('serviceRequests', function($q) {
                return $q->where('status', 0);
        })->with('serviceRequests.service')->count();
        View::share ( '_count_packages_need_service', $_count_packages_need_service );

        //Service Orders
        $_count_orders_need_service = Order::whereHas('serviceRequests', function($q) {
                return $q->where('status', 0);
        })->with('serviceRequests.service')->count();
        View::share ( '_count_orders_need_service', $_count_orders_need_service );

        //New Orders
        $_count_orders_new = Order::where('status', 0)->count();
        View::share ( '_count_orders_new', $_count_orders_new );

        //Orders Paid
        $_count_orders_paid = Order::where('status', 3)->count();
        View::share ( '_count_orders_paid', $_count_orders_paid );

        //Sum badge orders
        $_badge_orders = $_count_orders_need_service + $_count_orders_new + $_count_orders_paid;
        View::share ( '_badge_orders', $_badge_orders );

        //Conversations
        $_count_conversations_new = Conversation::where('status', 'new')->count();
        View::share ( '_count_conversations_new', $_count_conversations_new );

        //Info Warehouse
        $_warehouse_info = Warehouse::first();
        View::share ( '_warehouse_info', $_warehouse_info );
    }

}
