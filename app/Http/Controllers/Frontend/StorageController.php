<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\JoshController as Controller;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use Redirect;
use App\Warehouse;
use App\Notification;

class StorageController extends Controller
{
    protected $user;
    protected $notifications;

    public function __construct()
    {
        parent::__construct();

        //Get User info
        $this->middleware(function ($request, $next) {
            $this->user = Sentinel::getUser();
            View::share ( 'user', $this->user );

            $this->notifications = Notification::where('user_id', $this->user->id)->where('status', 0)->get();
            View::share ( 'badge_notifications', $this->notifications->count() );

            return $next($request);
        });

        //Get info IA Warehouse
        $warehouse = Warehouse::first();
        View::share ( 'warehouse', $warehouse );
    }

    public function dashboard()
    {
        return view('storage.dashboard', ['notifications' => $this->notifications]);
    }
}
