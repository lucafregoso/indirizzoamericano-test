<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\JoshController as Controller;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Lang;
use Mail;
use Reminder;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Suggestion;
use App\Newsletter;

class HomeController extends Controller
{
    /**
     * Homepage site
     * 
     * @return View
     */
    public function index()
    {
        $suggestions = Suggestion::get();

        return view('index', ['suggestions' => $suggestions]);
    }

    /**
     * explanation page
     * 
     * @return View
     */
    public function explanation()
    {
        return view('static.explanation');
    }

    /**
     * shipping page
     * 
     * @return View
     */
    public function shipping()
    {
        return view('static.shipping');
    }

    /**
     * why page
     * 
     * @return View
     */
    public function why()
    {
        return view('static.why');
    }

    /**
     * rates page
     * 
     * @return View
     */
    public function rates()
    {
        return view('static.rates');
    }

    /**
     * options page
     * 
     * @return View
     */
    public function options()
    {
        return view('static.options');
    }

    /**
     * contacts page
     * 
     * @return View
     */
    public function contacts()
    {
        return view('static.contacts');
    }

    /**
     * support page
     * 
     * @return View
     */
    public function support()
    {
        return view('static.support');
    }

    /**
     * track page
     * 
     * @return View
     */
    public function track()
    {
        return view('static.track');
    }

    /**
     * register email for newsletter
     * 
     * @return Json
     */
    public function newsletters(Request $request)
    {
        // Declare the rules for the form validation
        $rules = array(
            'email' => 'required|email|unique:newsletters',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make($request->all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return response()->json([], 400);
        }

        $newsletter = Newsletter::create(['email' => $request->email ]);

        if ($newsletter) {
            return response()->json([], 200);
        }

        return response()->json([], 400);
    }
}
