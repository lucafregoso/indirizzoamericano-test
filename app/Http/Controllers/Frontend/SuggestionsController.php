<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\JoshController as Controller;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Lang;
use Mail;
use Reminder;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Suggestion;

class SuggestionsController extends Controller
{
    /**
     * Suggestion page
     * 
     * @return View
     */
    public function index($suggestion = null)
    {
        if (!$suggestion) {
            $suggestion = 1;
        }

        $suggestion = Suggestion::find($suggestion);

        if (!$suggestion) {
            return Redirect::route('home');
        }

        return view('suggestions',['suggestion' => $suggestion]);
    }

    public function proxyMe(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        echo $this->proxyPage($request->url);
    }

    public function proxyPage($url = null)
    {
        if ($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            /*
            $proxy = 'http://proxy.company.com:8080';
            $proxyauth = 'domain\proxy_username:proxy_password';
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            */
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $data = curl_exec($ch);
            curl_close($ch);

            return $data;
        }

    }

}
