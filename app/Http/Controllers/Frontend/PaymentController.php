<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Order;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Helpers\Helper;
use App\Package;
use Redirect;
use Datatables;
use App\Service;
use App\Payment;
use Mail;
use Paypalpayment;
use Sentinel;
use App\PaymentTransaction;

class PaymentController extends Controller
{
    /**
     * object to authenticate the call.
     * @param object $_apiContext
     */
    protected $_apiContext;

    public function __construct()
    {
        parent::__construct();

        $config = config('paypal_payment');
        $flatConfig = array_dot($config);

        $this->_apiContext = Paypalpayment::ApiContext(config('paypal_payment.Account.ClientId'), config('paypal_payment.Account.ClientSecret'));
        $this->_apiContext->setConfig($flatConfig);
    }

    /**
     * getCheckOut PayPal
     *
     * @return Response
     */
    public function getCheckout(Order $order, Request $request)
    {

        $user = Sentinel::getUser();

        if (!$user) {
            return Redirect::route('auth');
        }

        if ($order) {

            $orderPayment = $order->payments()->where('payments.status', 1)->where('payments.paid', 0)->first();
            if ($orderPayment) {

                // #Payer
                $payer = Paypalpayment::payer();
                $payer->setPaymentMethod("paypal");

                $item1 = Paypalpayment::item();
                $item1->setName('Indirizzo Americano - Order'. $order->id)
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($orderPayment->total);

                $itemList = Paypalpayment::itemList();
                $itemList->setItems([$item1]);

                $details = Paypalpayment::details();
                $details->setShipping('0')->setTax('0');

                //Amount
                $amount = Paypalpayment::amount();
                $amount->setCurrency("USD")->setTotal($orderPayment->total)->setDetails($details);

                //Transaction
                $transaction = Paypalpayment::transaction();
                $transaction->setAmount($amount)
                    ->setItemList($itemList)
                    ->setDescription("Payment Order Indirizzo Americano")
                    ->setInvoiceNumber(uniqid());

                //Redirect urls
                $doneUrl = route('storage.orders.checkout.done', $order->id);
                $cancelUrl = route('storage.orders.checkout.cancel', $order->id);
                $redirectUrls = Paypalpayment::redirectUrls();
                $redirectUrls->setReturnUrl($doneUrl)->setCancelUrl($cancelUrl);

                //Payment
                $payment = Paypalpayment::payment();
                $payment->setIntent("sale")
                    ->setPayer($payer)
                    ->setRedirectUrls($redirectUrls)
                    ->setTransactions(array($transaction));

                // Create Payment
                try {
                    $payment->create($this->_apiContext);
                } catch (\Exception $ex) {
                    \Log::error($ex);
                    return Redirect::route('storage.orders');
                }

                PaymentTransaction::create([
                    'user_id' => $user->id,
                    'payment_id' => $orderPayment->id,
                    'paypal_id' => $payment->id,
                    'invoice_number' => $payment->transactions[0]->invoice_number,
                    'payment_method' => $payment->payer->payment_method,
                    'state' => $payment->state,
                    'total' => $payment->transactions[0]->amount->total,
                ]);

                $approvalUrl = $payment->getApprovalLink();

                return Redirect::to($approvalUrl);

                // echo "Created Payment Using PayPal. Please visit the URL to Approve.Payment <a href={$approvalUrl}>{$approvalUrl}</a>";
                // var_dump($payment);
                // die();
            }
            //No Payment found
            return Redirect::route('storage.orders');
        }

        //No order found
        return Redirect::route('storage.orders');
    }

    public function getDone(Request $request)
    {
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');

        $payment_transaction = PaymentTransaction::where('paypal_id', $id)->first();

        $payment = Payment::where('id', $payment_transaction->payment_id)->first();

        if ($payment) {
            // Update status payment
            $payment->update(['paid' => 1]);

            // Update status order
            $order = $payment->order()->first();
            if ($order) {
                $order->update(['status' => 3]);
            }

            return Redirect::route('storage.orders');
        }

        return Redirect::route('storage.orders');

    }

    public function getCancel()
    {
        return Redirect::route('storage.orders');
    }

}
