<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\StorageController;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use Redirect;
use App\Package;
use App\Service;
use App\Order;
use App\OrderAddress;
use App\Events\NotificationEvent;
use Event;

class PackagesController extends StorageController
{
    public function __construct()
    {
        parent::__construct();
    }

    /*
    *   Get Packages List
    */
    public function getPackages()
    {
        $packages = Package::where('user_id', $this->user->id)->where('order_id', null)->get();
        $services = Service::where('reference', 'orders')->get();

        return view('storage.packages.index', ['packages' => $packages, 'services' => $services]);
    }

    /*
    *   Create Order
    */
    public function postPackages(Request $request)
    {
        $id_service_extra_address = Service::where('slug', 'extra_address')->first();

        $request->request->add(['service_extra_address' => 0]);
        if ($request->services) {
            if (in_array($id_service_extra_address->id, $request->services)) {
                $request->request->add(['service_extra_address' => 1]);
            }
        }

        // Declare the rules for the form validation
        $rules = [
            'packages'                    => 'required',
            'extra_address.address'       => 'required_if:service_extra_address,1',
            'extra_address.city'          => 'required_if:service_extra_address,1',
            'extra_address.zip_code'      => 'required_if:service_extra_address,1',
            'extra_address.state'         => 'required_if:service_extra_address,1',
        ];

        // Create a new validator instance from our validation rules
        $validator = Validator::make($request->all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        if (count($request->packages)) {

            //Check if package have value
            foreach ($request->packages as $package_id) {
                $package = Package::where('id', $package_id)->first();
                if (!$package->value) {
                    $validator->errors()->add('field', Lang::get('validation.custom.package_value.required'));
                    return Redirect::back()->withInput()->withErrors($validator);
                }
            }

            $order = Order::create([
                'user_id' => $this->user->id,
                'status' => 0,
            ]);

            if ($order) {
                //Set packages
                foreach ($request->packages as $package_id) {
                    $package = Package::where('id', $package_id)->first();
                    if ($package) {
                        $package->update(['order_id' => $order->id]);
                    }
                }

                //Set service
                if (count($request->services)) {
                    foreach ($request->services as $service) {
                        $service_request = $order->serviceRequests()->create([ 'user_id' => $this->user->id, 'service_id' => $service]);

                        if ($check_service = Service::find($service)) {
                            if ($check_service && $check_service->slug == 'extra_address') {

                                //Save extra address
                                if (count($request->extra_address)) {
                                    $extra_address_data = [
                                        "address" => $request->extra_address['address'],
                                        "city" => $request->extra_address['city'],
                                        "zip_code" => $request->extra_address['zip_code'],
                                        "state" => $request->extra_address['state'],
                                        "service_request_id" => $service_request->id
                                    ];
                                    
                                    if (count(array_filter($extra_address_data)) == count($extra_address_data)) {
                                        $extra_address = OrderAddress::create($extra_address_data);
                                    }
                                }

                            }
                        }

                    }
                }

                Event::fire(new NotificationEvent($this->user, Lang::get('notifications.orders.crated')));

                return  Redirect::route('storage.packages.success');
            }

            return Redirect::back()->withInput()->withErrors(['error' => 'order error']);
        }

        return Redirect::back()->withInput()->withErrors(['error' => 'no packages selected']);
    }

    /*
    *   Request service photo
    */
    public function servicePhotos(Package $package, Request $request)
    {
        if ($package->user_id == $this->user->id) {
            $photo_service = Service::where('slug', 'photo')->first();
            $package->serviceRequests()->create(['user_id' => $this->user->id, 'service_id' => $photo_service->id]);

            return Redirect::back()->with('success', Lang::get('message.success.update'));
        }

        return Redirect::back()->withInput()->withErrors(['error' => 'permission deniend']);
    }

    public function postPackagesValue(Request $request)
    {
        $input = $request->all();
        if ($input['package'] && (float) $input['value']) {
            if ($input['value'] > 0) {

                $package = Package::where('id', $input['package'])->first();
                if ($package) {
                    $package->update([ 'value' => (float) $input['value'] ]);

                    return Redirect::back()->withInput()->with('success', Lang::get('message.success.update'));
                }
            }
        }

        return Redirect::back()->withInput()->withErrors(['error' => 'permission deniend']);
    }

    public function success()
    {
        return view('storage.packages.success');
    }
}
