<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\StorageController;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use Redirect;
use App\Item;
use App\Order;

class OrdersController extends StorageController
{
    public function getOrders()
    {
        $orders = Order::where('user_id', $this->user->id)->get();

        return view('storage.orders.index', ['orders' => $orders, 'notifications' => $this->notifications]);
    }
}
