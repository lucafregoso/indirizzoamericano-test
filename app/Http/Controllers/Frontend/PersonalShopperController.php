<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\StorageController;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use Redirect;
use App\Package;
use App\Order;
use App\PersonalShopper;
use App\PersonalShopperProduct;

class PersonalShopperController extends StorageController
{

    public function getPersonalShopper()
    {
        return view('storage.personal_shoppers.index');
    }

    public function postPersonalShopper(Request $request)
    {
        $input = $request->all();

        // Declare the rules for the form validation
        $rules = [
            'reference'                 => 'required',
            'products.*.quantity'       => 'required',
            'products.*.article_code'   => 'required',
        ];

        // Create a new validator instance from our validation rules
        $validator = Validator::make($request->all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $order = PersonalShopper::create([
            'reference' => $request->reference,
            'note' => $request->note,
            'user_id' => $this->user->id,
            'status' => 0
        ]);

        if ($order) {
            if($request->products) {
                foreach ($request->products as $product) {
                    $new_product = PersonalShopperProduct::create([
                        'personal_shopper_id' => $order->id,
                        'quantity' => $product['quantity'] != '' ? $product['quantity'] : null, 
                        'article_code' => $product['article_code'] != '' ? $product['article_code'] : null, 
                        'size' => $product['size'] != '' ? $product['size'] : null, 
                        'color' => $product['color'] != '' ? $product['color'] : null, 
                        'note' => $product['note'] != '' ? $product['note'] : null, 
                    ]);
                }
            }
        }

        return Redirect::route('storage.personal-shopper.order', $order->id);
    }

    public function getPersonalShopperOrder(PersonalShopper $order, Request $request)
    {
        if ($order->user_id == $this->user->id) {
            if ($order->status == 0) {
                return view('storage.personal_shoppers.order', [ 'order' => $order ]);
            }
        }

        return Redirect::route('storage.personal-shopper');
    }

    public function confirmOrderPersonalShopper(PersonalShopper $order, Request $request)
    {
        if ($order->user_id == $this->user->id) {
            if ($order->status == 0) {
                $order->update(['status' => 1]);

                return Redirect::route('storage.personal-shopper-success');
            }
        }

        return Redirect::route('storage.personal-shopper');
    }

    public function success()
    {
        return view('storage.personal_shoppers.success');
    }
}
