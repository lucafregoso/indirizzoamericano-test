<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\StorageController;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use Redirect;
use App\Item;
use App\Order;
use App\Notification;

class NotificationsController extends StorageController
{
    public function index()
    {
        $notifications = Notification::where('user_id', $this->user->id)->orderBy('id', 'DESC')->get();

        return view('storage.notifications.index', ['notifications' => $notifications]);
    }


    public function updateNotification(Request $request)
    {
        $notification = Notification::where('user_id', $this->user->id)->where('id', $request->id)->first();

        if ($notification) {
            $notification->update(['status' => 1]);

            return response()->json([], 200);
        }

        return response()->json([], 400);
    }
}
