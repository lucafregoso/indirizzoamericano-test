<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\StorageController;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use Redirect;
use App\Item;
use App\Order;
use Image;

class UserController extends StorageController
{

    public function getProfileEdit()
    {
        $user = Sentinel::getUser();

        return view('storage.users.profile', ['user' => $user]);
    }

    public function postProfileEdit(Request $request)
    {

        $user = Sentinel::getUser();

        if (!$user) {
            $this->messageBag->add('email', Lang::get('message.delete.update'));

            return Redirect::back()->withInput()->withErrors($this->messageBag);
        }

        // Declare the rules for the form validation
        $rules = [
            'first_name'        => 'required|min:2',
            'last_name'         => 'required|min:2',
            'dob'               => 'required|date',
            'tax'               => 'required|min:10|max:16',
            'phone'             => 'required|min:3',
            'address'           => 'required|min:2',
            'city'              => 'required|min:2',
            'zip_code'          => 'required|min:2',
            'state'             => 'required|min:2',
            'email'             => 'required|email|unique:users,email,'.$user->id,
        ];

        // Create a new validator instance from our validation rules
        $validator = Validator::make($request->all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $request_data = [
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'dob'           => Carbon::parse($request->dob)->format('Y-m-d'),
            'tax'           => $request->tax,
            'phone'         => $request->phone,
            'address'       => $request->address,
            'city'          => $request->city,
            'zip_code'      => $request->zip_code,
            'state'         => $request->state,
            'email'         => $request->email,
        ];

        if ($file = $request->file('avatar')) {
            $filename         = md5(uniqid(microtime(), true));
            $path             = Helper::getFilePath($filename);
            $originalname     = $file->getClientOriginalName();
            $extension        = $file->getClientOriginalExtension();
            $mimetype         = $file->getClientMimeType();
            $size             = $file->getClientSize();
            $destination_path = public_path().'/uploads/users/'.Helper::getFilePath($filename);
            $remote_url       = url('/').'/uploads/users/'.Helper::getFilePath($filename).$filename.'.'.$extension;

            $make_destination = mkdir($destination_path, 0755, true);

            if ($make_destination) {
                $maxWidth = 150;
                $avatar = Image::make($file);
                if($size > $maxWidth){
                    // $avatar->resize($maxWidth, null, function ($constraint) {
                    //     $constraint->aspectRatio();
                    // });
                    $avatar->resize($maxWidth, $maxWidth);
                }
                //$avatar->crop($maxWidth, $maxWidth);

                $save = $avatar->save($destination_path . '/'. $filename . '.' . $extension);

                if($save) {
                    $request_data['pic'] = $filename.'.'.$extension;
                }
            }
        }

        try {
            // Register the user
            $user->update($request_data); 

            // Redirect to the home page with success menu
            return Redirect::back()->with('success', Lang::get('message.success.update'));
            //return View::make('user_account')->with('success', Lang::get('auth/message.signup.success'));

        } catch (UserExistsException $e) {
            $this->messageBag->add('email', Lang::get('message.delete.update'));
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }
}
