<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\StorageController;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Sentinel;
use URL;
use Validator;
use View;
use App\User;
use Carbon\Carbon;
use App\Helpers\Helper;
use Redirect;
use App\Package;
use App\Service;
use App\Order;
use App\OrderAddress;
use App\Events\NotificationEvent;
use Event;

class OptionsController extends StorageController
{
    public function __construct()
    {
        parent::__construct();
    }

    /*
    *   Get User services lists
    */
    public function getOptions()
    {
        $user = $this->user;
        $services = Service::where('reference', 'users')->get();
        $related_services = $user->serviceRequests()->pluck('service_id');

        return view('storage.options.index', ['services' => $services, 'related_services' => $related_services]);
    }

    /*
    *   Save user services
    */
    public function postOptions(Request $request)
    {
        $user = $this->user;

        if ($user) {
            $request->services = count($request->services) ? $request->services : [];

            //Delete services
            $ServiceRequest = $user->serviceRequests()->whereNotIn('service_id', $request->services)->delete();

            if (count($request->services)) {
                foreach ($request->services as $service) {
                    if (!$user->serviceRequests()->where('service_id', $service)->first()) {
                        $user->serviceRequests()->create([ 'user_id' => $user->id, 'service_id' => $service, 'status' => 1]);
                    }
                }
            }
        }

        return Redirect::back();
    }

}
