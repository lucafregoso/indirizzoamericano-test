<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Order;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Helpers\Helper;
use App\Package;
use Redirect;
use Datatables;
use App\Service;
use App\Notification;
use Event;
use App\Events\NotificationEvent;
use Excel;
use App\Newsletter;

class NewslettersController extends Controller
{

    /**
     * Export all users newsletters
     *
     * @param Request $request
     * @return Excel
     */
    public function export(Request $request)
    {
        Excel::create('Users-Newsletter'. time(), function($excel) {

            $excel->sheet('Excel sheet', function($sheet) {

                $sheet->setOrientation('landscape');

                $sheet->row(1, array(
                    'Emails'
                ));

                $sheet->cells('A1:A1', function($cells) {
                    $cells->setBackground('#f1f1f1');
                });
                $newsletters = Newsletter::get();
                $users = User::whereNotIn('email', array_pluck($newsletters->toArray(), 'email'))->where('newsletter', 1)->get();

                foreach($newsletters as $nl) {
                    $sheet->appendRow(array($nl->email));
                }

                foreach($users as $user) {
                    $sheet->appendRow(array($user->email));
                }
            });

        })->export('xls');
    }

}
