<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Order;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Helpers\Helper;
use App\Package;
use Redirect;
use Datatables;
use App\Service;
use App\Payment;
use Mail;

class PaymentController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the Order Payment.
     *
     * @param Order $order
     * @param Request $request
     * @return Response
     */
    public function order(Order $order, Request $request)
    {
        return view('admin.payments.orders.index', ['order' => $order]);
    }

    /**
     * Store/Update order payment
     *
     * @param Order $order
     * @param Request $request
     * @return Response
     */
    public function postOrderPayment(Order $order, Request $request)
    {
        if ($order) {
            $payment = $order->payments()->create([ 
                'user_id' => $order->user_id, 
                'spedition' => $request->spedition,
                'services' => $request->services,
                'partial' => $request->partial,
                'additional' => $request->additional,
                'total' => $request->total,
            ]);

            return Redirect::route('admin.orders.show', $order->id);
        }

        return Redirect::route('admin.orders.index');
    }

    /**
     * Sent Payment Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalAccept($order = null, $payment = null)
    {
        $error = '';
        $model = '';
        $confirm_route =  route('admin.payments.orders.accept',['order' => $order, 'payment' => $payment]);
        return View('admin.layouts/modal_payment_confirmation', compact('error','model', 'confirm_route'));
    }

    /**
     * Sent payment order
     *
     * @param Order $order
     * @param Request $request
     * @return Response
     */
    public function orderPaymentAccept(Order $order, Payment $payment, Request $request)
    {
        if ($order && $payment) {
            //Update status payment
            $payment->update(['status' => 1]);

            $user = User::find($order->user_id);
            $data = ['user' => $user, 'order' => $order, 'link' => route('storage.orders.checkout', $order->id)];
            if ($user) {
                //Send Email
                Mail::queue('emails.payment', $data, function ($m) use ($user, $order) {
                    $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                    $m->subject('Payment Order ' . $order->id);
                });
            }

            //Update status order
            $order->update(['status' => 2]);
        }

        return Redirect::route('admin.orders.show', $order->id);
    }

    public function speditionPrice(Request $request)
    {
        $zip_code = $request->zip_code;
        $width = $request->width;
        $height = $request->height;
        $depth = $request->depth;
        $weight = $request->weight;

        if (!is_numeric($zip_code) || !is_numeric($width) || !is_numeric($width) || !is_numeric($height) || !is_numeric($depth)|| !is_numeric($weight)) {
            return response()->json([ 'price' => 0 ]);
        }

        $price = Helper::getSpeditionPrice($zip_code, $width , $height, $depth, $weight);

        return response()->json([ 'price' => ceil($price) ]);
    }

}
