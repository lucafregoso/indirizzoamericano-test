<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Order;
use App\OrderAddress;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Helpers\Helper;
use App\Package;
use Redirect;
use Datatables;
use App\Service;
use App\ServiceRequest;
use App\File;

class ServiceRequestController extends Controller
{
    public function index()
    {
        return view('admin.serviceRequests.index');
    }

    public function requestServicePackagesEdit(Package $package)
    {
        return view('admin.serviceRequests.package-edit', ['package' => $package]);
    }

    public function requestServiceOrdersEdit(Order $order)
    {
        return view('admin.serviceRequests.order-edit', ['order' => $order]);
    }

    public function requestServiceUpdate(Request $r)
    {

        if (count($r->requests)) {
            foreach ($r->requests as $k => $v) {
                $serviceRequest = ServiceRequest::find($k);
                if ($serviceRequest) {
                    $serviceRequest->update([
                        'status' => $v['status'] ?: 0,
                        'problem' => isset($v['problem']) ? $v['problem'] : 0,
                        'note' => $v['note'] ?: null,
                    ]);
                }

                //Service Photo
                if ($serviceRequest->service->slug == 'photo') {
                    $this->syncPhotoService($serviceRequest, $r);
                }

                //Extra address
                if ($serviceRequest->service->slug == 'extra_address') {
                    $this->extraAddressService($serviceRequest, $r);
                }
            }
        }

        return Redirect::back();
    }

    public function requestServicePackagesData(Request $request)
    {
        $packages = Package::whereHas('serviceRequests', function($q) {
                return $q->where('status', 0);
        })->with('serviceRequests.service')->get();

        return Datatables::of($packages)
            ->edit_column('reference',function(Package $packages) {
                return '<a href='. route('admin.packages.show', $packages->id) .' target="_blank">Package #'.$packages->uid.'</a>';
            })
            ->edit_column('created_at',function(Package $packages) {
                return $packages->created_at->diffForHumans();
            })
            ->edit_column('services',function(Package $packages) {
                $services = [];
                foreach ($packages->serviceRequests as $serviceRequest) {
                    $services[] = $serviceRequest->service->name;
                }
                return $services;
            })
            ->add_column('actions',function($packages) {
                $actions = '
                    <a href='. route('admin.serviceRequests.packages.edit', $packages->id) .' class="btn default"><i class="fa fa-edit"></i> Edit</a>
                ';
                return $actions;

            })->make(true);
    }

    public function requestServiceOrdersData(Request $request)
    {
        $orders = Order::whereHas('serviceRequests', function($q) {
                return $q->where('status', 0);
        })->with('serviceRequests.service')->get();

        return Datatables::of($orders)
            ->edit_column('reference',function(Order $orders) {
                return '<a href='. route('admin.orders.show', $orders->id) .' target="_blank">Order #'.$orders->uid.'</a>';
            })
            ->edit_column('created_at',function(Order $orders) {
                return $orders->created_at->diffForHumans();
            })
            ->edit_column('services',function(Order $orders) {
                $services = [];
                foreach ($orders->serviceRequests as $serviceRequest) {
                    $services[] = $serviceRequest->service->name;
                }
                return $services;
            })
            ->add_column('actions',function($orders) {
                $actions = '
                    <a href='. route('admin.serviceRequests.orders.edit', $orders->id) .' class="btn default"><i class="fa fa-edit"></i> Edit</a>
                ';
                return $actions;

            })->make(true);
    }

    public function syncPhotoService($serviceRequest, $r)
    {
        //Sync associate
        $photos_key = [];
        if (count($r->photos)) {
            foreach ($r->photos as $photo) {
                //dd($photo);
                if($photo['photo']['id'] != ""){
                    $photos_key[] = $photo['photo']['id'];
                }
            }
        }

        $serviceRequest->photos()->sync($photos_key);

        //Add.Update photo
        if (count($r->photos)) {
            foreach ($r->photos as $photo) {
                //New
                if($photo['photo']['id'] == ""){
                    if ($file = $photo['photo']['photo']) {
                        $filename         = md5(uniqid(microtime(), true));
                        $path             = Helper::getFilePath($filename);
                        $originalname     = $file->getClientOriginalName();
                        $extension        = $file->getClientOriginalExtension();
                        $mimetype         = $file->getClientMimeType();
                        $size             = $file->getClientSize();
                        $destination_path = public_path().'/uploads/files/'.Helper::getFilePath($filename);
                        $remote_url       = url('/').'/uploads/files/'.Helper::getFilePath($filename).$filename.'.'.$extension;

                        $save = $file->move($destination_path . '/', $filename . '.' . $extension);

                        if($save) {
                            $new_image = File::create(['originalname' => $originalname, 'mimetype' => $mimetype, 'filename' => $filename, 'size' => $size, 'extension' => $extension]);
                            if ($new_image) {
                                $image[] = $new_image->id;
                            }
                            $serviceRequest->photos()->attach($new_image);
                        }
                    }
                }
            }
        }
    }

    public function extraAddressService($serviceRequest, $r)
    {
        if (count($r->extra_address)) {
            $extra_address_data = [
                "address" => $r->extra_address['address'],
                "city" => $r->extra_address['city'],
                "zip_code" => $r->extra_address['zip_code'],
                "state" => $r->extra_address['state'],
                "service_request_id" => $serviceRequest->id
            ];

            if (count(array_filter($extra_address_data)) == count($extra_address_data)) {
                $extra_address = OrderAddress::create($extra_address_data);
            }
        }
    }
}