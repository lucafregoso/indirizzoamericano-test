<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreatePersonalShopperRequest;
use App\Http\Requests\UpdatePersonalShopperRequest;
use App\Repositories\PersonalShopperRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\PersonalShopper;
use App\PersonalShopperProduct;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Helpers\Helper;
use App\User;
use Datatables;
use Redirect;

class PersonalShopperController extends Controller
{
    /** @var  PersonalShopperRepository */
    private $personalShopperRepository;

    public function __construct(PersonalShopperRepository $personalShopperRepo)
    {
        parent::__construct();
        $this->personalShopperRepository = $personalShopperRepo;
    }

    /**
     * Display a listing of the PersonalShopper.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->personalShopperRepository->pushCriteria(new RequestCriteria($request));
        $personalShoppers = $this->personalShopperRepository->all();

        return view('admin.personalShoppers.index')
            ->with('personalShoppers', $personalShoppers);
    }

    /**
     * Show the form for creating a new PersonalShopper.
     *
     * @return Response
     */
    public function create()
    {
        $users = User::pluck('email', 'id');

        return view('admin.personalShoppers.create', ['users' => $users]);
    }

    /**
     * Store a newly created PersonalShopper in storage.
     *
     * @param CreatePersonalShopperRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonalShopperRequest $request)
    {
        $input = $request->all();

        $order = $this->personalShopperRepository->create([
            'reference' => $request->reference,
            'note' => $request->note,
            'user_id' => $request->user_id,
            'status' => 1
        ]);

        if ($order) {
            if($request->products) {
                foreach ($request->products as $product) {
                    $new_product = PersonalShopperProduct::create([
                        'personal_shopper_id' => $order->id,
                        'quantity' => $product['quantity'] != '' ? $product['quantity'] : null, 
                        'article_code' => $product['article_code'] != '' ? $product['article_code'] : null, 
                        'size' => $product['size'] != '' ? $product['size'] : null, 
                        'color' => $product['color'] != '' ? $product['color'] : null, 
                        'note' => $product['note'] != '' ? $product['note'] : null, 
                    ]);
                }
            }
        }

        Flash::success('PersonalShopper saved successfully.');

        return redirect(route('admin.personalShoppers.index'));
    }

    /**
     * Display the specified PersonalShopper.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $personalShopper = PersonalShopper::with('user', 'products')->find($id);

        if (empty($personalShopper)) {
            Flash::error('PersonalShopper not found');

            return redirect(route('personalShoppers.index'));
        }

        return view('admin.personalShoppers.show')->with('personalShopper', $personalShopper);
    }

    /**
     * Show the form for editing the specified PersonalShopper.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $personalShopper = PersonalShopper::with('products')->find($id);

        if (empty($personalShopper)) {
            Flash::error('PersonalShopper not found');

            return redirect(route('personalShoppers.index'));
        }

        $users = User::pluck('email', 'id');

        return view('admin.personalShoppers.edit', ['personalShopper' => $personalShopper, 'users' => $users]);
    }

    /**
     * Update the specified PersonalShopper in storage.
     *
     * @param  int              $id
     * @param UpdatePersonalShopperRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonalShopperRequest $request)
    {
        $personalShopper = $this->personalShopperRepository->findWithoutFail($id);

        if (empty($personalShopper)) {
            Flash::error('PersonalShopper not found');

            return redirect(route('personalShoppers.index'));
        }

        $order = $this->personalShopperRepository->update([
            'reference' => $request->reference,
            'note' => $request->note,
            'user_id' => $request->user_id,
        ], $id);

        //Sync associate
        $products_key = [];
        foreach ($request->products as $product) {
            if($product['product']['id'] != ""){
                $products_key[] = $product['product']['id'];
            }
        }
        if(count($products_key) > 0) {
            PersonalShopperProduct::where('personal_shopper_id', $order->id)->whereNotIn('id', $products_key)->delete();
        }

        //Add.Update Product
        foreach ($request->products as $product) {
            //New
            if($product['product']['id'] == ""){
                $new_product = PersonalShopperProduct::create([
                    'personal_shopper_id' => $order->id,
                    'quantity' =>       $product['product']['quantity'] != ''       ? $product['product']['quantity'] : null, 
                    'article_code' =>   $product['product']['article_code'] != ''   ? $product['product']['article_code'] : null, 
                    'size' =>           $product['product']['size'] != ''           ? $product['product']['size'] : null, 
                    'color' =>          $product['product']['color'] != ''           ? $product['product']['color'] : null, 
                    'note' =>           $product['product']['note'] != ''           ? $product['product']['note'] : null, 
                ]);
            }

            //Update
            if($product['product']['id'] != ""){
                $update_product = PersonalShopperProduct::where('id', $product['product']['id'])->update([
                    'personal_shopper_id' => $order->id,
                    'quantity' =>       $product['product']['quantity'] != ''       ? $product['product']['quantity'] : null, 
                    'article_code' =>   $product['product']['article_code'] != ''   ? $product['product']['article_code'] : null, 
                    'size' =>           $product['product']['size'] != ''           ? $product['product']['size'] : null, 
                    'color' =>          $product['product']['color'] != ''           ? $product['product']['color'] : null, 
                    'note' =>           $product['product']['note'] != ''           ? $product['product']['note'] : null, 
                ]);
            }
        }

        Flash::success('PersonalShopper updated successfully.');

        return redirect(route('admin.personalShoppers.index'));
    }

    /**
     * Remove the specified PersonalShopper from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.personalShoppers.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = PersonalShopper::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.personalShoppers.index'))->with('success', Lang::get('message.success.delete'));
       }

    /*
     * Pass data through ajax call
     */
    public function data(Request $request)
    {
        $orders = PersonalShopper::with('user');
        $orders = $orders->where('status', '!=', 0)->get();

        return Datatables::of($orders)
            ->edit_column('created_at',function(PersonalShopper $orders) {
                return $orders->created_at->diffForHumans();
            })
            ->edit_column('user_data',function(PersonalShopper $orders) {
                return '<a href='. route('admin.users.show', $orders->user->id) .' target="_blank">'.(isset($orders->user->uuic) ? $orders->user->uuic : '').' - '.$orders->user->first_name.' '.$orders->user->last_name.' - '.$orders->user->email.'</a>';
            })
            ->add_column('actions',function($orders) {
                $actions = '
                    <a href='. route('admin.personalShoppers.show', $orders->id) .'><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view order"></i></a>
                    <a href='. route('admin.personalShoppers.edit', $orders->id) .'><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update order"></i></a>
                    <a href='. route('admin.personalShoppers.confirm-delete', $orders->id) .' data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete order"></i></a>
                ';
                return $actions;

            })->make(true);
    }

    /**
     * Show a list of all the deleted items.
     *
     * @return View
     */
    public function getDeletedOrders()
    {
        // Grab deleted users
        $orders = PersonalShopper::onlyTrashed()->get();

        // Show the page
        return view('admin.personalShoppers.deleted_personalShoppers', compact('orders'));
    }

    /**
     * Restore a deleted user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function getRestore($id = null)
    {
        try {
            // Get user information
            $order = PersonalShopper::withTrashed()->find($id);

            // Restore the user
            $order->restore();

            // Prepare the success message
            $success = Lang::get('message.success.restored');

            // Redirect to the user management page
            return Redirect::route('admin.deleted_personalShoppers')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.deleted_personalShoppers')->with('error', $error);
        }
    }

     /**
     * Destroy Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalDestroy($id = null)
    {
        $error = '';
        $model = '';
        $confirm_route =  route('admin.personalShoppers.total-destroy',['id'=>$id]);
        return View('admin.layouts/modal_destroy_confirmation', compact('error','model', 'confirm_route'));
    }

    /**
     * Destroy the given user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy($id = null)
    {
        $order = PersonalShopper::withTrashed()->find($id);
        $order->forceDelete();

       // Redirect to the group management page
       return redirect(route('admin.personalShoppers.index'))->with('success', Lang::get('message.success.delete'));
    }

}
