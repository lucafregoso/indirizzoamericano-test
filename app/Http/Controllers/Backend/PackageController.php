<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreatePackageRequest;
use App\Http\Requests\UpdatePackageRequest;
use App\Repositories\PackageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Package;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use Datatables;
use Redirect;
use App\Service;
use App\ServiceRequest;
use App\Helpers\Helper;
use App\File;
use App\Order;

class PackageController extends Controller
{
    /** @var  PackageRepository */
    private $packageRepository;

    public function __construct(PackageRepository $packageRepo)
    {
        parent::__construct();
        $this->packageRepository = $packageRepo;
    }

    /**
     * Display a listing of the Package.
     *
     * @param Request $request
     * @return Response
     */
    public function indexServicePhoto(Request $request)
    {
        return view('admin.packages.index');
    }

    /**
     * Display a listing of the Package.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin.packages.index');
    }

    /**
     * Show the form for creating a new Package.
     *
     * @return Response
     */
    public function create()
    {
        $users = User::get();
        $services = Service::where('reference', 'packages')->get();

        return view('admin.packages.create', ['users' => $users, 'services' => $services]);
    }

    /**
     * Store a newly created Package in storage.
     *
     * @param CreatePackageRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageRequest $request)
    {
        $input = $request->all();

        $package = $this->packageRepository->create([
            'barcode' => $request->barcode ?: null,
            'description' => $request->description ?: null,
            'reseller' => $request->reseller ?: null,
            'broken' => $request->broken ?: 0,
            'problem' => $request->problem ?: 0,
            'user_id' => $request->user_id,
        ]);

        if ($package) {

            //Set Uid
            $client = User::find($request->user_id);
            $package->update(['uid' => $client->uuic.'-'.$package->id]);

            //Set service
            if (count($request->services)) {
                foreach ($request->services as $service) {
                    $package->serviceRequests()->create([ 'user_id' => $request->user_id, 'service_id' => $service]);
                }
            }

            //Check if user have service package_auto_send
            $haveServiceAutoSendPackage = $client->haveServiceAutoSendPackage();
            if ($haveServiceAutoSendPackage) {
                $order = Order::create([
                    'user_id' => $client->id,
                    'status' => 0,
                ]);

                if ($order) {
                    //Set order Uid
                    $order->update(['uid' => $client->uuic.'-'.$order->id]);
                    $package->update(['order_id' => $order->id]);
                }
            }
        }

        Flash::success('Package saved successfully.');

        return redirect(route('admin.packages.index'));
    }

    /**
     * Display the specified Package.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('Package not found');

            return redirect(route('packages.index'));
        }

        return view('admin.packages.show')->with('package', $package);
    }

    /**
     * Show the form for editing the specified Package.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('Package not found');

            return redirect(route('packages.index'));
        }

        $users = User::get();
        $services = Service::where('reference', 'packages')->get();
        $related_services = $package->serviceRequests()->pluck('service_id');

        return view('admin.packages.edit', ['package' => $package, 'users' => $users, 'services' => $services, 'related_services' => $related_services]);
    }

    /**
     * Update the specified Package in storage.
     *
     * @param  int              $id
     * @param UpdatePackageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageRequest $request)
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('Package not found');

            return redirect(route('packages.index'));
        }

        $package = $this->packageRepository->update([
            'barcode' => $request->barcode ?: null,
            'description' => $request->description ?: null,
            'reseller' => $request->reseller ?: null,
            'broken' => $request->broken ?: 0,
            'problem' => $request->problem ?: 0,
            'user_id' => $request->user_id,
        ], $id);

        if ($package) {

            //Set Uid
            $client = User::find($request->user_id);
            $package->update(['uid' => $client->uuic.'-'.$package->id]);

            //Set service
            if (count($request->services)) {

                //Delete services
                $ServiceRequest = $package->serviceRequests()->whereNotIn('service_id', $request->services)->delete();

                foreach ($request->services as $service) {

                    if (!$package->serviceRequests()->where('service_id', $service)->first()) {
                        $package->serviceRequests()->create([ 'user_id' => $request->user_id, 'service_id' => $service]);
                    }

                }
            }
        }

        Flash::success('Package updated successfully.');

        return Redirect::back();
    }

    /**
    * Remove the specified Package from storage.
    *
    * @param  int $id
    *
    * @return Response
    */
    public function getModalDelete($id = null)
    {
        $error = '';
        $model = '';
        $confirm_route =  route('admin.packages.delete',['id'=>$id]);
        return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));
    }

    public function getDelete($id = null)
    {
       $sample = Package::destroy($id);

       // Redirect to the group management page
       return redirect(route('admin.packages.index'))->with('success', Lang::get('message.success.delete'));
    }

    /*
     * Pass data through ajax call
     */
    public function data()
    {
        $packages = Package::with('user')->latest('id');

        return Datatables::of($packages)
            ->edit_column('services',function(Package $packages) {
                $services = [];
                foreach ($packages->serviceRequests as $serviceRequest) {
                    $services[] = '<a href='. route('admin.serviceRequests.packages.edit', $packages->id) .' target="_blank">'.$serviceRequest->service->name.'</a>';
                }
                return $services;
            })
            ->edit_column('created_at',function(Package $packages) {
                return $packages->created_at->diffForHumans();
            })
            ->edit_column('user_data',function(Package $packages) {
                return '<a href='. route('admin.users.show', $packages->user->id) .' target="_blank">'.(isset($packages->user->uuic) ? $packages->user->uuic : '').' - '.$packages->user->first_name.' '.$packages->user->last_name.' - '.$packages->user->email.'</a>';
            })
            ->add_column('actions',function($packages) {
                $actions = '
                    <a href='. route('admin.packages.show', $packages->id) .' class="btn default"><i class="fa fa-info"></i> View</a>
                    <a href='. route('admin.packages.edit', $packages->id) .' class="btn default"><i class="fa fa-edit"></i> Edit</a>
                    <a href='. route('admin.packages.confirm-delete', $packages->id) .' data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
                ';
                return $actions;

            })->make(true);
    }

    /**
     * Show a list of all the deleted packages.
     *
     * @return View
     */
    public function getDeletedPackages()
    {
        // Grab deleted users
        $packages = Package::onlyTrashed()->get();

        // Show the page
        return view('admin.packages.deleted_packages', compact('packages'));
    }

    /**
     * Restore a deleted user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function getRestore($id = null)
    {
        try {
            // Get user information
            $package = Package::withTrashed()->find($id);

            // Restore the user
            $package->restore();

            // Prepare the success message
            $success = Lang::get('users/message.success.restored');

            // Redirect to the user management page
            return Redirect::route('admin.deleted_packages')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.deleted_packages')->with('error', $error);
        }
    }

     /**
     * Destroy Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalDestroy($id = null)
    {
        $error = '';
        $model = '';
        $confirm_route =  route('admin.packages.total-destroy',['id'=>$id]);
        return View('admin.layouts/modal_destroy_confirmation', compact('error','model', 'confirm_route'));
    }

    /**
     * Destroy the given user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy($id = null)
    {
        $package = Package::withTrashed()->find($id);
        $package->forceDelete();

       // Redirect to the group management page
       return redirect(route('admin.packages.index'))->with('success', Lang::get('message.success.delete'));
    }

}
