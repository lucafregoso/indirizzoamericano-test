<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Order;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Helpers\Helper;
use App\Package;
use Redirect;
use Datatables;
use App\Service;

class OrderController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        parent::__construct();
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     * @return Response
     */
    public function indexOrderRequests(Request $request)
    {
        $status = Helper::getOrderStatus();

        return view('admin.orders.index', ['status' => $status]);
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $status = Helper::getOrderStatus();

        return view('admin.orders.index', ['status' => $status]);
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        $users = User::get();
        $packages = Package::whereNull('order_id')->get();
        $order_status = Helper::getOrderStatus();
        $services = Service::where('reference', 'orders')->get();

        return view('admin.orders.create', ['users' => $users, 'packages' => $packages, 'order_status' => $order_status, 'services' => $services]);
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->create([
            'tracking' => $request->tracking,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ]);

        //Set Uid
        $client = User::find($request->user_id);
        $order->update(['uid' => $client->uuic.'-'.$order->id]);

        foreach ($request->packages as $package_id) {
            $package = Package::where('id', $package_id)->first();
            if ($package) {
                $package->update(['order_id' => $order->id]);
            }
        }

        //Set service
        if (count($request->services)) {
            foreach ($request->services as $service) {
                $order->serviceRequests()->create([ 'user_id' => $request->user_id, 'service_id' => $service]);
            }
        }

        Flash::success('Order saved successfully.');

        return redirect(route('admin.orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('admin.orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $users = User::get();
        $packages = Package::whereNull('order_id')->get();
        $related_packages = $order->packages()->get();

        $merged_packages = $packages->merge($related_packages);

        $order_status = Helper::getOrderStatus();
        $services = Service::where('reference', 'orders')->get();
        $related_services = $order->serviceRequests()->pluck('service_id');

        return view('admin.orders.edit', ['users' => $users, 'packages' => $merged_packages, 'related_packages' => $related_packages, 'order_status' => $order_status, 'order' => $order, 'services' => $services, 'related_services' => $related_services]);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param  int              $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $order = $this->orderRepository->update([
            'tracking' => $request->tracking,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ], $id);

        //Set Uid
        $client = User::find($request->user_id);
        $order->update(['uid' => $client->uuic.'-'.$order->id]);

        $sync_packages = Package::where('order_id', $order->id)->update(['order_id' => null]);
        foreach ($request->packages as $package_id) {
            $package = Package::where('id', $package_id)->first();
            if ($package) {
                $package->update(['order_id' => $order->id]);
            }
        }

        //Set service
        if (count($request->services)) {

            //Delete services
            $ServiceRequest = $order->serviceRequests()->whereNotIn('service_id', $request->services)->delete();

            foreach ($request->services as $service) {

                if (!$order->serviceRequests()->where('service_id', $service)->first()) {
                    $order->serviceRequests()->create([ 'user_id' => $request->user_id, 'service_id' => $service]);
                }

            }
        }//End set service

        Flash::success('Order updated successfully.');

        return redirect(route('admin.orders.index'));
    }

    /**
    * Remove the specified Order from storage.
    *
    * @param  int $id
    *
    * @return Response
    */
    public function getModalDelete($id = null)
    {
        $error = '';
        $model = '';
        $confirm_route =  route('admin.orders.delete',['id'=>$id]);
        return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

    }

    public function getDelete($id = null)
    {
        $sample = Order::destroy($id);

        // Redirect to the group management page
        return redirect(route('admin.orders.index'))->with('success', Lang::get('message.success.delete'));
    }

    /*
     * Pass data through ajax call
     */
    public function data(Request $request)
    {
        $orders = Order::with('user');

        if ($request->get('status')) {
            $orders = $orders->where('status', $request->get('status'));
        }

        $orders = $orders->latest('id');

        return $this->makeDataTable($orders);
    }

    /*
     * Pass data through ajax call
     */
    public function dataOrdersNew(Request $request)
    {
        $orders = Order::with('user')->where('status', 0)->latest('id');

        return $this->makeDataTable($orders);
    }

    /*
     * Pass data through ajax call
     */
    public function dataOrdersPaid(Request $request)
    {
        $orders = Order::with('user')->where('status', 3)->latest('id');

        return $this->makeDataTable($orders);
    }

    /*
     * Pass data through ajax call
     */
    public function makeDataTable($orders)
    {
        return Datatables::of($orders)
            ->edit_column('services',function(Order $orders) {
                $services = [];
                foreach ($orders->serviceRequests as $serviceRequest) {
                    $services[] = '<a href='. route('admin.serviceRequests.orders.edit', $orders->id) .' target="_blank">'.$serviceRequest->service->name.'</a>';
                }
                return $services;
            })
            ->edit_column('created_at',function(Order $orders) {
                return $orders->created_at->diffForHumans();
            })
            ->edit_column('user_data',function(Order $orders) {
                return '<a href='. route('admin.users.show', $orders->user->id) .' target="_blank">'.(isset($orders->user->uuic) ? $orders->user->uuic : '').' - '.$orders->user->first_name.' '.$orders->user->last_name.' - '.$orders->user->email.'</a>';
            })
            ->edit_column('status',function(Order $orders) {
                return Helper::getOrderStatus($orders->status);
            })
            ->add_column('actions',function($orders) {
                $actions = '
                    <a href='. route('admin.orders.show', $orders->id) .' class="btn default"><i class="fa fa-info"></i> View</a>
                    <a href='. route('admin.orders.edit', $orders->id) .' class="btn default"><i class="fa fa-edit"></i> Edit</a>
                    <a href='. route('admin.orders.confirm-delete', $orders->id) .' data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
                    <a href='. route('admin.payments.orders.index', $orders->id) .' class="btn default"><i class="fa fa-money"></i> Payment</a>
                ';
                return $actions;

            })->make(true);
    }

    /**
     * Show a list of all the deleted packages.
     *
     * @return View
     */
    public function getDeletedOrders()
    {
        // Grab deleted users
        $orders = Order::onlyTrashed()->get();

        // Show the page
        return view('admin.orders.deleted_orders', compact('orders'));
    }

    /**
     * Restore a deleted user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function getRestore($id = null)
    {
        try {
            // Get user information
            $order = Order::withTrashed()->find($id);

            // Restore the user
            $order->restore();

            // Prepare the success message
            $success = Lang::get('users/message.success.restored');

            // Redirect to the user management page
            return Redirect::route('admin.deleted_orders')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.deleted_orders')->with('error', $error);
        }
    }

     /**
     * Destroy Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalDestroy($id = null)
    {
        $error = '';
        $model = '';
        $confirm_route =  route('admin.orders.total-destroy',['id'=>$id]);
        return View('admin.layouts/modal_destroy_confirmation', compact('error','model', 'confirm_route'));
    }

    /**
     * Destroy the given user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy($id = null)
    {
        $order = Order::withTrashed()->find($id);
        $packages = Package::where('order_id', $id)->update(['order_id' => null]);
        $order->forceDelete();

       // Redirect to the group management page
       return redirect(route('admin.orders.index'))->with('success', Lang::get('message.success.delete'));
    }

}
