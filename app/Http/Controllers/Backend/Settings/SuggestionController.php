<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Requests;
use App\Http\Requests\CreateSuggestionRequest;
use App\Http\Requests\UpdateSuggestionRequest;
use App\Repositories\SuggestionRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Suggestion;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\File;
use App\Helpers\Helper;

class SuggestionController extends Controller
{
    /** @var  SuggestionRepository */
    private $suggestionRepository;

    public function __construct(SuggestionRepository $suggestionRepo)
    {
        parent::__construct();
        $this->suggestionRepository = $suggestionRepo;
    }

    /**
     * Display a listing of the Suggestion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->suggestionRepository->pushCriteria(new RequestCriteria($request));
        $suggestions = $this->suggestionRepository->all();

        return view('admin.suggestions.index')
            ->with('suggestions', $suggestions);
    }

    /**
     * Show the form for creating a new Suggestion.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.suggestions.create');
    }

    /**
     * Store a newly created Suggestion in storage.
     *
     * @param CreateSuggestionRequest $request
     *
     * @return Response
     */
    public function store(CreateSuggestionRequest $request)
    {
        $input = $request->all();

        //Upload image
        if ($file = $request->file('image')) {
            $filename         = md5(uniqid(microtime(), true));
            $path             = Helper::getFilePath($filename);
            $originalname     = $file->getClientOriginalName();
            $extension        = $file->getClientOriginalExtension();
            $mimetype         = $file->getClientMimeType();
            $size             = $file->getClientSize();
            $destination_path = public_path().'/uploads/files/'.Helper::getFilePath($filename);
            $remote_url       = url('/').'/uploads/files/'.Helper::getFilePath($filename).$filename.'.'.$extension;

            $save = $file->move($destination_path . '/', $filename . '.' . $extension);

            if($save) {
                $image = File::create(['originalname' => $originalname, 'mimetype' => $mimetype, 'filename' => $filename, 'size' => $size, 'extension' => $extension]);
            }
        }

        $suggestion = $this->suggestionRepository->create([
            'name' => $request->name,
            'description' => $request->description,
            'image_id' => isset($image) ? $image->id : null,
            'url' => $request->url,
            'blank' => $request->blank
        ]);

        Flash::success('Suggestion saved successfully.');

        return redirect(route('admin.suggestions.index'));
    }

    /**
     * Display the specified Suggestion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suggestion = $this->suggestionRepository->findWithoutFail($id);

        if (empty($suggestion)) {
            Flash::error('Suggestion not found');

            return redirect(route('suggestions.index'));
        }

        return view('admin.suggestions.show')->with('suggestion', $suggestion);
    }

    /**
     * Show the form for editing the specified Suggestion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suggestion = $this->suggestionRepository->findWithoutFail($id);

        if (empty($suggestion)) {
            Flash::error('Suggestion not found');

            return redirect(route('suggestions.index'));
        }

        return view('admin.suggestions.edit')->with('suggestion', $suggestion);
    }

    /**
     * Update the specified Suggestion in storage.
     *
     * @param  int              $id
     * @param UpdateSuggestionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuggestionRequest $request)
    {
        $suggestion = $this->suggestionRepository->findWithoutFail($id);

        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'url' => $request->url,
            'blank' => $request->blank
        ];

        if ($file = $request->file('image')) {
            $filename         = md5(uniqid(microtime(), true));
            $path             = Helper::getFilePath($filename);
            $originalname     = $file->getClientOriginalName();
            $extension        = $file->getClientOriginalExtension();
            $mimetype         = $file->getClientMimeType();
            $size             = $file->getClientSize();
            $destination_path = public_path().'/uploads/files/'.Helper::getFilePath($filename);
            $remote_url       = url('/').'/uploads/files/'.Helper::getFilePath($filename).$filename.'.'.$extension;

            $save = $file->move($destination_path . '/', $filename . '.' . $extension);

            if($save) {
                $image = File::create(['originalname' => $originalname, 'mimetype' => $mimetype, 'filename' => $filename, 'size' => $size, 'extension' => $extension]);
                $data['image_id'] = $image->id;
            }
        }

        if($request->has('blank')){
           $request->merge(['blank' => 1]);
        }
        else{
          $request->merge(['blank' => 0]);
         }

        if (empty($suggestion)) {
            Flash::error('Suggestion not found');

            return redirect(route('suggestions.index'));
        }

        $suggestion = $this->suggestionRepository->update($data, $id);

        Flash::success('Suggestion updated successfully.');

        return redirect(route('admin.suggestions.index'));
    }

    /**
     * Remove the specified Suggestion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.suggestions.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Suggestion::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.suggestions.index'))->with('success', Lang::get('message.success.delete'));

       }

}
