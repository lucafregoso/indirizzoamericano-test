<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Requests;
use App\Http\Requests\CreateWarehouseRequest;
use App\Http\Requests\UpdateWarehouseRequest;
use App\Repositories\WarehouseRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Warehouse;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class WarehouseController extends Controller
{
    /** @var  WarehouseRepository */
    private $warehouseRepository;

    public function __construct(WarehouseRepository $warehouseRepo)
    {
        parent::__construct();
        $this->warehouseRepository = $warehouseRepo;
    }

    /**
     * Display a listing of the Warehouse.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->warehouseRepository->pushCriteria(new RequestCriteria($request));
        $warehouses = $this->warehouseRepository->all();

        return view('admin.warehouses.index')
            ->with('warehouses', $warehouses);
    }

    /**
     * Show the form for creating a new Warehouse.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.warehouses.create');
    }

    /**
     * Store a newly created Warehouse in storage.
     *
     * @param CreateWarehouseRequest $request
     *
     * @return Response
     */
    public function store(CreateWarehouseRequest $request)
    {
        $input = $request->all();

        $warehouse = $this->warehouseRepository->create($input);

        Flash::success('Warehouse saved successfully.');

        return redirect(route('admin.warehouses.index'));
    }

    /**
     * Display the specified Warehouse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $warehouse = $this->warehouseRepository->findWithoutFail($id);

        if (empty($warehouse)) {
            Flash::error('Warehouse not found');

            return redirect(route('warehouses.index'));
        }

        return view('admin.warehouses.show')->with('warehouse', $warehouse);
    }

    /**
     * Show the form for editing the specified Warehouse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $warehouse = $this->warehouseRepository->findWithoutFail($id);

        if (empty($warehouse)) {
            Flash::error('Warehouse not found');

            return redirect(route('warehouses.index'));
        }

        return view('admin.warehouses.edit')->with('warehouse', $warehouse);
    }

    /**
     * Update the specified Warehouse in storage.
     *
     * @param  int              $id
     * @param UpdateWarehouseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWarehouseRequest $request)
    {
        $warehouse = $this->warehouseRepository->findWithoutFail($id);

        

        if (empty($warehouse)) {
            Flash::error('Warehouse not found');

            return redirect(route('warehouses.index'));
        }

        $warehouse = $this->warehouseRepository->update($request->all(), $id);

        Flash::success('Warehouse updated successfully.');

        return redirect(route('admin.warehouses.index'));
    }

    /**
     * Remove the specified Warehouse from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.warehouses.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Warehouse::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.warehouses.index'))->with('success', Lang::get('message.success.delete'));

       }

}
