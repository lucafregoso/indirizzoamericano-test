<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Order;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Helpers\Helper;
use App\Package;
use Redirect;
use Datatables;
use App\Service;
use App\Notification;
use Event;
use App\Events\NotificationEvent;

class NotificationsController extends Controller
{

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin.notifications.index');
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        $users = User::get();

        return view('admin.notifications.create', ['users' => $users]);
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user    = User::find($request->get('user_id'));
        $content = $request->get('content');

        Event::fire(new NotificationEvent($user, $content));

        return redirect('admin/notifications')->with('success', Lang::get('message.success.create'));
    }

    /**
    * Remove the specified Order from storage.
    *
    * @param  int $id
    *
    * @return Response
    */
    public function getModalDelete($id = null)
    {
        $error = '';
        $model = '';
        $confirm_route =  route('admin.notifications.delete',['id'=>$id]);
        return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

    }

    public function getDelete($id = null)
    {
        $notification = Notification::destroy($id);

        // Redirect to the group management page
        return redirect(route('admin.notifications.index'))->with('success', Lang::get('message.success.delete'));
    }

    /*
     * Pass data through ajax call
     */
    public function data(Request $request)
    {
        $notifications = Notification::with('user');
        $notifications = $notifications->latest('id');

        return $this->makeDataTable($notifications);
    }

    /*
     * Pass data through ajax call
     */
    public function makeDataTable($notifications)
    {
        return Datatables::of($notifications)
                ->edit_column('user_data',function(Notification $notifications) {
                    return '<a href='. route('admin.users.show', $notifications->user->id) .' target="_blank">'.(isset($notifications->user->uuic) ? $notifications->user->uuic : '').' - '.$notifications->user->first_name.' '.$notifications->user->last_name.' - '.$notifications->user->email.'</a>';
                })
                ->edit_column('created_at',function(Notification $notifications) {
                    return $notifications->created_at->diffForHumans();
                })
                ->add_column('actions', '
                    <a href="{{ route(\'admin.notifications.confirm-delete\', $id) }}" data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>
                ')
                ->make(true);
    }

}
