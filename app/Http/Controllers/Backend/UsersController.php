<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\UserRequest;
use App\User;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use File;
use Hash;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Datatables;
use Validator;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;

class UsersController extends Controller
{
    protected $countries;

    public function __construct()
    {
        parent::__construct();
        $this->countries = Helper::getCountries();
    }

    /**
     * Show a list of all the users.
     *
     * @return View
     */
    public function index()
    {
        // Grab all the users
        $roles = Sentinel::getRoleRepository()->all();

        // Show the page
        return view('admin.users.index', compact('roles')); 
    }

    /*
     * Pass data through ajax call
     */
    public function data(Request $request)
    {
        $users = new User;

        if ($request->get('role')) {
            $users = $users->whereHas('roles', function($q) use ($request) {
                $q->where('slug', $request->get('role'));
            });
        }

        $users = $users->with('roles')->orderBy('id', 'desc')->get(['id', 'uuic', 'first_name', 'last_name', 'email','created_at']);

        $datatables = Datatables::of($users);
        $datatables->edit_column('created_at',function(User $user) {
                return $user->created_at->diffForHumans();
            })
            ->add_column('status',function($user){
                if($activation = Activation::completed($user))
                    return 'Activated';
                else
                    return 'Pending';
            })
            ->add_column('actions',function($user) {
                $actions = '<a href='. route('admin.users.show', $user->id) .' class="btn default"><i class="fa fa-info"></i> View</a>
                            <a href='. route('admin.users.edit', $user->id) .' class="btn default"><i class="fa fa-edit"></i> Edit</a>';

                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href='. route('admin.users.confirm-delete', $user->id) .' data-toggle="modal" data-target="#delete_confirm" class="btn default"><i class="fa fa-trash-o"></i> Delete</a>';
                }
                return $actions;
            });

        return $datatables->make(true);
    }

    /**
     * Create new user
     *
     * @return View
     */
    public function create()
    {
        // Get all the available groups
        $groups = Sentinel::getRoleRepository()->all();

        $countries = $this->countries;

        // Show the page
        return view('admin.users.create', compact('groups', 'countries'));
    }

    /**
     * User create form processing.
     *
     * @return Redirect
     */
    public function store(UserRequest $request)
    {
        //upload image
        if ($file = $request->file('pic_file')) {
            $fileName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = '/uploads/users/';
            $destinationPath = public_path() . $folderName;
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
            $request['pic'] = $safeName;
        }
        //check whether use should be activated by default or not
        $activate       = $request->get('activate') ? true : false;
        $pwd_generate   = str_random(8);

        try {

            $request_data = [
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'dob'           => !empty($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : null,
                'tax'           => !empty($request->tax) ? $request->tax : null,
                'phone'         => !empty($request->phone) ? $request->phone : null,
                'address'       => !empty($request->address) ? $request->address : null,
                'city'          => !empty($request->city) ? $request->city : null,
                'zip_code'      => !empty($request->zip_code) ? $request->zip_code : null,
                'state'         => !empty($request->state) ? $request->state : null,
                'email'         => $request->email_admin,
                'password'      => $pwd_generate,
                'uuic'          => Helper::generate_uuic(),
                'privacy'       => 1,
                'newsletter'    => $request->newsletter ? 1 : 0,
            ];

            // Register the user
            $user = Sentinel::register($request_data, $activate);

            //add user to 'User' group
            $role = Sentinel::findRoleById($request->get('group'));
            if ($role) {
                $role->users()->attach($user);
            }
            //check for activation and send activation mail if not activated by default
            if (!$request->get('activate')) {
                // Data to be used on the email view
                $data = array(
                    'user' => $user,
                    'password' => $pwd_generate,
                    'activationUrl' => URL::route('activate', [$user->id, Activation::create($user)->code]),
                );

                // Send the activation code through email
                Mail::send('emails.register-activate', $data, function ($m) use ($user) {
                    $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                    $m->subject('Welcome ' . $user->first_name);
                });
            } else {
                // Data to be used on the email view
                $data = array(
                    'user' => $user,
                    'password' => $pwd_generate,
                );

                // Send the activation code through email
                Mail::send('emails.activated', $data, function ($m) use ($user) {
                    $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                    $m->subject('Welcome ' . $user->first_name);
                });
            }

            // Redirect to the home page with success menu
            return Redirect::route('admin.users.index')->with('success', Lang::get('users/message.success.create'));

        } catch (LoginRequiredException $e) {
            $error = Lang::get('admin/users/message.user_login_required');
        } catch (PasswordRequiredException $e) {
            $error = Lang::get('admin/users/message.user_password_required');
        } catch (UserExistsException $e) {
            $error = Lang::get('admin/users/message.user_exists');
        }

        // Redirect to the user creation page
        return Redirect::back()->withInput()->with('error', $error);
    }

    /**
     * User update.
     *
     * @param  int $id
     * @return View
     */
    public function edit(User $user = null)
    {
        // Get this user groups
        $userRoles = $user->getRoles()->pluck('name', 'id')->all();

        // Get a list of all the available groups
        $roles = Sentinel::getRoleRepository()->all();

        $status = Activation::completed($user);

        $countries = $this->countries;

        // Show the page
        return view('admin.users.edit', compact('user', 'roles', 'userRoles', 'countries', 'status'));
    }

    /**
     * User update form processing page.
     *
     * @param  User $user
     * @param UserRequest $request
     * @return Redirect
     */
    public function update(User $user, UserRequest $request)
    {

        try {

            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email_admin');
            $user->dob = !empty($request->dob) ? Carbon::parse($request->dob)->format('Y-m-d') : null;
            $user->address = !empty($request->address) ? $request->address : null;
            $user->city = !empty($request->city) ? $request->city : null;
            $user->zip_code = !empty($request->zip_code) ? $request->zip_code : null;
            $user->state = !empty($request->state) ? $request->state : null;
            $user->phone = !empty($request->phone) ? $request->phone : null;
            $user->tax = !empty($request->tax) ? $request->tax : null;
            $user->newsletter = $request->newsletter ? $request->newsletter : 0;

            if ($password = $request->has('password')) {
                $user->password = Hash::make($request->password);
            }

            //save record
            $user->save();

            // Get the current user groups
            $userRoles = $user->roles()->pluck('id')->all();

            // Get the selected groups
            $selectedRoles = $request->get('groups', array());

            // Groups comparison between the groups the user currently
            // have and the groups the user wish to have.
            $rolesToAdd = array_diff($selectedRoles, $userRoles);
            $rolesToRemove = array_diff($userRoles, $selectedRoles);

            // Assign the user to groups
            foreach ($rolesToAdd as $roleId) {
                $role = Sentinel::findRoleById($roleId);

                $role->users()->attach($user);
            }

            // Remove the user from groups
            foreach ($rolesToRemove as $roleId) {
                $role = Sentinel::findRoleById($roleId);

                $role->users()->detach($user);
            }

            // Activate / De-activate user
            // $status = $activation = Activation::completed($user);
            // if ($request->get('activate') != $status) {
            //     if ($request->get('activate')) {
            //         $activation = Activation::exists($user);
            //         if ($activation) {
            //             Activation::complete($user, $activation->code);
            //         }
            //     } else {
            //         //remove existing activation record
            //         Activation::remove($user);
            //         //add new record
            //         Activation::create($user);

            //         //send activation mail
            //         $data = array(
            //             'user' => $user,
            //             'activationUrl' => URL::route('activate', $user->id, Activation::exists($user)->code),
            //         );

            //         // Send the activation code through email
            //         Mail::send('emails.register-activate', $data, function ($m) use ($user) {
            //             $m->to($user->email, $user->first_name . ' ' . $user->last_name);
            //             $m->subject('Welcome ' . $user->first_name);
            //         });

            //     }
            // }

            // Was the user updated?
            if ($user->save()) {
                // Prepare the success message
                $success = Lang::get('users/message.success.update');

                // Redirect to the user page
                return Redirect::route('admin.users.edit', $user)->with('success', $success);
            }

            // Prepare the error message
            $error = Lang::get('users/message.error.update');
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('user'));
            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }

        // Redirect to the user page
        return Redirect::route('admin.users.edit', $user)->withInput()->with('error', $error);
    }

    /**
     * Show a list of all the deleted users.
     *
     * @return View
     */
    public function getDeletedUsers()
    {
        // Grab deleted users
        $users = User::onlyTrashed()->get();

        // Show the page
        return view('admin.users.deleted_users', compact('users'));
    }


    /**
     * Delete Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalDelete($id = null)
    {
        $model = 'users';
        $confirm_route = $error = null;
        try {
            // Get user information
            $user = Sentinel::findById($id);

            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = Lang::get('users/message.error.delete');

                return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
            }
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
        $confirm_route = route('admin.users.delete', ['id' => $user->id]);
        return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    /**
     * Delete the given user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function delete($id = null)
    {
        try {
            // Get user information
            $user = Sentinel::findById($id);

            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = Lang::get('admin/users/message.error.delete');

                // Redirect to the user management page
                return Redirect::route('admin.users.index')->with('error', $error);
            }

            // Delete the user
            //to allow soft deleted, we are performing query on users model instead of Sentinel model
            //$user->delete();
            User::destroy($id);

            // Prepare the success message
            $success = Lang::get('users/message.success.delete');

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('admin/users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }
    }

    /**
     * Destroy Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalDestroy($id = null)
    {
        $model = 'users';
        $confirm_route = $error = null;

        try {
            // Get user information
            $user = User::withTrashed()->where('id', $id)->first();

            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = Lang::get('users/message.error.delete');

                return view('admin.layouts.modal_destroy_confirmation', compact('error', 'model', 'confirm_route'));
            }
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));
            return view('admin.layouts.modal_destroy_confirmation', compact('error', 'model', 'confirm_route'));
        }
        $confirm_route = route('admin.users.total-destroy', ['id' => $user->id]);
        return view('admin.layouts.modal_destroy_confirmation', compact('error', 'model', 'confirm_route'));
    }

    /**
     * Destroy the given user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy($id = null)
    {
        try {
            // Get user information
            $user = User::withTrashed()->where('id', $id)->first();

            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = Lang::get('admin/users/message.error.delete');

                // Redirect to the user management page
                return Redirect::route('admin.deleted_users')->with('error', $error);
            }

            // Delete the user
            //to allow soft deleted, we are performing query on users model instead of Sentinel model
            //$user->delete();
            $user->forceDelete();

            // Prepare the success message
            $success = Lang::get('users/message.success.delete');

            // Redirect to the user management page
            return Redirect::route('admin.deleted_users')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('admin/users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.deleted_users')->with('error', $error);
        }
    }

    /**
     * Restore a deleted user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function getRestore($id = null)
    {
        try {
            // Get user information
            $user = User::withTrashed()->find($id);

            // Restore the user
            $user->restore();

            // Prepare the success message
            $success = Lang::get('users/message.success.restored');

            // Redirect to the user management page
            return Redirect::route('admin.deleted_users')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.deleted_users')->with('error', $error);
        }
    }

    /**
     * Display specified user profile.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            // Get the user information
            $user = Sentinel::findUserById($id);

            //get country name
            if ($user->country) {
                $user->country = $this->countries[$user->country];
            }

        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }

        // Show the page
        return view('admin.users.show', compact('user'));

    }

    public function passwordreset($id, Request $request)
    {
        $data = $request->all();
        $user = Sentinel::findUserById($id);
        $password = $request->get('password');
        $user->password = Hash::make($password);
        $user->save();
    }

    public function lockscreen($id){
        $user = Sentinel::findUserById($id);
        return view('admin.lockscreen',compact('user'));
    }

    public function postLockscreen(Request $request){
        $password = Sentinel::getUser()->password;
        if(Hash::check($request->password,$password)){
            return 'success';
        }
        else{
            return 'error';
        }
    }



}
