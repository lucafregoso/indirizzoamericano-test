<?php

namespace App\Http\Controllers\Backend\Logistics;

use App\Http\Requests;
use App\Http\Requests\CreatePackageRequest;
use App\Http\Requests\UpdatePackageRequest;
use App\Repositories\PackageRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Package;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use Datatables;
use Redirect;
use App\Service;
use App\ServiceRequest;
use App\Helpers\Helper;
use App\File;
use App\Order;
use App\Logistics\DHL\DHLPrice;
use Validator;

class DHLController extends Controller
{
    /**
     * Show the form for editing the specified Package.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit()
    {
        $prices = DHLPrice::get();
        return view('admin.logistics.dhl.price-edit', ['prices' => $prices]);
    }

    /**
     * Update the specified Package in storage.
     *
     * @param  int              $id
     * @param UpdatePackageRequest $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        // Declare the rules for the form validation
        $rules = [
            'dhl.*.price'                    => 'required|numeric',
            'dhl.*.weight'                    => 'required|numeric',
        ];

        // Create a new validator instance from our validation rules
        $validator = Validator::make($request->all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        foreach ($request->dhl as $k => $v) {
            DHLPrice::where('id', $k)->update($v);
        }

        Flash::success('Informations updated successfully.');

        return Redirect::back();
    }


}
