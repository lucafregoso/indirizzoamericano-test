<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PaymentTransaction extends Model
{
    use SoftDeletes;

    public $table = 'payment_transactions';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'user_id',
        'payment_id',
        'paypal_id',
        'invoice_number',
        'payment_method',
        'state',
        'total',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'payment_id' => 'integer',
        'paypal_id' => 'string',
        'invoice_number' => 'string',
        'payment_method' => 'string',
        'state' => 'string',
        'total' => 'float',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //
    ];

}
