<?php namespace App;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends EloquentUser {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes to be fillable from the model.
     *
     * A dirty hack to allow fields to be fillable by calling empty fillable array
     *
     * @var array
     */
    protected $fillable = [];
    protected $guarded = [];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
    * To allow soft deletes
    */
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    //Scope Roles
    public function scopeHaveRole($query, $role = null)
    {
        if ($role) {
            return $query->whereHas('roles', function($q) use ($role){
                        $q->where('slug', $role);
                    });
        }
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'user_id', 'id');
    }

    public function packages()
    {
        return $this->hasMany('App\Package', 'user_id', 'id');
    }

    public function serviceRequests()
    {
        return $this->morphMany('App\ServiceRequest', 'service_requests' ,'type','ref_id');
    }

    function haveServiceAutoSendPackage() {
        return (bool) $this->serviceRequests()->where('status', 1)->whereHas('service', function($q) {
            $q->where('slug','=','package_auto_send');
        })->count();
    }
}
