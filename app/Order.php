<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'uid',
        'tracking',
        'user_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'uid' => 'string',
        'tracking' => 'string',
        'user_id' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //
    ];

    public function packages()
    {
        return $this->hasMany('App\Package');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function serviceRequests()
    {
        return $this->morphMany('App\ServiceRequest', 'service_requests' ,'type','ref_id');
    }

    public function payments()
    {
        return $this->morphMany('App\Payment', 'payments' ,'type','ref_id');
    }
}
