<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class PersonalShopper extends Model
{
    use SoftDeletes;

    public $table = 'personal_shopper';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'reference',
        'user_id',
        'note',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'reference' => 'string',
        'user_id' => 'integer',
        'note' => 'string',
        'status' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function products()
    {
        return $this->hasMany('App\PersonalShopperProduct');
    }
}
