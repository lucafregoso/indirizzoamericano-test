<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ServiceRequest extends Model
{
    use SoftDeletes;

    public $table = 'service_requests';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'ref_id',
        'type',
        'service_id',
        'status',
        'problem',
        'data',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'string',
        'ref_id' => 'string',
        'type' => 'string',
        'service_id' => 'integer',
        'status' => 'boolean',
        'problem' => 'boolean',
        'data' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //
    ];


    public function service()
    {
        return $this->hasOne('App\Service', 'id', 'service_id');
    }

    public function photos()
    {
        return $this->belongsToMany('App\File', 'request_files', 'request_id', 'file_id');
    }

    public function extra_address()
    {
        return $this->hasOne('App\OrderAddress', 'service_request_id', 'id');
    }
}
