<?php
namespace App\Helpers;

use App\Logistics\DHL\DHLPrice;

trait Spedition
{
    public static function getSpeditionPrice($zip_code = null, $width = null, $height = null, $depth = null, $weight = null)
    {
        if ($width  === null || $height === null  || $depth === null || $weight === null) {
            return 0;
        }

        $volumetric_price = self::getVolumetricPrice($width, $height, $depth);
        $weight_price     = self::getWeightPrice($weight);

        return ($volumetric_price > $weight_price) ? $volumetric_price : $weight_price;
    }

    private static function getVolumetricPrice($width, $height, $depth)
    {
        $total = ($width * $height * $depth) / 139;

        return $total;
    }

    private static function getWeightPrice($_weight)
    {
        $weight = intval($_weight);
        $max = DHLPrice::orderBy('id', 'desc')->first();
        $extra_additional = config('dhl.extra_weight_additional');

        if ($weight <= $max->weight) {
            $check_weight = DHLPrice::where('weight', '>=', $weight)->first();
            if ($check_weight) {
                return $check_weight->price;
            }

            return 0;
        }

        $delta = $weight - $max->weight;
        $delta_additional = 0;
        for ($i=1; $i <= $delta; $i++) { 
            $delta_additional += $extra_additional;
        }

        $total = $max->price + $delta_additional;

        return $total;
    }
}