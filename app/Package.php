<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Package extends Model
{
    use SoftDeletes;

    public $table    = 'packages';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'uid',
        'barcode',
        'description',
        'reseller',
        'value',
        'price',
        'status',
        'user_id',
        'order_id',
        'broken',
        'problem',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'uid' => 'string',
        'barcode' => 'string',
        'description' => 'string',
        'reseller' => 'string',
        'value' => 'float',
        'price' => 'float',
        'user_id' => 'integer',
        'order_id' => 'integer',
        'broken' => 'boolean',
        'problem' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    function haveService($relation_name, $id) {
        return (bool) $this->$relation_name()->where('id','=',$id)->count();
    }

    function haveServicePhoto() {
        return (bool) $this->serviceRequests()->where('status', 1)->whereHas('service', function($q) {
            $q->where('slug','=','photo');
        })->count();
    }

    function servicePhoto() {
        return $this->serviceRequests()->where('status', 1)->whereHas('service', function($q) {
            $q->where('slug','=','photo');
        })->with('photos')->get();
    }

    public function serviceRequests()
    {
        return $this->morphMany('App\ServiceRequest', 'service_requests' ,'type','ref_id');
    }
}
