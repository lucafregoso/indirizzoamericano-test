<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Suggestion extends Model
{
    use SoftDeletes;

    public $table = 'suggestions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'image_id',
        'url',
        'blank'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'image_id' => 'integer',
        'url' => 'string',
        'blank' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function image()
    {
        return $this->hasOne('App\File', 'id', 'image_id');
    }
}
