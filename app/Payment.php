<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Payment extends Model
{
    use SoftDeletes;

    public $table = 'payments';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'user_id',
        'ref_id',
        'type',
        'spedition',
        'services',
        'partial',
        'additional',
        'total',
        'paid',
        'status',
        'note',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'string',
        'ref_id' => 'string',
        'type' => 'string',
        'spedition' => 'float',
        'services' => 'float',
        'partial' => 'float',
        'additional' => 'float',
        'total' => 'float',
        'paid' => 'boolean',
        'status' => 'boolean',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //
    ];

    /**
     * Get all of the order that are assigned this tag.
     */
    public function order()
    {
        return $this->morphTo(null,'type','ref_id');
    }


}
