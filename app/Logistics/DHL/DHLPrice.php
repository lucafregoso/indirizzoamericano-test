<?php
namespace App\Logistics\DHL;

use Illuminate\Database\Eloquent\Model;

class DHLPrice extends Model
{
    public $table = 'dhl_price';

    public $fillable = [
        'weight',
        'price',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'weight' => 'integer',
        'price' => 'float',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //
    ];
}
