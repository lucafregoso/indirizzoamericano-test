<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    public $table = 'order_addresses';

    public $fillable = [
        'address',
        'city',
        'zip_code',
        'state',
        'service_request_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'address' => 'string',
        'city' => 'string',
        'zip_code' => 'string',
        'state' => 'string',
        'service_request_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function serviceRequest()
    {
        return $this->hasOne('App\Order', 'service_request_id', 'id');
    }
}
