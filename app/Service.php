<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Service extends Model
{
    use SoftDeletes;

    public $table = 'services';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug',
        'description',
        'image_id',
        'price',
        'status',
        'locale',
        'reference'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'image_id' => 'integer',
        'price' => 'float',
        'status' => 'boolean',
        'locale' => 'string',
        'reference' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function image()
    {
        return $this->hasOne('App\File', 'id', 'image_id');
    }
}
