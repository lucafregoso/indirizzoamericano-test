<?php
/**
 * Model binding into route
 */
Route::model('users', 'App\User');
Route::pattern('slug', '[a-z0-9- _]+');

/*
|
| Admin Routes
|
*/
Route::group(array('prefix' => 'admin'), function () {

    # Error pages should be shown without requiring login
    Route::get('404', function () {
        return View('admin/404');
    });
    Route::get('500', function () {
        return View::make('admin/500');
    });

    # All basic routes defined here
    Route::get('signin', array('as' => 'signin', 'uses' => 'Backend\AuthController@getSignin'));
    Route::post('signin', 'Backend\AuthController@postSignin');
    Route::get('logout', array('as' => 'logout', 'uses' => 'Backend\AuthController@getLogout'));

    Route::group(['middleware' => 'SentinelAdmin', 'as' => 'admin.'], function () {
        # Dashboard / Index
        Route::get('/', array('as' => 'dashboard','uses' => 'JoshController@showHome'));

        # User Management
        Route::group(array('prefix' => 'users'), function () {
            Route::get('/', array('as' => 'users', 'uses' => 'Backend\UsersController@index'));
            Route::get('data',['as' => 'users.data', 'uses' =>'Backend\UsersController@data']);
            Route::get('create', 'Backend\UsersController@create');
            Route::post('create', 'Backend\UsersController@store');
            Route::get('{user}', array('as' => 'users.show', 'uses' => 'Backend\UsersController@show'));
            Route::get('{user}/destroy', array('as' => 'users.total-destroy', 'uses' => 'Backend\UsersController@destroy'));
            Route::get('{user}/delete', array('as' => 'users.delete', 'uses' => 'Backend\UsersController@delete'));
            Route::get('{user}/confirm-delete', array('as' => 'users.confirm-delete', 'uses' => 'Backend\UsersController@getModalDelete'));
            Route::get('{user}/confirm-destroy', array('as' => 'users.confirm-destroy', 'uses' => 'Backend\UsersController@getModalDestroy'));
            Route::get('{user}/restore', array('as' => 'restore/user', 'uses' => 'Backend\UsersController@getRestore'));
            Route::post('{user}/passwordreset', array('as' => 'passwordreset', 'uses' => 'Backend\UsersController@passwordreset'));
        });
        Route::resource('users', 'Backend\UsersController');
        Route::get('deleted_users',array('as' => 'deleted_users','before' => 'Sentinel', 'uses' => 'Backend\UsersController@getDeletedUsers'));

        # Group Management
        Route::group(array('prefix' => 'groups'), function () {
            Route::get('/', array('as' => 'groups', 'uses' => 'Backend\GroupsController@index'));
            Route::get('create', array('as' => 'groups.create', 'uses' => 'Backend\GroupsController@create'));
            Route::post('create', 'Backend\GroupsController@store');
            Route::get('{group}/edit', array('as' => 'groups.edit', 'uses' => 'Backend\GroupsController@edit'));
            Route::post('{group}/edit', 'Backend\GroupsController@update');
            Route::get('{group}/delete', array('as' => 'groups.delete', 'uses' => 'Backend\GroupsController@destroy'));
            Route::get('{group}/confirm-delete', array('as' => 'groups.confirm-delete', 'uses' => 'Backend\GroupsController@getModalDelete'));
            Route::get('{group}/restore', array('as' => 'groups.restore', 'uses' => 'Backend\GroupsController@getRestore'));
        });

        # Packages Management
        Route::get('packages', ['as'=> 'packages.index', 'uses' => 'Backend\PackageController@index']);
        Route::get('packages/data',['as' => 'packages.data', 'uses' =>'Backend\PackageController@data']);
        Route::get('packages/photo/data',['as' => 'packages.photo.data', 'uses' =>'Backend\PackageController@photoData']);
        Route::post('packages', ['as'=> 'packages.store', 'uses' => 'Backend\PackageController@store']);
        Route::get('packages/create', ['as'=> 'packages.create', 'uses' => 'Backend\PackageController@create']);
        Route::get('packages/service/photo',['as' => 'packages.service.photo', 'uses' =>'Backend\PackageController@indexServicePhoto']);
        Route::put('packages/{packages}', ['as'=> 'packages.update', 'uses' => 'Backend\PackageController@update']);
        Route::patch('packages/{packages}', ['as'=> 'packages.update', 'uses' => 'Backend\PackageController@update']);
        Route::get('packages/{id}/delete', array('as' => 'packages.delete', 'uses' => 'Backend\PackageController@getDelete'));
        Route::get('packages/{id}/destroy', array('as' => 'packages.total-destroy', 'uses' => 'Backend\PackageController@destroy'));
        Route::get('packages/{id}/confirm-delete', array('as' => 'packages.confirm-delete', 'uses' => 'Backend\PackageController@getModalDelete'));
        Route::get('packages/{id}/confirm-destroy', array('as' => 'packages.confirm-destroy', 'uses' => 'Backend\PackageController@getModalDestroy'));
        Route::get('packages/{id}/restore', array('as' => 'restore/package', 'uses' => 'Backend\PackageController@getRestore'));
        Route::get('packages/{packages}', ['as'=> 'packages.show', 'uses' => 'Backend\PackageController@show']);
        Route::get('packages/{packages}/edit', ['as'=> 'packages.edit', 'uses' => 'Backend\PackageController@edit']);
        Route::get('deleted_packages',array('as' => 'deleted_packages','before' => 'Sentinel', 'uses' => 'Backend\PackageController@getDeletedPackages'));

        # Orders Management
        Route::get('orders', ['as'=> 'orders.index', 'uses' => 'Backend\OrderController@index']);
        Route::get('orders/lists/new',['as' => 'orders.lists.new', 'uses' =>'Backend\OrderController@index']);
        Route::get('orders/lists/new/data',['as' => 'orders.lists.new.data', 'uses' =>'Backend\OrderController@dataOrdersNew']);
        Route::get('orders/lists/paid',['as' => 'orders.lists.paid', 'uses' =>'Backend\OrderController@index']);
        Route::get('orders/lists/paid/data',['as' => 'orders.lists.paid.data', 'uses' =>'Backend\OrderController@dataOrdersPaid']);
        Route::get('orders/data',['as' => 'orders.data', 'uses' =>'Backend\OrderController@data']);
        Route::get('orders/requests/data',['as' => 'orders.requests.data', 'uses' =>'Backend\OrderController@requestsData']);
        Route::post('orders', ['as'=> 'orders.store', 'uses' => 'Backend\OrderController@store']);
        Route::get('orders/requests', ['as'=> 'orders.request', 'uses' => 'Backend\OrderController@indexOrderRequests']);
        Route::get('orders/create', ['as'=> 'orders.create', 'uses' => 'Backend\OrderController@create']);
        Route::put('orders/{orders}', ['as'=> 'orders.update', 'uses' => 'Backend\OrderController@update']);
        Route::patch('orders/{orders}', ['as'=> 'orders.update', 'uses' => 'Backend\OrderController@update']);
        Route::get('orders/{id}/delete', array('as' => 'orders.delete', 'uses' => 'Backend\OrderController@getDelete'));
        Route::get('orders/{id}/destroy', array('as' => 'orders.total-destroy', 'uses' => 'Backend\OrderController@destroy'));
        Route::get('orders/{id}/confirm-delete', array('as' => 'orders.confirm-delete', 'uses' => 'Backend\OrderController@getModalDelete'));
        Route::get('orders/{id}/confirm-destroy', array('as' => 'orders.confirm-destroy', 'uses' => 'Backend\OrderController@getModalDestroy'));
        Route::get('orders/{id}/restore', array('as' => 'restore/order', 'uses' => 'Backend\OrderController@getRestore'));
        Route::get('orders/{orders}', ['as'=> 'orders.show', 'uses' => 'Backend\OrderController@show']);
        Route::get('orders/{orders}/edit', ['as'=> 'orders.edit', 'uses' => 'Backend\OrderController@edit']);
        Route::get('deleted_orders',array('as' => 'deleted_orders','before' => 'Sentinel', 'uses' => 'Backend\OrderController@getDeletedOrders'));

        # Personal Shopper Management
        Route::get('personalShoppers', ['as'=> 'personalShoppers.index', 'uses' => 'Backend\PersonalShopperController@index']);
        Route::get('personalShoppers/data',['as' => 'personalShoppers.data', 'uses' =>'Backend\PersonalShopperController@data']);
        Route::post('personalShoppers', ['as'=> 'personalShoppers.store', 'uses' => 'Backend\PersonalShopperController@store']);
        Route::get('personalShoppers/create', ['as'=> 'personalShoppers.create', 'uses' => 'Backend\PersonalShopperController@create']);
        Route::put('personalShoppers/{personalShoppers}', ['as'=> 'personalShoppers.update', 'uses' => 'Backend\PersonalShopperController@update']);
        Route::patch('personalShoppers/{personalShoppers}', ['as'=> 'personalShoppers.update', 'uses' => 'Backend\PersonalShopperController@update']);
        Route::get('personalShoppers/{id}/delete', array('as' => 'personalShoppers.delete', 'uses' => 'Backend\PersonalShopperController@getDelete'));
        Route::get('personalShoppers/{id}/destroy', array('as' => 'personalShoppers.total-destroy', 'uses' => 'Backend\PersonalShopperController@destroy'));
        Route::get('personalShoppers/{id}/confirm-delete', array('as' => 'personalShoppers.confirm-delete', 'uses' => 'Backend\PersonalShopperController@getModalDelete'));
        Route::get('personalShoppers/{id}/confirm-destroy', array('as' => 'personalShoppers.confirm-destroy', 'uses' => 'Backend\PersonalShopperController@getModalDestroy'));
        Route::get('personalShoppers/{id}/restore', array('as' => 'restore/personalShopper', 'uses' => 'Backend\PersonalShopperController@getRestore'));
        Route::get('personalShoppers/{personalShoppers}', ['as'=> 'personalShoppers.show', 'uses' => 'Backend\PersonalShopperController@show']);
        Route::get('personalShoppers/{personalShoppers}/edit', ['as'=> 'personalShoppers.edit', 'uses' => 'Backend\PersonalShopperController@edit']);
        Route::get('deleted_personalShoppers',array('as' => 'deleted_personalShoppers','before' => 'Sentinel', 'uses' => 'Backend\PersonalShopperController@getDeletedOrders'));

        # Service Request
        Route::get('serviceRequests/packages', ['as'=> 'serviceRequests.packages.index', 'uses' => 'Backend\ServiceRequestController@index']);
        Route::get('serviceRequests/packages/data',['as' => 'serviceRequests.packages.data', 'uses' =>'Backend\ServiceRequestController@requestServicePackagesData']);
        Route::get('serviceRequests/orders', ['as'=> 'serviceRequests.orders.index', 'uses' => 'Backend\ServiceRequestController@index']);
        Route::get('serviceRequests/orders/data',['as' => 'serviceRequests.orders.data', 'uses' =>'Backend\ServiceRequestController@requestServiceOrdersData']);
        Route::get('serviceRequests/packages/{package}/edit',['as' => 'serviceRequests.packages.edit', 'uses' =>'Backend\ServiceRequestController@requestServicePackagesEdit']);
        Route::get('serviceRequests/orders/{order}/edit',['as' => 'serviceRequests.orders.edit', 'uses' =>'Backend\ServiceRequestController@requestServiceOrdersEdit']);
        Route::post('serviceRequests/update',['as' => 'serviceRequests.update', 'uses' =>'Backend\ServiceRequestController@requestServiceUpdate']);


        # Payment System
        Route::get('payments/orders/{order}', ['as'=> 'payments.orders.index', 'uses' => 'Backend\PaymentController@order']);
        Route::post('payments/orders/{order}', ['as'=> 'payments.orders.store', 'uses' => 'Backend\PaymentController@postOrderPayment']);
        Route::get('payments/orders/{order}/accept/{payment}', ['as'=> 'payments.orders.accept', 'uses' => 'Backend\PaymentController@orderPaymentAccept']);
        Route::get('payments/orders/{order}/accept/{payment}/confirm-accept', ['as' => 'payments.orders.confirm-accept', 'uses' => 'Backend\PaymentController@getModalAccept']);

        #Notifications
        Route::get('notifications/data',['as' => 'notifications.data', 'uses' =>'Backend\NotificationsController@data']);
        Route::get('notifications/{id}/delete', array('as' => 'notifications.delete', 'uses' => 'Backend\NotificationsController@getDelete'));
        Route::get('notifications/{id}/confirm-delete', array('as' => 'notifications.confirm-delete', 'uses' => 'Backend\NotificationsController@getModalDelete'));
        Route::resource('notifications', 'Backend\NotificationsController');

        #Newsletter
        Route::get('newsletters',['as' => 'newsletters', 'uses' =>'Backend\NewslettersController@export']);

        #############
        #Settings
        #############
        Route::group(array('prefix' => 'settings'), function () {
            #Warehouses
            Route::get('warehouses', ['as'=> 'warehouses.index', 'uses' => 'Backend\Settings\WarehouseController@index']);
            Route::post('warehouses', ['as'=> 'warehouses.store', 'uses' => 'Backend\Settings\WarehouseController@store']);
            Route::get('warehouses/create', ['as'=> 'warehouses.create', 'uses' => 'Backend\Settings\WarehouseController@create']);
            Route::put('warehouses/{warehouses}', ['as'=> 'warehouses.update', 'uses' => 'Backend\Settings\WarehouseController@update']);
            Route::patch('warehouses/{warehouses}', ['as'=> 'warehouses.update', 'uses' => 'Backend\Settings\WarehouseController@update']);
            Route::get('warehouses/{id}/delete', array('as' => 'warehouses.delete', 'uses' => 'Backend\Settings\WarehouseController@getDelete'));
            Route::get('warehouses/{id}/confirm-delete', array('as' => 'warehouses.confirm-delete', 'uses' => 'Backend\Settings\WarehouseController@getModalDelete'));
            Route::get('warehouses/{warehouses}', ['as'=> 'warehouses.show', 'uses' => 'Backend\Settings\WarehouseController@show']);
            Route::get('warehouses/{warehouses}/edit', ['as'=> 'warehouses.edit', 'uses' => 'Backend\Settings\WarehouseController@edit']);

            #Suggestions
            Route::get('suggestions', ['as'=> 'suggestions.index', 'uses' => 'Backend\Settings\SuggestionController@index']);
            Route::post('suggestions', ['as'=> 'suggestions.store', 'uses' => 'Backend\Settings\SuggestionController@store']);
            Route::get('suggestions/create', ['as'=> 'suggestions.create', 'uses' => 'Backend\Settings\SuggestionController@create']);
            Route::put('suggestions/{suggestions}', ['as'=> 'suggestions.update', 'uses' => 'Backend\Settings\SuggestionController@update']);
            Route::patch('suggestions/{suggestions}', ['as'=> 'suggestions.update', 'uses' => 'Backend\Settings\SuggestionController@update']);
            Route::get('suggestions/{id}/delete', array('as' => 'suggestions.delete', 'uses' => 'Backend\Settings\SuggestionController@getDelete'));
            Route::get('suggestions/{id}/confirm-delete', array('as' => 'suggestions.confirm-delete', 'uses' => 'Backend\Settings\SuggestionController@getModalDelete'));
            Route::get('suggestions/{suggestions}', ['as'=> 'suggestions.show', 'uses' => 'Backend\Settings\SuggestionController@show']);
            Route::get('suggestions/{suggestions}/edit', ['as'=> 'suggestions.edit', 'uses' => 'Backend\Settings\SuggestionController@edit']);

            #Services
            Route::get('services', ['as'=> 'services.index', 'uses' => 'Backend\Settings\ServiceController@index']);
            Route::post('services', ['as'=> 'services.store', 'uses' => 'Backend\Settings\ServiceController@store']);
            Route::get('services/create', ['as'=> 'services.create', 'uses' => 'Backend\Settings\ServiceController@create']);
            Route::put('services/{services}', ['as'=> 'services.update', 'uses' => 'Backend\Settings\ServiceController@update']);
            Route::patch('services/{services}', ['as'=> 'services.update', 'uses' => 'Backend\Settings\ServiceController@update']);
            Route::get('services/{id}/delete', array('as' => 'services.delete', 'uses' => 'Backend\Settings\ServiceController@getDelete'));
            Route::get('services/{id}/confirm-delete', array('as' => 'services.confirm-delete', 'uses' => 'Backend\Settings\ServiceController@getModalDelete'));
            Route::get('services/{services}', ['as'=> 'services.show', 'uses' => 'Backend\Settings\ServiceController@show']);
            Route::get('services/{services}/edit', ['as'=> 'services.edit', 'uses' => 'Backend\Settings\ServiceController@edit']);

            #DHL
            Route::get('dhl/price',['as' => 'dhl.price', 'uses' =>'Backend\Logistics\DHLController@edit']);
            Route::post('dhl/price',['as' => 'dhl.price', 'uses' =>'Backend\Logistics\DHLController@update']);
        });

        # Remaining pages will be called from below controller method
        # in real world scenario, you may be required to define all routes manually
        Route::get('{name?}', 'JoshController@showView');
    });
});