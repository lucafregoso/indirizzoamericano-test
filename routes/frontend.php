<?php

/*
|
| Frontend Routes
|
*/

# Authentication
Route::group(['prefix' => 'authentication'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::get('/', ['as' => 'auth','uses' => 'Frontend\AuthController@getAuth']);
        Route::post('login', ['as' => 'login', 'uses' => 'Frontend\AuthController@postLogin']);
        Route::post('register', ['as' => 'register', 'uses' => 'Frontend\AuthController@postRegister']);
        Route::get('activate/{userId}/{activationCode}', ['as' =>'activate','uses'=>'Frontend\AuthController@getActivate']);
        Route::get('forgot-password', ['as' => 'forgot-password','uses' => 'Frontend\AuthController@getForgotPassword']);
        Route::post('forgot-password', ['as' => 'forgot-password','uses' => 'Frontend\AuthController@postForgotPassword']);
        Route::get('forgot-password-confirm/{userId}/{passwordResetCode}', ['as' => 'forgot-password-confirm', 'uses' => 'Frontend\AuthController@getForgotPasswordConfirm']);
        Route::post('forgot-password-confirm/{userId}/{passwordResetCode}', ['as' => 'forgot-password-confirm', 'uses' => 'Frontend\AuthController@postForgotPasswordConfirm']);
    });
    Route::get('logout', ['as' => 'user.logout', 'uses' => 'Frontend\AuthController@getLogout']);
});

#Static
Route::get(Lang::get('routes.static.explanation'), ['as' => 'static.explanation', 'uses' => 'Frontend\HomeController@explanation']);
Route::get(Lang::get('routes.static.shipping'), ['as' => 'static.shipping', 'uses' => 'Frontend\HomeController@shipping']);
Route::get(Lang::get('routes.static.why'), ['as' => 'static.why', 'uses' => 'Frontend\HomeController@why']);
Route::get(Lang::get('routes.static.rates'), ['as' => 'static.rates', 'uses' => 'Frontend\HomeController@rates']);
Route::get(Lang::get('routes.static.options'), ['as' => 'static.options', 'uses' => 'Frontend\HomeController@options']);
Route::get(Lang::get('routes.static.contacts'), ['as' => 'static.contacts', 'uses' => 'Frontend\HomeController@contacts']);
Route::get(Lang::get('routes.static.support'), ['as' => 'static.support', 'uses' => 'Frontend\HomeController@support']);
Route::get(Lang::get('routes.static.track'), ['as' => 'static.track', 'uses' => 'Frontend\HomeController@track']);

Route::group(['middleware' => 'SentinelUser'], function () {
    # Storage
    Route::group(['prefix' => 'storage'], function () {
        Route::get('/', ['as' => 'storage', function () {
            return Redirect::route('storage.dashboard');
        }]);

        #Dashboard
        Route::get('dashboard', ['as' => 'storage.dashboard', 'uses' => 'Frontend\StorageController@dashboard']);

        #Packages
        Route::get('packages', ['as' => 'storage.packages', 'uses' => 'Frontend\PackagesController@getPackages']);
        Route::post('packages', ['as' => 'storage.packages', 'uses' => 'Frontend\PackagesController@postPackages']);
        Route::get('packages/{package}/services/photo', ['as' => 'storage.packages.services.photo', 'uses' => 'Frontend\PackagesController@servicePhotos']);
        Route::post('packages/value', ['as' => 'storage.packages.value', 'uses' => 'Frontend\PackagesController@postPackagesValue']);
        Route::get('packages/success', ['as' => 'storage.packages.success', 'uses' => 'Frontend\PackagesController@success']);

        #Orders
        Route::get('orders', ['as' => 'storage.orders', 'uses' => 'Frontend\OrdersController@getOrders']);

        #Personal Shopper
        Route::get('personal-shopper', ['as' => 'storage.personal-shopper', 'uses' => 'Frontend\PersonalShopperController@getPersonalShopper']);
        Route::post('personal-shopper', ['as' => 'storage.personal-shopper', 'uses' => 'Frontend\PersonalShopperController@postPersonalShopper']);
        Route::get('personal-shopper/{order}', ['as' => 'storage.personal-shopper.order', 'uses' => 'Frontend\PersonalShopperController@getPersonalShopperOrder']);
        Route::post('personal-shopper-confirm/{order}', ['as' => 'storage.personal-shopper-confirm', 'uses' => 'Frontend\PersonalShopperController@confirmOrderPersonalShopper']);
        Route::get('personal-shopper-success', ['as' => 'storage.personal-shopper-success', 'uses' => 'Frontend\PersonalShopperController@success']);

        #Notifications
        Route::get('notifications', ['as' => 'storage.notifications.index', 'uses' => 'Frontend\NotificationsController@index']);
        Route::post('notifications/update', ['as' => 'storage.notifications.update', 'uses' => 'Frontend\NotificationsController@updateNotification']);

        #Options
        Route::get('options', ['as' => 'storage.options', 'uses' => 'Frontend\OptionsController@getOptions']);
        Route::post('options', ['as' => 'storage.options', 'uses' => 'Frontend\OptionsController@postOptions']);

        #Profile
        Route::group(['prefix' => 'profile'], function () {
            Route::get('edit', ['as' => 'user.profile.edit', 'uses' => 'Frontend\UserController@getProfileEdit']);
            Route::post('edit', ['as' => 'user.profile.edit', 'uses' => 'Frontend\UserController@postProfileEdit']);
        });
    });

    #Payments
    Route::get('orders/{order}/checkout', ['as' => 'storage.orders.checkout', 'uses' => 'Frontend\PaymentController@getCheckout']);
    Route::get('orders/{order}/checkout/done', ['as' => 'storage.orders.checkout.done', 'uses' => 'Frontend\PaymentController@getDone']);
    Route::get('orders/{order}/checkout/cancel', ['as' => 'storage.orders.checkout.cancel', 'uses' => 'Frontend\PaymentController@getCancel']);

});

# Spedition price for tool
Route::post('spedition/price', ['as'=> 'spedition.price', 'uses' => 'Backend\PaymentController@speditionPrice']);

# Suggestion
Route::get('suggestions/proxyme', ['as' => 'suggestions.proxyme', 'uses' => 'Frontend\SuggestionsController@proxyMe']);
Route::get('suggestions/{id?}', ['as' => 'suggestions', 'uses' => 'Frontend\SuggestionsController@index']);

# Newsletter
Route::post('newsletters', ['as'=> 'newsletters', 'uses' => 'Frontend\HomeController@newsletters']);

# Home
Route::get('/', ['as' => 'home', 'uses' => 'Frontend\HomeController@index']);

//TEMP
Route::get(Lang::get('routes.static.temp'), ['as' => 'temp', function () {
    return View::make('static.temp');
}]);
